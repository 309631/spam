doc:
	mkdir -p public
	sphinx-apidoc -o docs/source src/spam
	make -C docs/source html
	cp -r docs/source/_build/html/* public

coverage: tests/*
	mkdir -p public
	coverage run -m pytest tests/test_excursions.py
	coverage html
	coverage report
	cp -r coverage public

doc-full:
	mkdir -p public
	sphinx-apidoc -o docs/source src/spam
	make -C docs/source html SPHINXOPTS="-D sphinx_gallery_conf.run_stale_examples=True"
	cp -r docs/source/_build/html/* public

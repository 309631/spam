#ifndef __TETRAHEDRON_HPP
#define __TETRAHEDRON_HPP

#include <vector> // this include is needed for swig compilation

class tetrahedron
{

public:
  tetrahedron(){};
  ~tetrahedron(){};

  std::vector<std::vector<double>> get_coor_intrs(std::vector<std::vector<double>> c_tet,
                                                  std::vector<double> v_tet, std::vector<double> v_mid, std::vector<std::vector<unsigned>> v_pos,
                                                  unsigned int ph, double thresh, std::vector<unsigned> &l_theta);
  std::vector<double> get_interface(std::vector<std::vector<double>> c_tet, std::vector<std::vector<double>> c_theta,
                                    std::vector<std::vector<unsigned>> v_pos, unsigned int fd);

  std::vector<double> get_centroid(std::vector<std::vector<double>> c_tet);
  double get_volume_tet(std::vector<std::vector<double>> c_tet);
  double get_sub_volume(std::vector<std::vector<double>> c_tet, std::vector<std::vector<double>> c_theta,
                        unsigned int fd, std::vector<std::vector<unsigned>> v_pos, std::vector<unsigned> l_theta);

  double _dprod(std::vector<double> a, std::vector<double> b);
  std::vector<double> _cprod(std::vector<double> a, std::vector<double> b);
};

#endif //__TETRAHEDRON

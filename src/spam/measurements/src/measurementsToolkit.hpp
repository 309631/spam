#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>

namespace py=pybind11;

void computeCorrelationFunction(py::array_t<float> vol,
                                py::array_t<double> output,
                                unsigned int stepCentre,
                                unsigned int nthreads);

std::vector<std::vector<double> > computeCurvatures(const std::vector<std::vector<unsigned int> >& faces,
                                                    const std::vector<std::vector<double> >& verts);

void porosityFieldBinary(py::array_t<unsigned char> porosityFieldBinaryVol,
                         py::array_t<int> porosityFieldBinaryPos,
                         py::array_t<int> porosityFieldBinaryHws,
                         py::array_t<float> porosityFieldBinaryOut);

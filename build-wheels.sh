#!/bin/bash
set -e -x

PYBINS=(
#   "/opt/python/cp27-cp27m/bin"  # Gone from manylinux2014
#   "/opt/python/cp27-cp27mu/bin" # Gone from manylinux2014
#    "/opt/python/cp35-cp35m/bin"
#    "/opt/python/cp36-cp36m/bin"
#    "/opt/python/cp37-cp37m/bin"
#    "/opt/python/cp38-cp38/bin"
   "/opt/python/cp39-cp39/bin"
   "/opt/python/cp310-cp310/bin"
   "/opt/python/cp311-cp311/bin"
  )

# Compile wheels
for PYBIN in ${PYBINS[@]}; do
    "${PYBIN}/pip" install -U pip
    "${PYBIN}/pip" install -U wheel
    "${PYBIN}/pip" install -U setuptools
    "${PYBIN}/pip" install numpy
    "${PYBIN}/pip" install pybind11
    "${PYBIN}/pip" install cython        # needed for scikit image
    "${PYBIN}/python" setup.py build -e "/usr/bin/env python"
    "${PYBIN}/python" setup.py install
    "${PYBIN}/pip" wheel . -w wheelhouse/
    rm -rf build dist
done

# Bundle external shared libraries into the wheels
for whl in wheelhouse/spam*.whl; do
    auditwheel repair "$whl" --plat manylinux2014_x86_64 -w /wheelhouse/
done

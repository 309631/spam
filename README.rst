===============================================================
spam - The Software for Practical Analysis of Materials
===============================================================


.. image:: https://img.shields.io/badge/license-GPLv3-blue.svg
    :target: https://gitlab.com/spam-project/spam/-/blob/master/LICENSE.md

.. image:: https://gitlab.com/spam-project/spam/badges/master/pipeline.svg
   :target: https://gitlab.com/spam-project/spam/-/commits/master

.. image:: https://gitlab.com/spam-project/spam/badges/master/coverage.svg
   :target: https://spam-project.gitlab.io/spam/coverage/

.. image:: https://badge.fury.io/py/spam.svg
   :target: https://pypi.org/project/spam/

.. image:: https://joss.theoj.org/papers/10.21105/joss.02286/status.svg
   :target: https://doi.org/10.21105/joss.02286

.. image:: https://static.pepy.tech/badge/spam/month
   :target: https://pepy.tech/project/spam

.. image:: https://img.shields.io/static/v1?label=Chat&color=green&logo=matrix&style=social&message=join
   :target: https://matrix.to/#/#spam:matrix.org

Spam is a piece of Python software built upon NumPy and SciPy
for the analysis and manipulation of 3D and 2D data sets in
material science, be they from x-ray tomography, random fields
or any other source.

A number of common functions are provided that are
either lacking or slow in Numpy and Scipy, which are expected
to be used by users within new python scripts.
These functions are in the tools/ directory, and include tools
to work with random fields, morphological operations, digital
image correlation, and labelled images.
Some of spam's functions transparently call C/C++ functions
for speed.

Some user-callable scipts are also provided -- they are more
complex pieces of code that combine a number of functions and
which have a command-line interface.
For the moment the scripts are 3 different image correlation
techniques.

Please have a look at our online documentation for:

* `Installation instructions`_
* `General introduction`_
* `Examples`_
* And a number of detailed tutorials

If you find bugs, need help, or want to talk to the developers, we use a `element.io`_/`matrix.org`_ **chat room** for organisation, please join it `here`_ and come and talk to us -- it is easy, there is a chat client that can run in your web broswer.
All you need to do is choose a user name!


.. _Installation instructions: https://www.spam-project.dev/docs/installation/index.html
.. _General introduction: https://www.spam-project.dev/docs/intro.html
.. _Examples: https://www.spam-project.dev/docs/spam_examples/index.html

.. _element.io: https://element.io/features
.. _matrix.org: https://matrix.org/
.. _here: https://matrix.to/#/#spam:matrix.org



Changelog
==========

+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version         | Date       | Notes                                                                                                                                |
+=================+============+======================================================================================================================================+
| Version 0.7.0.1 | 2024-02-16 | Remove R dependency for generating random fields in favor of gstools                                                                 |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.5.2 | 2024-01-24 | Update returnStatus for the registration and local DIC differentiating image texture and transformation operator issues              |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.5.1 | 2023-12-04 | Drop python 3.8 and update doc links                                                                                                 |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.5   | 2023-11-23 | Revamp documentation                                                                                                                 |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.4   | 2023-08-28 | new `returnPhiMaskCentre` option in spam.DIC.register()                                                                              |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.3.2 | 2023-07-19 | Fix inputs to spam-ereg, impose same-sized images in spam-reg                                                                        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.3.1 | 2023-07-07 | Revert inputs to spam-ereg, update links on pypi                                                                                     |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.3   | 2023-07-06 | Fixes masking in register(), move to pyproject.toml, and 2D imShowProgress and applyPhiPython                                        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.2.1 | 2023-03-21 | Imports in graphical tools fixed                                                                                                     |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.2   | 2023-03-17 | Fixes to registration, pep8 and black the whole code, dev on global and projection                                                   |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.1.3 | 2022-10-25 | Fix for histogramTools, a number of library calls updated for deprecation warnings                                                   |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.1.2 | 2022-09-22 | Parallelised tetLabel, safety in spam-pixelSearch, -skp in spam-ldic                                                                 |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.1.1 | 2022-01-20 | Registration subtraction option for spam-passPhiField (-regs)                                                                        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.1   | 2021-10-29 | New package spam.orientations and segmentations functions                                                                            |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.0.3 | 2021-07-08 | Fixed -applyF check. Now registration guess is correctly applied to a set of points                                                  |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.0.2 | 2021-06-23 | Fixed spam-mmr-graphical and PyQt5 dependencies. New spam.plotting.plotSphericalHistogram renders a 3D orientation distribution!     |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.0.1 | 2021-05-20 | Small fixes to spam-filterPhifield and spam-regularStrain                                                                            |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.6.0   | 2021-05-04 |  Massive rewrite of image correlation scripts, introduction of spam-pixelSearchPropagate, spam-filterPhiField, spam-passPhiField.    |
|                 |            |  The spam-pixelSearch is now separate from spam-ldic and spam-ddic, and works both in grid and labelled mode.                        |
|                 |            |  Please check out new script documentation for a flowchart of how these should be used in series.                                    |
|                 |            |  Loads of scripts and quite a few functions are now multiprocess, and MPI parallelisation is completely dropped along with mpi4py.   |
|                 |            |  pygmsh dependency is also now dropped                                                                                               |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.3.4 | 2021-03-19 |  Last version to support python 3.5.                                                                                                 |
|                 |            |  Update gradient option in spam-ldic, new function to generate pixelated spheroids: spam.kalisphera.makeBlurryNoisySpheroid()        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.3.3 | 2020-11-27 |  spam-reg script, spam-ereg-discrete writing fix, spam-ldic update gradient option, implementation of Geers in 2D                    |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.3.2 | 2020-10-27 |  spam-ereg-discrete mask option reinstated, many fixes for spam-ldic and registerMultiscale() for 2D images                          |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.3.1 | 2020-10-23 |  spam-ereg-discrete make safer with slicePadded and moveGrains now renamed to moveLabels and proposed as function with erodeLabels   |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.3   | 2020-10-07 |  Improvements in edge cases in spam-ldic and spam-ddic, thanks to a helper function called spam.helpers.slicePadded().               |
|                 |            |  New debug mode for spam-ddic as well as a graphical tool to manually align labels called spam-ereg-discrete                         |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.2.1 | 2020-07-20 |  This is the version in the JOSS paper. Making python 3.8 package for PyPI, along with classifiers. Python 2.7 dropped               |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.2   | 2020-06-03 |  Big improvements in spam-mmr and spam-mmr-graphical, all TSVs im1->im2 and gradients always computed in im2                         |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1.5 | 2020-05-28 |  Don't recompute Jacobian in register if not needed. Safety in pixel search                                                          |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1.4 | 2020-05-16 |  Fix spam-deformImageFromField, C++14                                                                                                |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1.3 | 2020-04-20 |  Fix spam-mmr and improvements to pixel search in spam-ddic                                                                          |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1.2 | 2020-04-20 |  Fix and test for large initial guesses in register(), spam-mmr-graphical revived                                                    |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1.1 | 2020-04-08 |  Fix for running `spam-ldic` for pixel search                                                                                        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.1   | 2020-04-07 |  Fix for running `spam-ddic` with mpi, implementation of S. Brisard's Directional Erosion                                            |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.5.0   | 2020-03-27 |  Big rename of scripts, functions, variables, parameters, with some backwards compatibility in TSV file reading.                     |
|                 |            |  Some examples:                                                                                                                      |
|                 |            |                                                                                                                                      |
|                 |            |    - spam.correlate.lucasKanade -> spam.correlate.register                                                                           |
|                 |            |    - spam.helpers.readTSV       -> spam.helpers.readCorrelationTSV                                                                   |
|                 |            |                                                                                                                                      |
|                 |            |  New framework for the calculation of strains, where the computation of F is separated from its decomposition.                       |
|                 |            |  Output fields from correlation with prefixes "SubPix" and "SubPixel" become prefixless                                              |
|                 |            |  In TSV outputs from correlation the components like "F12" are now called "Fzy"                                                      |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.4.3   | 2020-01-16 |  Various fixes to graphical clients (able to save TSV from `spam-mmr-graphical` and do a last, precise run with `spam-mmr`).         |
|                 |            |  Improvement to triangulation (now with CGAL alpha shapes) and discrete strain calculator (tested results)                           |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.4.2   | 2019-09-25 |  spam-mmr-graphical now working, improvements in spam-mmr.                                                                           |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.4.1   | 2019-09-13 |  spam-mmr-graphical now working (c-python type error).                                                                               |
|                 |            |  Various bugfixes in clients.                                                                                                        |
|                 |            |  spam-ITKwatershed now accepts markers                                                                                               |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.4.0   | 2019-07-18 |  c++ now bound with pybind11.                                                                                                        |
|                 |            |  New graphical script `spam-mmr-graphical` for multi-modal registration.                                                             |
|                 |            |  New graphical script `spam-ereg` for eye (manual) registration.                                                                     |
|                 |            |  Python3 upgrade recommended for all users                                                                                           |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.3.3.1 | 2019-05-27 |  Binning 2^31 fix, remove lines for immediate prints that are not py3 compatible                                                     |
|                 |            |  First version with CGAL triangulation                                                                                               |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.3.2.1 | 2019-05-14 |  Update pip documentation and changelog                                                                                              |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.3.2   | 2019-04-30 |  Fix segfault with images larger than 2^31 voxels, and output both subtracted                                                        |
|                 |            |  and original fields in spam-ldic                                                                                                    |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.3.1   | 2019-04-08 |  Fix a number of forgotten spam.DIC.transformationOperator functions                                                                 |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.3.0   | 2019-03-28 |  Consistent naming in DIC: Phi is 4x4 homogeneous deformation function                                                               |
|                 |            |  and F is its internal 3x3 displacement gradient                                                                                     |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.2.2.2 | 2019-03-21 |  First version on pip with complete dependencies.                                                                                    |
|                 |            |  This version of spam runs fully in a venv with `pip install spam`                                                                   |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.2.2.1 | 2019-03-20 |  Pull in `requirements.txt` into `setup.py` automatically.                                                                           |
|                 |            |  This aligns the build from git with the build from pip.                                                                             |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.2.2   | 2019-02-21 |  Approximate python3 compatibility                                                                                                   |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.2.1   | 2019-02-18 |  Add PyPI documentation to pip in RST                                                                                                |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+
| Version 0.2.0   | 2019-02-18 |  Add PyPI documentation to `README.rst` to appear on pip.                                                                            |
|                 |            |  Note that the build status and coverage badges won't appear until access to gitlab is opened                                        |
+-----------------+------------+--------------------------------------------------------------------------------------------------------------------------------------+

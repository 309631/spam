Spam Meeting 0001 (Week 16 of 2018)

Agenda:

    1. Spam-Core Riot Room
        Let's continue like that for now.
        Public but not searchable.

    2. Initial Publication authors etc.
        Large, perhaps make riot-room for the paper?
        Eddy will write.

    3. SPAM Lame
        Fix a date -- 2018-05-15 PM
        Graphics cards - for now use old Tesla?

    4. Tidy up data
        There will be two data:
            - ad minima for examples intagrated into spam GitLab
            - "client" data which is a bit bigger:
                getClientData.sh wget whole directory from people.3sr-grenoble.fr or similar

    5. How to organise:

        5.1 clients
            Clients documentation in top-level documentation of "command index".
            No images here, a formal function-style documentation

        5.2 Plot examples
            Use blocks and multiple images

        5.3 Tutorial vs. Examples
            Can we link MathJax locally?
            e.g. How to transform an image, how to correalte fakely
                 Real examples with clients need data download

        5.4 Functions examples

        5.5 link to mod index function
            Locally it's not possible because of filesystem and not server

    6. Pip Packages, name SPAM? / data OK / relancer pip OK

    7. License Final -- Keep GLPv2 -- should be fine to feed back to scipy upstream

    8. Public Repo:
        We decide on a pip package for "passive" users
        GitLab open on univ-grenoble-alpes

    9. Make serious CI-integrated tests -- +git script for building and pushing

    10. PyBind 11 next time...

.. _howToContribute:

##################
How to Contribute
##################

Everyone is welcome to contribute to this tool.
Write some documentation, or contribute your beautiful examples!
If you add citations they should be in APA format

The following programming conventions are used in *spam*:

    * General programming guidelines
        * We use `camelCase` for function and variable names
        * Readability and understandability come first
        * When manipulating files, be careful to use `os.path.join(DIR, FILENAME)` instead of `DIR + FILENAME`
        * C/C++ bindings with *pybind11* for critical pieces of code are accepted, but are not expected to be called directly by users, so should be accompanied by python helper functions which prepare data for execution with C/C++
        * Parallelisation is done with the multiprocessing library in python, and not in C/C++

    * Documentation:
        * Should be clear and refer to scientific literature where possible (and where it helps understanding)
        * *i.e.*, a *docstring* should accompany each function

    * Layout of *spam*
        * Atomic functions are grouped into toolkits in /tools
        * C/C++ code is kept all together in a /src folder in each directory inside /tools and is compiled into a single shared object \*.so called toolToolkit
        * /scripts contains more complex code that calls functions and which are called on the command line with flags or a settings file
          These functions should be self-explanatory and monkey-safe
          /tools/helpers/optionsParser.py contains all the settings for command line arguments
        * /tests contains tests to be run against each function
        * /examples contains python examples showing (off) pieces of functionality that logically go together for users to adapt and change
        * /tools/data contains examples of 3D data for tests

    * Functions:
        * Should have clear names
        * Should be understandable by themselves by moderately experienced users
        * Should take in simple data structures (numpy arrays, lists, numbers, strings), avoid Classes *etc*
        * Should require as few inputs as possible, with intelligent setting of optional variables
        * Should ideally have an example
        * When called don't use the full path of the function, *i.e.*, prefer `spam.DIC.register()` instead of `spam.DIC.registration.register()`

    * Scripts:
        * Should have short and meaningful names beginning with `spam-`

    * Tests:
        * For function we are aiming for 100% coverage, see here, so check all cases:
            https://spam-project.gitlab.io/spam/coverage/
        * Write your output files (with `os.path.join`, see above) into a directory called `.testDump` then remove the while directory during `tearDown()`

Our open access gitlab is here: https://gitlab.com/spam-project/spam

You will have to make an account to be able to push.

If you'd like to contribute something to spam, please develop it in a branch and then ask the core team to inspect and eventually merge it!

﻿##########################
Conventions
##########################

Sign conventions
-----------------
Full field measurements are recorded as a 3D matrix of values.
Consistent with addressing matrices, the (0,0,0) position refers to the top-left of the image.
These measurements typically come from small area-sensors such as CCD or CMOS sensors.

In spam, we consider that indices referring to pixel (or voxels in 3D) refer to the *centre* of the pixel, and not the position of its top corner.
This is consistent with `scipy` (in particular tools such as `scipy.ndimage.measurements.center_of_mass`).

The `label` toolkit follows this convention, and the `kalisphera` toolkit has been modified to follow this convention.

The centre of an image is at :code:`(numpy.array(im.shape) - 1) / 2.0`


Units of measurements
----------------------
In the `DIC` package in spam, displacements are measured in pixels, and rotations are expressed in degrees.
3D rotations are expressed in an *axis-and-angle* format, more specifically, a compact *rotation vector* format, where the `direction` of the vector gives the 3D `axis` of rotation, and the `length` of the vector is the rotation `angle in degrees`.
See the wonderful `Wikipedia`_ page on this very subject.

.. _Wikipedia: https://en.wikipedia.org/wiki/Rotation_formalisms_in_three_dimensions

Unless reported otherwise, lengths in the `label` toolkit are described in pixels, and volumes as a number of voxels.

Strain from the different strain calulations is (by definition) dimensionless and is given in absolute units (not %).

The mechanical (and not soil mechanics) convention is used, therefore increases in size are positive (this also applies for the first invariant of strain).


Image format
----------------------
Almost all scripts take in images in the form of **one TIFF file** per state.
If your 3D data is in the form of a series of slices, please save them as a single 3D TIFF file.
This can be done by opening it as a stack in ImageJ/Fiji and saving directly,
or loading each slice into a 3D numpy array and saving it with `tifffile.imsave`
This is not the case for individual functions, since they take in images as numpy arrays -- so load your image data in whatever format you want.


Digital Image/Volume correlation
---------------------------------
There are a number of different tools in `spam` for making kinematics measurements from recorded field measurements.
Some of these tools work for both 3D volumes coming from tomography, as well as surface measurements coming from photography, and so we use the generic term "image" to cover both 2D and 3D fields.

In all DIC function and scripts, we are correlating(`im1`, `im2`), where `im1` and `im2` are either images of the sample at different time steps or two images with different modalities of the same sample at the same time.
In all cases the Φ deformation function returned from the function is to be applied to im1 to deform it into im2 (although in some/many cases it is im2 which is numerically deformed).

Therefore if your DIC converges you can apply your Φ to image 1 with :code:`im1def = applyPhi(im1, Phi=Phi)` if it is a registration and with the following script :code:`spam-deformImageFromField -pf yourDIC.tsv im1.tif` if you've measured a displacement field.

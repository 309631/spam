.. spam documentation master file, created by
   sphinx-quickstart on Tue Sep 12 22:24:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to spam's documentation!
========================================

This is the documentation of `the spam project <https://www.spam-project.dev>`_.

.. toctree::
    :maxdepth: 3
    :caption: Getting started

    intro.rst
    installation/index

.. toctree::
    :maxdepth: 3
    :caption: Getting inspired

    tutorials/index
    spam_examples/index
    scripts

.. toctree::
    :maxdepth: 1
    :caption: Getting technical

    conventions
    tricks/index
    indices/index

    Repository <https://gitlab.com/spam-project/spam>


.. toctree::
    :caption: Community

    publications
    howToContribute
    Chat with us <https://spam-project.dev/chat>

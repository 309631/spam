.. _tricks:

Tips & tricks
================

Some random tricks that might help you.

.. toctree::
    :maxdepth: 1

    overlapping
    vtk


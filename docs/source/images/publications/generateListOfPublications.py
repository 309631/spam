from bs4 import BeautifulSoup
import json

# Create data dictionary
citations = []

for i in range(1,11):
    
    with open('spam'+str(i)+'.html') as f:
        soup = BeautifulSoup(f, 'html.parser')

    for item in soup.select('[data-lid]'):
        
        # Get the title
        title = item.select('h3')[0].get_text()
        # Sometimes we got some EOL characters, deal with them
        title = title.replace("\n", "")
        # Sometimes we have [XXX] tags, remove them
        title = title.replace("[HTML]", "")
        title = title.replace("[PDF]", "")
        title = title.replace("[CITATION]", "")
        title = title.replace("[C]", "")
        title = title.replace("[LIVRE]", "")
        title = title.replace("[B]", "")
        # Remove the initial space if any
        title = title.lstrip()
        # Get the link
        try:
            link = item.select('.gs_rt')[0].select('a')[0]['href']
        except:
            link = " "
        # Get the authors, year and Journal data
        refData = item.select('.gs_a')[0].get_text()
        # Remove the \xa0 characters
        refData = refData.replace('\xa0', ' ')
        refData = refData.split(' - ')
        authors = refData[0].replace("…"," et al.")
        year = refData[-2].split(',')[-1].lstrip()

        citations.append([title, authors, year])
        # print('Title: ',title)
        # print('Authors: ', authors)
        # print('Year: ', year)

json_str = json.dumps(citations)    
with open('citations.json', 'w') as outfile:
    outfile.write(json_str)


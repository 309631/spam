.. _spamDeveloperInstall:

Installation for developers
==================================

Running **spam** as a developer means :ref:`clone`, :ref:`sysDep` and then :ref:`spamGITinstall`.
We assume you've created a virtual environment as per the user install, and that it's activated.


.. _clone:

Cloning our repository
------------------------
.. code-block:: console

    $ git clone https://gitlab.com/spam-project/spam.git

Now `cd` into the spam directory that has been downloaded:

.. code-block:: console

    $ cd spam

Good, now onwards to :ref:`sysDep`.


|
|

.. _sysDep:

Installing system dependencies
-------------------------------

The installation of these required packages (for **spam** compilation and for some of our Python dependencies).
The details of this step is very system-dependent, please see below for your system:

* :ref:`debianDepends`

* :ref:`ScientificLinuxl6.9Depends`

* :ref:`otherLinuxDistros`

|
|

.. _debianDepends:

System dependencies for Debian/Ubuntu Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you're on Debian/Ubuntu:

.. code-block:: console

        $ sudo apt update

        $ sudo apt upgrade

        $ sudo apt install git python3-dev python3-venv python3-tk gcc g++ libeigen3-dev libicu-dev libgmp-dev libmpfr-dev libcgal-dev gmsh libfreetype6-dev libxml2-dev libxslt-dev


Now you're ready to go to: :ref:`spamGITinstall`

|
|

.. _ScientificLinuxl6.9Depends:

System dependencies for Scientific Linux 6.9
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning:
  This is very out of date

If you're on Scientific Linux 6.9 (and you don't want to update to a reasonably modern distribution, `e.g.,` it's installed on a cluster):

.. code-block:: console

    $ sudo yum install epel-release

    $ sudo sh -c 'wget -qO- http://people.redhat.com/bkabrda/scl_python27.repo >> /etc/yum.repos.d/scl.repo'

    $ sudo yum install git swig python27 python27-python-libs python27-runtime python27-python-devel python-pip python-virtualenv gcc eigen3-devel gmp-devel mpfr-devel

Now you're ready to go to: :ref:`spamGITinstall`

|
|

.. _otherLinuxDistros:

System dependencies to install for other Linux distributions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For other linux installations our dependencies are currently:

Python components:
  - python3 and python3 development files
  - python3 virtual env (highly recommended to not install **spam** right into the system)
  - python3 tk libraries for matplotlib

Compilation dependencies:
  - gcc and/or g++
  - eigen3 development files
  - CGAL development files

External programs we call:
  - gmsh

Dependencies of the pip packages we use:
  - libfreetype6-dev (for matplotlib)
  - libxml2-dev
  - libxslt-dev
  - libicu-dev
  - libgmp-dev
  - libmpfr-dev

Once these are install you're ready to go to: :ref:`spamGITinstall`

|
|

.. _spamGITinstall:

Install **spam** from **source**
--------------------------------

Basic install:

.. code-block:: console

    (spam-venv) $ pip install .

Complete install (you can cherry-pick the options), they represent, in order:

* development tools for testing and building
* documentation-building tools around sphinx
* graphical dependencies for GUIs such as `spam-ereg` and `spam-mmr-graphical`

.. code-block:: console

    (spam-venv) $ pip install ".[dev,docs,graphical]"


In the future, when you do

.. code-block:: console

    (spam-venv) $ git pull

to download the latest version of the code, you'll have to repeat setup install above.

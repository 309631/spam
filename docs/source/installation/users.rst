.. _spamUserInstall:

Installation for users
===============================

If you are on Linux or OSX with an arm (M1/M2) chip, good news, installation is easy!

If you are on Windows please install a Debian/Ubuntu WSL (Windows Subsystem for Linux) and then follow this guide.

If you are on an Intel OSX, or you just want to use docker then please follow :ref:`runSpamWithDocker`

OK, let's get started, you just need to issue the following commands:

.. code-block:: bash

    $ # Create a virtual environment
    $ python3 -m venv spam-venv
    $ # Activate your virtual environment
    $ source spam-venv/bin/activate
    $ # update python package manager
    $ pip install -U pip
    $ # install spam
    $ pip install spam

.. note::

  - If the creation of the virtual environment fails, you can try to install the ``venv`` module of python on your system. On Ubuntu this is simply `apt install python3-venv`. More details on the `python virtual environment <https://docs.python.org/3/library/venv.html>`_.

  - After sourcing the virtual environment you'll see ``(spam-venv)`` in front of your terminal prompt. This means that you have activated this python virtual environment. When you open a new terminal, you will have to re-activate your environment to have access to **spam**

**spam** should now be installed! You can try the following command to check:

.. code-block:: console

    (spam-venv) $ spam-ldic --help

If it prints out the options of the programme with no errors you're good to go!
If needed please visit: :ref:`spamJupyter`.


How to use
===========

In order to use **spam** you will always have to **activate its virtual environment** where it is installed:

.. code-block:: console

    $ source /path/to/spam/venv/bin/activate
    (spam-venv) $

Using **spam** in your scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can **include spam in your own scripts** by importing different modules (see `module index`_)

.. code-block:: python

    import spam.DIC

A lot of **spam** functionalities are available as atomic functions.
Please refer to the :ref:`examples-index` for use of **spam** functions in python.

Using existing **spam** scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We also provide more sophisticated functionality in **spam-scripts**, which are called directly from the command line (see :ref:`scriptsTutorial`).

.. _module index: https://spam-project.gitlab.io/spam/py-modindex.html

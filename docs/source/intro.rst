############
Introduction
############

********************
Welcome to **spam**
********************

Welcome to **spam** -- The **S**\oftware for the **P**\ractical **A**\nalysis of **M**\aterials.


This is a **Python** package for handling and correlating 3D fields for applications in material science.
**Spam** has evolved to cover needs of data analysis from 3D x-ray tomography work and correlated random fields with mechanical applications.
**Spam** is first and foremost a measurement package, and is divided into the following toolkits:

  - ``DIC`` and ``deformation``: Tools for measuring the transformation between 2D and 3D images, containing a non-rigid image correlation engine
  - ``label``: Toolkit to measure and manipulate labelled images, where discrete particles are labelled with integer voxel patches
  - ``mesh``: Toolkit for generating and manipulation 3D spatial meshes. In **spam** tetrahedral meshes are principally used
  - ``kalisphera``: Toolkit for generating analytical partial volume spheres, useful for testing
  - ``excursions``: Toolkit for the excursion set of correlated random fields theory


Please see :ref:`introToolsProvided` below for a few examples of how **spam** has been used in published literature.
You'll find more in-depth discussion in the various **Tutorials** which introduce our Python function and our more complex `scripts`_, and downloadable ready-to run examples of the Python functions provided in the `Gallery of Examples`_.
Don't forget to browse what Python functions are available in the `module index`_.

Please follow the :ref:`installationInstructions` instructions to install **spam**.


How to cite **spam**
====================

If you find this project useful, please cite:

Stamati et al., (2020). spam: Software for Practical Analysis of Materials. Journal of Open Source Software, 5(51), 2286, https://doi.org/10.21105/joss.02286



Come and Chat!
===============
At the moment **spam** is principally developed by **Edward Andò**, **Rémi Cailletaud**, **Emmanuel Roubin**, **Olga Stamati** and **Gustavo Pinzon**, contributions are welcome!
Head over to :ref:`howToContribute` to find out more.

**Spam** is built on top of **NumPy** and **SciPy**, and has some optimised C/C++ functions for speed.

There is a `element.io`_/`matrix.org`_ **chat room** for **spam**, please join it `here`_ and come and talk to us -- it is easy, there is a chat script that can run in your web broswer.
All you need to do is choose a user name!

.. _scripts: https://spam-project.gitlab.io/spam/scripts.html
.. _module index: https://spam-project.gitlab.io/spam/py-modindex.html
.. _Gallery of Examples: https://spam-project.gitlab.io/spam/spam_examples/index.html
.. _element.io: https://element.io/features
.. _matrix.org: https://matrix.org/
.. _here: https://matrix.to/#/#spam:matrix.org


Issues?
========

If you have issues using spam, or if you find a bug, please create an issue on our `gitlab`_.

.. _gitlab: https://gitlab.com/spam-project/spam/-/issues



.. _introToolsProvided:

************************
Tools provided
************************

Image correlation/deformation toolkit
======================================

Tools for **measuring the transformation** between 2D and 3D images.

There is a tutorial to the deformation function Φ and some examples of image correlation (both with Python functions and with scripts): :ref:`imageCorrelationTheory`.
There is a specific toolkit called ``deformation`` to help with computing, decomposing and manipulating Φ.
This toolkit contains functions for computing Φ from a displacement field, and tools for computing strains -- please see the :ref:`strainTutorial` for more information.

The correlation library includes a number of **simple Python functions** that can be called manually (most conveniently within an iPython context) such as ``spam.DIC.register``, as well as more complex scripts, see :ref:`scriptsTutorial`.

The **local correlation** script uses a regular grid of squares/cubes (see :ref:`imageCorrelationPractice`), here is a published example:

.. figure:: images/intro/EleniCOxNeutrons.jpg
    :align: center

    Volumetric strain results from [Stavropoulou2018]_ of an unconfined Callovo-Oxfordian clay rock cracking from water uptake

.. Add when released:
..    Stamati et al Experimental paper

The **discrete correlation** script operates label-by-label on a labelled volume (see :ref:`discreteImageCorrelationTutorial`), here is a published example:

.. figure:: images/intro/ando2018.jpg
    :scale: 33%
    :align: center

    Example of discrete correlation from [Ando2019]_, showing grains coloured by their measured vertical displacement



A **global** DIC script (which solves a global correlation problem on a mesh) also exists, but is still being tested.
This has been used (carefully...) for the following publication:

.. figure:: images/intro/global.jpg
    :scale: 80%
    :align: center

    Global displacement field and nodal displacements from [Stavropoulou2020]_

We also have an implementation for a **multimodal registration** algorithm [Tudisco2017]_ to align two images of the same object acquired with different modalities.
Here is a published result:


.. figure:: images/intro/MMR.jpg
    :scale: 50%
    :align: center

    X-rays and Neutron tomography of a concrete specimen from [Roubin2019]_


Here is a graphical toolkit to help with initial alignment and other functions such as peak fitting:

.. figure:: images/intro/spam-mmr-graphical.jpg
    :align: center

    Example of the first step of `spam-mmr-graphical`




Label toolkit
===============

Tools to deal with labelled images (of particles) where 3D images are provided.
In these images, voxels are "labelled" with integer values that indicate what particle they belong to.
Please see the :ref:`labelToolkitTutorial`.

For example we offer a significantly faster (and much less memory demanding) calculation of centres of mass in a labelled image compared to ``scipy.ndimage.center_of_mass``.

Also offered are calculations of:
    * the volume of each label
    * the centre of mass of each label
    * the eigen-vectors and eigen values of the moment of inertia tensor of each label (convenient for orienting simple shapes in 3D)
    * the length of the half-axes of an ellipse fitting the particle

.. figure:: images/intro/LabelToolkit.jpg
    :scale: 33%
    :align: center

    Example of discrete measurements: here grains are coloured according to their volume



Mesh tools
===========

Tools to generate and manipulate meshes (random meshes, Delaunay triangulations). In **spam** tetrahedral meshes are principally used.
You can also find tools to project morphologies onto meshes which is detailed in the :ref:`projectionTutorial`, and can be seen in an example below:

.. figure:: images/intro/OlgaProjection.jpg
    :align: center

    Example of the projection of a microstructure identified from tomography onto a finite-element mesh [Stamati2018b]_



Plotting tools
===============

These tools contain standard and generally useful plotting tools. For example:

orientationPlotter
-------------------

This function allows distributions of 3D orientations to be visualised.
It puts all 3D unit vectors onto the origin (so that the set of all possible unit vectors represents a sphere) and looks at the sphere from the +z-axis.
All -z orientations are flipped, and the projection of the sphere onto the plane is an equal-area project, making it easier to detect clusters.
A binning function is also provided to allow more subtle trends to be detected.

.. figure:: images/intro/MaxOrientations.jpg
    :align: center

    Figure showing evolving contact orientations in a triaxial test on sand from [Wiebicke2020]_
..     Figure of grain-to-grain contact orientations adapted from [Jacquet2013]_



Kalisphera
===========

Kalisphera, a standalone analytical tool for the exact calculation of the partial volume effect of a sphere is also included in **spam**.

.. figure:: images/intro/Kalisphera.jpg
    :scale: 50%
    :align: center


    Illustration of the different analytical cases of a sphere intersection with a cube that are solved to calculate the exact partial volume effect on the edge of a sphere in Kalisphera [Tengattini2015]_

It has been imported into this project for convenience for potential users, but also because it helps in the testing of some functions (the label ones for example).
Here we use Félix Bertoni's C++ version through a Python helper function.

Below, for example, Kalisphera is used as a ground truth to which controlled measurement errors (blur and greyscale noise) are added to assess downstream errors in the analysis of contacts between spherical particles.

.. figure:: images/intro/MaxKalisphera.jpg
    :scale: 33%
    :align: center

    Simulation of measurement noise based on Kalisphera for a metrological study in [Wiebicke2017]_

There is also a live example where you can see the generation of a synthetic image from DEM data: :ref:`sphx_glr_spam_examples_kalisphera_plot_generateAssembly.py`


Helper tools
=============

This series of functions do generally useful and boring stuff like reading and writing files in a formatted way.



References
===========

.. [Ando2019] Andò, E., Dijkstra, J., Roubin, E., Dano, C., & Boller, E. (2019). A peek into the origin of creep in sand. *Granular Matter*, 21(1), 11.
    https://doi.org/10.1007/s10035-018-0863-5

.. .. [Jacquet2013] Jaquet, C., Andò, E., Viggiani, G., & Talbot, H. (2013, May). Estimation of separating planes between touching 3D objects using power watershed. *In International Symposium on Mathematical Morphology and Its Applications to Signal and Image Processing* (pp. 452-463). Springer, Berlin, Heidelberg.
..     https://doi.org/10.1007/978-3-642-38294-9_38

.. [Kawamoto2016] Kawamoto, R., Andò, E., Viggiani, G., & Andrade, J. E. (2016). Level set discrete element method for three-dimensional computations with triaxial case study. *Journal of the Mechanics and Physics of Solids*, 91, 1-13.
    https://doi.org/10.1016/j.jmps.2016.02.021

.. [Stamati2018b] Stamati, O., Roubin, E., Andò, E., & Malecot, Y. (2018). Tensile failure of micro-concrete: from mechanical tests to FE meso-model with the help of X-ray tomography. Meccanica, 1-16.
    https://doi.org/10.1007/s11012-018-0917-0

.. [Stavropoulou2018] Stavropoulou, E., Andò, E., Tengattini, A., Briffaut M., Dufour, F., Atkins, D., Armand, G. (2018). Liquid water uptake in unconfined Callovo Oxfordian clay-rock studied with neutron and X-ray imaging. *Acta Geotechnica*.
    https://doi.org/10.1007/s11440-018-0639-4

.. [Stavropoulou2020] Stavropoulou, E., Andò, E., Roubin, E., Lenoir, N., Tengattini, A., Briffaut, M., & Bésuelle, P. (2020). Dynamics of water absorption in Callovo-Oxfordian Claystone revealed with multimodal x-ray and neutron tomography. *Frontiers in Earth Science*, 8, 6.
    https://dx.doi.org/10.3389/feart.2020.00006

.. [Tengattini2015] Tengattini, A., & Andò, E. (2015). Kalisphera: an analytical tool to reproduce the partial volume effect of spheres imaged in 3D. *Measurement Science and Technology*, 26(9), 095606.
    https://doi.    org/10.1088/0957-0233/26/9/095606

.. [Tudisco2017] Tudisco, E., Jailin, C., Mendoza, A., Tengattini, A., Andò, E., Hall, S. A., ... & Roux, S. (2017). An extension of digital volume correlation for multimodality image registration. *Measurement Science and Technology*, 28(9), 095401.
    https://doi.org/10.1088/1361-6501/aa7b48

.. [Roubin2019] Roubin, E., Andò, E., & Roux, S. (2019). The colours of concrete as seen by X-rays and neutrons. *Cement and Concrete Composites*, 104, 103336.
    https://doi.org/10.1016/j.cemconcomp.2019.103336

.. [Wiebicke2017] Wiebicke, M., Andò, E., Herle, I., & Viggiani, G. (2017). On the metrology of interparticle contacts in sand from x-ray tomography images. *Measurement Science and Technology*, 28(12), 124007.
    https://doi.org/10.1088/1361-6501/aa8dbf

.. [Wiebicke2020] Wiebicke, M., Andò, E., Viggiani, G., & Herle, I. (2020). Measuring the evolution of contact fabric in shear bands with X-ray tomography. *Acta Geotechnica*, 15(1), 79-93.
    https://doi.org/10.1007/s11440-019-00869-9

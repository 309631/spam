import unittest

import numpy
import spam.datasets
import spam.helpers
import spam.kalisphera
import spam.label
import tifffile


class TestAll(spam.helpers.TestSpam):

    # test made on snow.tif
    # dtype = uint16
    # im.max()  = 40103
    # im.min()  =  6237
    # im.mean() = 24420.557174

    def test_crop(self):
        # makes a crop a chack if sum on specific axis are good
        snow = spam.datasets.loadSnow()
        im = spam.helpers.crop(snow, boxSize=[5, 4, 3])
        self.assertEqual(im[0, :, :].sum() + im[:, 1, :].sum() + im[:, :, 2].sum(), 1288729)

        # all cases
        spam.helpers.crop(snow, boxSize=[5, 4, 3], boxOrigin=[150, 400, 400])

    def test_rescale(self):
        # rescale snow.tif between -7.3 and 2.1 and check if max-min of 4bits 9.4
        snow = spam.datasets.loadSnow()
        im = spam.helpers.rescale(snow, scale=[-7.3, 2.1])
        self.assertAlmostEqual(im.max() - im.min(), 9.4, places=5)

    def test_rescaleToInteger(self):
        # rescale snow.tif between 0.0 and 1.0 and check if max-min of 8bits is 255
        snow = spam.datasets.loadSnow()
        im = spam.helpers.rescaleToInteger((snow.astype("<f4") - 6237.0) / 40103, nBytes=1)
        self.assertEqual(im.max() - im.min(), 255)

        # all cases
        spam.helpers.rescaleToInteger((snow.astype("<f4") - 6237.0) / 40103, scale=[0.2, 0.8], nBytes=1)

        with self.assertRaises(ValueError):
            spam.helpers.rescaleToInteger((snow.astype("<f4") - 6237.0) / 40103, nBytes=3)
        with self.assertRaises(ValueError):
            spam.helpers.rescaleToInteger((snow.astype("<f8") - 6237.0) / 40103, nBytes=1)

    def test_convertUnsignedIntegers(self):
        # convert 16bits to 32bits and check if maximum and minimum is good
        snow = spam.datasets.loadSnow()
        im = spam.helpers.convertUnsignedIntegers(snow, nBytes=4)
        self.assertEqual(im.max() - im.min(), 2628230311 - 408754269)

        # all cases
        spam.helpers.convertUnsignedIntegers(snow.astype("<u1"), nBytes=1)
        spam.helpers.convertUnsignedIntegers(snow.astype("<u2"), nBytes=1)
        spam.helpers.convertUnsignedIntegers(snow.astype("<u4"), nBytes=1)
        spam.helpers.convertUnsignedIntegers(snow.astype("<u8"), nBytes=1)
        with self.assertRaises(ValueError):
            spam.helpers.convertUnsignedIntegers(snow.astype("<f4"), nBytes=1)
        with self.assertRaises(ValueError):
            spam.helpers.convertUnsignedIntegers(snow.astype("<u1"), nBytes=3)

    def test_stackToArray(self):
        # create slices from snow.tif, restack
        snow = spam.datasets.loadSnow()
        print("DEBUG", snow.dtype)
        for i, s in enumerate(snow[:]):
            tifffile.imwrite(f"snow_{i:05d}.tif", s)
        stack = spam.helpers.stackToArray("snow_", stack=range(100))
        self.assertEqual(numpy.add(stack, -snow).sum(), 0)

        # test all cases
        # spam.helpers.stackToArray("snow_", stack=range(100), erosion=True)

    def test_shift(self):
        # create slices from snow.tif, restack
        im = numpy.random.rand(5, 5)
        spam.helpers.singleShift(im, 1, 0)
        spam.helpers.singleShift(im, -1, 0)
        spam.helpers.singleShift(im, 1, 1)
        spam.helpers.singleShift(im, -1, 1)

        im = numpy.random.rand(5, 5, 5)
        spam.helpers.singleShift(im, 1, 0)
        spam.helpers.singleShift(im, -1, 0)
        spam.helpers.singleShift(im, 1, 1)
        spam.helpers.singleShift(im, -1, 1)
        spam.helpers.singleShift(im, 1, 2)
        spam.helpers.singleShift(im, -1, 2)

        im = numpy.random.rand(5, 5, 5, 5)
        spam.helpers.singleShift(im, 1, 0)

        im = numpy.random.rand(5, 5, 5)
        spam.helpers.multipleShifts(im, [1, 2, 3])

    # EA: function removed 2020-03-10 for now
    # def test_binarisation(self):
    # im = numpy.random.rand(10, 10, 10)
    # spam.helpers.binarisation(im, threshold=0.5, mask=im > 0.2)

    def test_slicePadded(self):
        im = numpy.random.rand(100, 100, 100)
        # case 1: inside
        startStop = numpy.array([70, 90, 70, 90, 70, 90])
        imSliced = spam.helpers.slicePadded(im, startStop)
        self.assertEqual(
            numpy.subtract(
                im[
                    startStop[0] : startStop[1],
                    startStop[2] : startStop[3],
                    startStop[4] : startStop[5],
                ],
                imSliced,
            ).sum(),
            0,
        )
        self.assertEqual(list(imSliced.shape), [20, 20, 20])

        # case 2: hit top
        startStop = numpy.array([-10, 10, -10, 10, -10, 10])
        imSliced = spam.helpers.slicePadded(im, startStop)
        self.assertEqual(imSliced[0:10, 0:10, 0:10].sum(), 0)
        self.assertEqual(list(imSliced.shape), [20, 20, 20])
        self.assertEqual(numpy.subtract(im[0:10, 0:10, 0:10], imSliced[10:, 10:, 10:]).sum(), 0)

        # case 3: hit bot
        startStop = numpy.array([90, 110, 90, 110, 90, 110])
        imSliced = spam.helpers.slicePadded(im, startStop)
        self.assertEqual(imSliced[10:, 10:, 10:].sum(), 0)
        self.assertEqual(list(imSliced.shape), [20, 20, 20])
        self.assertEqual(numpy.subtract(im[90:, 90:, 90:], imSliced[0:10, 0:10, 0:10]).sum(), 0)

        # case 4: hit top/bot
        startStop = numpy.array([-10, 110, -10, 110, -10, 110])
        imSliced = spam.helpers.slicePadded(im, startStop)
        self.assertEqual(list(imSliced.shape), [120, 120, 120])
        self.assertEqual(imSliced[0:10, 0:10, 0:10].sum(), 0)
        self.assertEqual(imSliced[110:, 110:, 110:].sum(), 0)
        self.assertEqual(numpy.subtract(im, imSliced[10:110, 10:110, 10:110]).sum(), 0)

        # case 5: hit top/bot with mask
        startStop = numpy.array([-10, 110, -10, 110, -10, 110])
        imSliced, mask = spam.helpers.slicePadded(im, startStop, createMask=True)
        self.assertEqual(list(imSliced.shape), [120, 120, 120])
        self.assertEqual(list(mask.shape), [120, 120, 120])
        self.assertEqual(imSliced[0:10, 0:10, 0:10].sum(), 0)
        self.assertEqual(imSliced[110:, 110:, 110:].sum(), 0)
        self.assertEqual(numpy.subtract(im, imSliced[10:110, 10:110, 10:110]).sum(), 0)
        self.assertEqual(mask[10:110, 10:110, 10:110].sum(), 100**3)

        # case 6: not touching im
        startStop = numpy.array([100, 120, 40, 60, 40, 60])
        imSliced, mask = spam.helpers.slicePadded(im, startStop, createMask=True)
        self.assertEqual(list(imSliced.shape), [20, 20, 20])
        self.assertEqual(numpy.isfinite(imSliced).sum(), 0)
        self.assertEqual(mask.sum(), 0)

        # case 7: not touching im
        startStop = numpy.array([-120, -100, 40, 60, 40, 60])
        imSliced, mask = spam.helpers.slicePadded(im, startStop, createMask=True, verbose=True)
        self.assertEqual(list(imSliced.shape), [20, 20, 20])
        self.assertEqual(numpy.isfinite(imSliced).sum(), 0)
        self.assertEqual(mask.sum(), 0)

    def test_splitRebuildImage(self):
        # Importing snow data as im
        im = spam.datasets.loadSnow()

        # case 1: can be the slices be made?
        res1a = spam.helpers.imageManipulation.splitImage(im, (im.shape[0], 1, 1), 0)
        self.assertEqual(res1a, -1)
        res1b = spam.helpers.imageManipulation.splitImage(im, (1, im.shape[1], 1), 0)
        self.assertEqual(res1b, -1)
        res1c = spam.helpers.imageManipulation.splitImage(im, (1, 1, im.shape[2]), 0)
        self.assertEqual(res1c, -1)

        # case 2: margin not greater than the slice
        res2a = spam.helpers.imageManipulation.splitImage(im, (1, 1, 1), im.shape[0])
        self.assertEqual(res2a, -1)
        res2b = spam.helpers.imageManipulation.splitImage(im, (1, 1, 1), im.shape[1])
        self.assertEqual(res2b, -1)
        res2c = spam.helpers.imageManipulation.splitImage(im, (1, 1, 1), im.shape[2])
        self.assertEqual(res2c, -1)

        # case 3 : check if coordinates are repeating themselves
        # Split
        split = spam.helpers.imageManipulation.splitImage(im, (3, 3, 3), 10)
        # Extract
        listCoordinates = []
        listBlocks = []
        for key in split:
            if key != "margin":
                coord, block = split[key]
                listCoordinates.append(coord)
                listBlocks.append(block)
        # Get the margin
        margin = split["margin"]
        # Appending an existing vector to the list of coordinates
        listCoordinatesCopy = listCoordinates[:]
        listCoordinatesCopy.append(listCoordinates[0])

        res3 = spam.helpers.imageManipulation.rebuildImage(listBlocks, listCoordinatesCopy, margin, mode="grey")
        self.assertEqual(res3, -1)

        # case 4: check if listCoordinates and listBlocks have same length
        # Copy of listCoordinates
        listCoordinatesCopy = listCoordinates[:]
        listCoordinatesCopy.pop()

        res4 = spam.helpers.imageManipulation.rebuildImage(listBlocks, listCoordinatesCopy, margin, mode="grey")
        self.assertEqual(res4, -1)

        # case 5: MODE = Grey: is the result of the rebuilding equal to the original image?
        rebuild = spam.helpers.imageManipulation.rebuildImage(listBlocks, listCoordinates, margin, mode="grey")
        diff = im - rebuild
        res5 = numpy.sum(diff)
        self.assertEqual(res5, 0)

        # case 6: MODE = Label:

        # as per Kalisphera example
        # m/pixel
        pixelSize = 40.0e-6
        # The standard deviation of the image blurring to be applied first
        # blurSTD = 0  # not used
        # The standard deviation of the random noise to be added to each voxel
        # noiseSTD = 0  # not used
        boxSizeDEM, centres, radii = spam.datasets.loadDEMboxsizeCentreRadius()
        # get maximum radius to pad our image (periodic boundaries...)
        rMax = numpy.amax(radii)
        boxSize = boxSizeDEM + 3 * rMax
        # move the positions to the new center of the image
        centres[:, :] = centres[:, :] + 1.5 * rMax
        # turn the mm measures into pixels

        boxSize = int(numpy.ceil(numpy.max(boxSize[:]) / pixelSize))
        # centres = centres / pixelSize
        # radii = radii / pixelSize
        Box = spam.kalisphera.makeBlurryNoisySphere(
            (boxSize, boxSize, boxSize),
            centres / pixelSize,
            radii / pixelSize,
            blur=0,
            noise=0,
            flatten=True,
            background=0.0,
            foreground=1.0,
        )
        # Box = numpy.zeros((boxSize, boxSize, boxSize), dtype="<f8")
        # spam.kalisphera.makeSphere(Box, centres, radii)
        # Create Gold-standard segmentation
        imLab = spam.label.watershed((Box > 0.5).astype(int))
        # Split Greylevels
        res = spam.helpers.imageManipulation.splitImage(Box, (2, 1, 1), 15)
        # Create list
        listBlocks = []
        listCoordinates = []
        # Label the blocks
        for key in res:
            if key != "margin":
                coord, block = res[key]
                # Run the watershed
                imLabBlock = spam.label.watershed((block > 0.5).astype(int))
                # Add them to the list
                listBlocks.append(imLabBlock)
                listCoordinates.append(coord)
        # Get the margin
        margin = res["margin"]
        # Rebuild the image
        imTest = spam.helpers.imageManipulation.rebuildImage(listBlocks, listCoordinates, margin, mode="label")
        # Get the volumes & sort
        volOr = spam.label.volumes(imLab)
        volMod = spam.label.volumes(imTest)
        volOr.sort()
        volMod.sort()
        # Get the centres of mass & sort
        comOr = spam.label.centresOfMass(imLab)
        comMod = spam.label.centresOfMass(imTest)
        comOr = numpy.sort(comOr, axis=0)
        comMod = numpy.sort(comMod, axis=0)

        # Case 6A: Check the volumes
        self.assertEqual(numpy.sum(volOr - volMod), 0)
        # Case 6B: Check the centres of mass
        self.assertEqual(numpy.sum(comOr - comMod), 0)
        # Case 6C: Check the size of the final image
        self.assertTrue(imLab.shape == imTest.shape)

        # Case 7: Check for a weird mode
        # Create list
        listBlocks = []
        listCoordinates = []
        # Label the blocks
        for key in res:
            if key != "margin":
                coord, block = res[key]
                # Run the watershed
                imLabBlock = spam.label.watershed((block > 0.5).astype(int))
                # Add them to the list
                listBlocks.append(imLabBlock)
                listCoordinates.append(coord)
        # Get the margin
        margin = res["margin"]
        res7 = spam.helpers.imageManipulation.rebuildImage(listBlocks, listCoordinates, margin, mode="notMode")
        self.assertEqual(res7, -1)

    def test_checkerBoard(self):
        # load images
        xr = spam.datasets.loadConcreteXr().astype("<f4")

        # nonsense test, just to see if it works, 3D images should fail
        r1 = spam.helpers.checkerBoard(xr, xr)
        self.assertEqual(r1, 0)

        # With 2D images and without rescale we sould get what we put in
        r2 = spam.helpers.checkerBoard(xr[xr.shape[0] // 2], xr[xr.shape[0] // 2], rescale=False)
        self.assertEqual(r2.sum(), xr[xr.shape[0] // 2].sum())

        # Just make sure that inv and rescale run, don't check result
        r2 = spam.helpers.checkerBoard(xr[xr.shape[0] // 2], xr[xr.shape[0] // 2], inv=True, n=3)


if __name__ == "__main__":
    unittest.main()

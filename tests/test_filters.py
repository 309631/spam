import random
import unittest

import numpy
import spam.filters.morphologicalOperations as morph
import spam.filters.movingFilters as movf
import spam.kalisphera
import spam.label.ITKwatershed as ws
import spam.label.label
import spam.mesh.structured as smesh


class TestAll(spam.helpers.TestSpam):
    def test_average(self):
        # 3D image
        im = numpy.full((10, 8, 8), 2).astype("<f4")
        se = smesh.structuringElement(radius=2, order=numpy.inf).astype("<f4")
        imAv = movf.average(im, structEl=se)
        crop = (
            slice(int(se.shape[0] / 2), int(im.shape[0] - se.shape[0] / 2)),
            slice(int(se.shape[1] / 2), int(im.shape[1] - se.shape[1] / 2)),
            slice(int(se.shape[2] / 2), int(im.shape[2] - se.shape[2] / 2)),
        )
        self.assertEqual(imAv[crop].mean(), 2)

        # 2D image
        im2D = numpy.full((20, 20), 2).astype("<f4")
        se2D = smesh.structuringElement(radius=1, order=1, dim=2).astype("<f4")
        crop = (
            slice(int(se2D.shape[0] / 2), int(im2D.shape[0] - se2D.shape[0] / 2)),
            slice(int(se2D.shape[1] / 2), int(im2D.shape[1] - se2D.shape[1] / 2)),
        )
        imAv2D = movf.average(im2D, structEl=se2D)
        self.assertEqual(imAv2D[crop].mean(), 2)

    def test_variance(self):
        # 3D image
        im3D = numpy.full((10, 8, 8), 2).astype("<f4")
        se3D = smesh.structuringElement(radius=3, order=2).astype("<f4")
        crop = (
            slice(int(se3D.shape[0] / 2), int(im3D.shape[0] - se3D.shape[0] / 2)),
            slice(int(se3D.shape[1] / 2), int(im3D.shape[1] - se3D.shape[1] / 2)),
            slice(int(se3D.shape[2] / 2), int(im3D.shape[2] - se3D.shape[2] / 2)),
        )
        imVar3D = movf.variance(im3D, structEl=se3D)
        self.assertEqual(imVar3D[crop].sum(), 0)

        # 2D image
        im2D = numpy.full((8, 8), 2).astype("<f4")
        se2D = smesh.structuringElement(radius=1, order=1, dim=2).astype("<f4")
        crop = (
            slice(int(se2D.shape[0] / 2), int(im2D.shape[0] - se2D.shape[0] / 2)),
            slice(int(se2D.shape[1] / 2), int(im2D.shape[1] - se2D.shape[1] / 2)),
        )
        imVar2D = movf.variance(im2D, structEl=se2D)
        self.assertEqual(imVar2D[crop].sum(), 0)

    def test_hessian(self):
        # 3D image
        im3D = numpy.full((10, 8, 8), 2).astype("<f4")
        eigenVals, eigenVecs = movf.hessian(im3D)
        sum = 0
        for eigenVal in eigenVals:
            sum += eigenVal.sum()
        self.assertEqual(sum, 0)

    def test_DirectionalErosion(self):
        # Generate random ellipsoid grain
        theta = numpy.radians(random.randrange(30, 300, 1))
        phi = numpy.radians(random.randrange(0, 90, 1))
        vect = numpy.array(
            [
                numpy.cos(phi),
                numpy.sin(phi) * numpy.sin(theta),
                numpy.sin(phi) * numpy.cos(theta),
            ]
        )
        if vect[0] < 0:
            vect[:] = -1 * vect[:]
        a = random.randrange(1, 10, 1)
        c = random.randrange(1, 10, 1)
        if numpy.abs(a - c) < 2:
            a = a * 2
        maxDim = numpy.max([a + 10, c + 10])
        imTest = spam.kalisphera.makeBlurryNoisySpheroid(
            [maxDim, maxDim, maxDim],
            [numpy.floor(maxDim / 2), numpy.floor(maxDim / 2), numpy.floor(maxDim / 2)],
            [a, c],
            vect,
            background=0,
            foreground=1,
        )
        # Check that a non-list entry of vect generate error
        imEroded = morph.directionalErosion(imTest, vect, a, c, verbose=True)
        self.assertIs(imEroded, None)
        # Perform watershed to label the grains
        labIm = ws.watershed(imTest)
        # Perform directional erosion
        imEroded = morph.directionalErosion(imTest, [vect], a, c, verbose=True)
        # Label the markers
        markers = spam.label.watershed(imEroded)
        # Compute COM
        COM = spam.label.centresOfMass(markers)
        # Check that the center of mass of the marker lies inside the labelled grain
        self.assertEqual(
            labIm[int(COM[1][0]), int(COM[1][1]), int(COM[1][2])],
            numpy.unique(labIm)[1],
        )

    def test_greyDilation(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        im1 = im1 * 255
        # Run
        res = spam.filters.greyDilation(im1)
        # Test that it work without problem
        self.assertIsNotNone(res)
        # Test that the result make sense for the operation
        self.assertGreater(numpy.sum(res), numpy.sum(im1))

    def test_greyErosion(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        im1 = im1 * 255
        # Run
        res = spam.filters.greyErosion(im1)
        # Test that it work without problem
        self.assertIsNotNone(res)
        # Test that the result make sense for the operation
        self.assertGreater(numpy.sum(im1), numpy.sum(res))

    def test_greyMorphologicalGradient(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        im1 = im1 * 255
        # Run
        res = spam.filters.greyMorphologicalGradient(im1)
        # Test that it work without problem
        self.assertIsNotNone(res)

    def test_binaryDilation(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        im1 = im1 * 255
        # Binarise
        im1Bin = im1 > 255 / 2
        # Run
        res = spam.filters.binaryDilation(im1Bin)
        # Test that it work without problem
        self.assertIsNotNone(res)
        # Test that the result make sense for the operation
        self.assertGreater(numpy.sum(res), numpy.sum(im1Bin))

    def test_binaryErosion(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        im1 = im1 * 255
        # Binarise
        im1Bin = im1 > 255 / 2
        # Run
        res = spam.filters.binaryErosion(im1Bin)
        # Test that it work without problem
        self.assertIsNotNone(res)
        # Test that the result make sense for the operation
        self.assertGreater(numpy.sum(im1Bin), numpy.sum(res))

    def test_binaryGeodesicReconstruction(self):
        # create fake image
        im = numpy.zeros((10, 10, 10), dtype=bool)
        im[:, 5, 5] = True
        im[:, 7, 5] = True

        # check if works as intended
        rec = spam.filters.binaryGeodesicReconstruction(im, marker=[0, 0], verbose=True)
        self.assertEqual(rec.sum(), im.sum())
        self.assertEqual(rec[-1, 5, 5], True)
        self.assertEqual(rec[-1, 7, 5], True)
        # test with nothing to reconstruct and list of plan
        rec = spam.filters.binaryGeodesicReconstruction(im, marker=[1, 0, 2, -1])
        self.assertEqual(rec.sum(), 0)
        # test with direct input
        marker = numpy.zeros(im.shape, dtype=bool)
        marker[0, :, :] = im[0, :, :]
        rec = spam.filters.binaryGeodesicReconstruction(im, marker=marker)
        self.assertEqual(rec.sum(), im.sum())
        self.assertEqual(rec[-1, 5, 5], True)
        self.assertEqual(rec[-1, 7, 5], True)

        # test with dmax
        rec = spam.filters.binaryGeodesicReconstruction(im, marker=marker, dmax=5)
        self.assertEqual(rec.sum(), 14)

        # check with 2D
        rec = spam.filters.binaryGeodesicReconstruction(im[:, :, 5], marker=[0, 0, 1, 0], dmax=5)
        self.assertEqual(rec.sum(), 14)

    def test_morphologicalreconstruction(self):
        # Generate single sphere
        im1 = numpy.zeros((20, 20, 20), dtype="<f8")
        spam.kalisphera.makeSphere(im1, [10, 10, 10], 5)
        # Run
        res = spam.filters.morphologicalOperations.morphologicalReconstruction(im1)
        # Test that it work without problem
        self.assertIsNotNone(res)


if __name__ == "__main__":
    unittest.main()

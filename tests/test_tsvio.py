import unittest

import numpy
import spam
import spam.helpers

# input reg
regReturns = {"Phi": numpy.random.random((4, 4)), "returnStatus": 2, "iterations": 100, "error": 100, "deltaPhiNorm": 0.1}

# input field
grid = numpy.mgrid[1:10:2, 1:5:2, 1:5:2]

fieldCoords = numpy.zeros((int(grid.shape[1] * grid.shape[2] * grid.shape[3]), 3))
fieldCoords[:, 0] = grid[0].ravel()
fieldCoords[:, 1] = grid[1].ravel()
fieldCoords[:, 2] = grid[2].ravel()

# create a random PhiField
PhiField = numpy.zeros((fieldCoords.shape[0], 4, 4))
for n in range(PhiField.shape[0]):
    PhiField[n] = numpy.eye(4)
    PhiField[n][0:3, -1] = numpy.random.uniform(0, 5, 3)

RS = numpy.full((fieldCoords.shape[0]), 2)
DF = numpy.full((fieldCoords.shape[0]), 0.0001)
IT = numpy.full((fieldCoords.shape[0]), 1)
error = numpy.full((fieldCoords.shape[0]), 0.1)
labelDilate = numpy.full((fieldCoords.shape[0]), 2)
PSCC = numpy.full((fieldCoords.shape[0]), 0.2)

# save the tsv files
TSVheader = "NodeNumber\tZpos\tYpos\tXpos\tFzz\tFzy\tFzx\tZdisp\tFyz\tFyy\tFyx\tYdisp\tFxz\tFxy\tFxx\tXdisp\treturnStatus\tdeltaPhiNorm\titerations"
TSVheaderOld = "NodeNumber\tZpos\tYpos\tXpos\tF11\tF12\tF13\tF14\tF21\tF22\tF23\tF24\tF31\tF32\tF33\tF34\tSubPixReturnStat\tSubPixDeltaPhiNorm\tSubPixIterations"
TSVheaderDiscrete = "Label\tZpos\tYpos\tXpos\tF11\tF12\tF13\tZdisp\tF21\tF22\tF23\tYdisp\tF31\tF32\tF33\tXdisp\treturnStatus\tdeltaPhiNorm\titerations\terror\tLabelDilate\tpixelSearchCC"
TSVheaderDiscreteSimple = "Label\tZpos\tYpos\tXpos\tF11\tF12\tF13\tZdisp\tF21\tF22\tF23\tYdisp\tF31\tF32\tF33\tXdisp\treturnStatus\tdeltaPhiNorm\titerations"

outMatrix = numpy.array(
    [
        numpy.array(range(PhiField.shape[0])),
        fieldCoords[:, 0],
        fieldCoords[:, 1],
        fieldCoords[:, 2],
        PhiField[:, 0, 0],
        PhiField[:, 0, 1],
        PhiField[:, 0, 2],
        PhiField[:, 0, 3],
        PhiField[:, 1, 0],
        PhiField[:, 1, 1],
        PhiField[:, 1, 2],
        PhiField[:, 1, 3],
        PhiField[:, 2, 0],
        PhiField[:, 2, 1],
        PhiField[:, 2, 2],
        PhiField[:, 2, 3],
        RS,
        DF,
        IT,
    ]
).T

outMatrixDiscrete = numpy.array(
    [
        numpy.array(range(PhiField.shape[0])),
        fieldCoords[:, 0],
        fieldCoords[:, 1],
        fieldCoords[:, 2],
        PhiField[:, 0, 0],
        PhiField[:, 0, 1],
        PhiField[:, 0, 2],
        PhiField[:, 0, 3],
        PhiField[:, 1, 0],
        PhiField[:, 1, 1],
        PhiField[:, 1, 2],
        PhiField[:, 1, 3],
        PhiField[:, 2, 0],
        PhiField[:, 2, 1],
        PhiField[:, 2, 2],
        PhiField[:, 2, 3],
        RS,
        DF,
        IT,
        error,
        labelDilate,
        PSCC,
    ]
).T
outMatrixDiscreteSimple = numpy.array(
    [
        numpy.array(range(PhiField.shape[0])),
        fieldCoords[:, 0],
        fieldCoords[:, 1],
        fieldCoords[:, 2],
        PhiField[:, 0, 0],
        PhiField[:, 0, 1],
        PhiField[:, 0, 2],
        PhiField[:, 0, 3],
        PhiField[:, 1, 0],
        PhiField[:, 1, 1],
        PhiField[:, 1, 2],
        PhiField[:, 1, 3],
        PhiField[:, 2, 0],
        PhiField[:, 2, 1],
        PhiField[:, 2, 2],
        PhiField[:, 2, 3],
        RS,
        DF,
        IT,
    ]
).T


class TestAll(spam.helpers.TestSpam):
    def test_writeRegistrationTSV(self):
        # case 1: 3D images
        spam.helpers.writeRegistrationTSV("spamReg.tsv", numpy.random.uniform(10, 20, 3), regReturns)

        # case 2: 2D images + old format
        regReturns["PhiCentre"] = regReturns["Phi"]
        del regReturns["Phi"]
        regReturns["iterationNumber"] = regReturns["iterations"]
        del regReturns["iterations"]
        spam.helpers.writeRegistrationTSV("spamReg2D.tsv", numpy.random.uniform(10, 20, 2), regReturns)

    def test_writeStrainTSV(self):
        import spam.deformation
        import spam.mesh

        # first 1D case as from bagi
        # numpy.random.seed(1)
        pointsRef = numpy.random.randint(-20, 20, (20, 3)).astype("<f4")
        connectivity = spam.mesh.triangulate(pointsRef, alpha=-1)
        displacements = numpy.random.random((20, 3)).astype("<f4")
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, displacements)
        decomposedFfield = spam.deformation.decomposeFfield(Ffield, ["dev", "vol", "r", "z", "e", "U"])
        # Positions will be the first point of the connectivity matrix,
        spam.helpers.writeStrainTSV("spam-strain.tsv", numpy.mean(pointsRef[connectivity[:]], axis=1), decomposedFfield, startRow=0)

        # Now read it back
        TSV = numpy.genfromtxt("spam-strain.tsv", delimiter="\t", names=True)
        self.assertAlmostEqual(numpy.mean(TSV["vol"]) - numpy.mean(decomposedFfield["vol"]), 0, places=4)

    def test_readCorrelationTSV(self):
        # case 0: wrong tsv name
        f0 = spam.helpers.readCorrelationTSV(fileName="sampReg.tsv")
        # case 1: read registration tsv
        spam.helpers.writeRegistrationTSV("spamReg.tsv", numpy.random.uniform(10, 20, 3), regReturns)
        f1 = spam.helpers.readCorrelationTSV(fileName="spamReg.tsv")
        self.assertAlmostEqual(regReturns["Phi"][0:3, -1].sum(), f1["PhiField"][0][0:3, -1].sum(), places=4)
        self.assertEqual(regReturns["returnStatus"], f1["returnStatus"])
        self.assertEqual(regReturns["deltaPhiNorm"], f1["deltaPhiNorm"])

        # case 1b: read registration tsv with binning 2
        f1b = spam.helpers.readCorrelationTSV(fileName="spamReg.tsv", fieldBinRatio=2)
        self.assertAlmostEqual(f1b["PhiField"][0][0:3, -1].sum(), regReturns["Phi"][0:3, -1].sum() * 2, places=4)

        # case 2: read regularGrid tsv file
        numpy.savetxt("spamPhiField.tsv", outMatrix, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheader)
        f2 = spam.helpers.readCorrelationTSV(fileName="spamPhiField.tsv")
        self.assertEqual([5, 2, 2], f2["fieldDims"].tolist())
        self.assertAlmostEqual(fieldCoords[:, 1].sum(), f2["fieldCoords"][:, 1].sum(), places=4)
        self.assertAlmostEqual(PhiField.sum(), f2["PhiField"].sum(), places=4)
        self.assertAlmostEqual(RS.sum(), f2["returnStatus"].sum(), places=4)
        self.assertAlmostEqual(DF.sum(), f2["deltaPhiNorm"].sum(), places=4)

        # case 2b: read PhiField tsv file with 'F14' instead of 'Zdisp'
        numpy.savetxt("spamPhiFieldOld.tsv", outMatrix, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderOld)
        f2b = spam.helpers.readCorrelationTSV(fileName="spamPhiFieldOld.tsv")
        self.assertAlmostEqual(PhiField[:][0:3, -1].sum(), f2b["PhiField"][:][0:3, -1].sum(), places=4)

        numpy.savetxt("spamPhiFieldOldReg.tsv", numpy.array([outMatrix[0, :]]), fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderOld)
        f2c = spam.helpers.readCorrelationTSV(fileName="spamPhiFieldOldReg.tsv")
        self.assertAlmostEqual(PhiField[0][0:3, -1].sum(), f2c["PhiField"][0][0:3, -1].sum(), places=4)

        # case 3: read discrete tsv file
        numpy.savetxt("spamPhiFieldDiscrete.tsv", outMatrixDiscrete, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderDiscrete)
        f3 = spam.helpers.readCorrelationTSV(fileName="spamPhiFieldDiscrete.tsv", readConvergence=True, readError=True, readLabelDilate=True, readPixelSearchCC=True)
        self.assertEqual(20, f3["numberOfLabels"])
        self.assertEqual([0, 0, 0], f3["fieldDims"])
        self.assertAlmostEqual(PhiField.sum(), f3["PhiField"].sum(), places=4)
        self.assertAlmostEqual(error.sum(), f3["error"].sum(), places=4)
        self.assertAlmostEqual(PSCC.sum(), f3["pixelSearchCC"].sum(), places=4)
        self.assertAlmostEqual(labelDilate.sum(), f3["LabelDilate"].sum(), places=4)

        # case 4a: read simple tsv file with all the options (but no data)
        numpy.savetxt("spamPhiFieldDiscreteSimple.tsv", outMatrixDiscreteSimple, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderDiscreteSimple)
        f4 = spam.helpers.readCorrelationTSV(fileName="spamPhiFieldDiscreteSimple.tsv", readConvergence=True, readError=True, readLabelDilate=True, readPixelSearchCC=True)
        self.assertEqual(20, f4["numberOfLabels"])
        self.assertEqual([0, 0, 0], f4["fieldDims"])
        self.assertAlmostEqual(PhiField.sum(), f4["PhiField"].sum(), places=4)

        # case 4b: read one-line tsv file with all the options (but no data)
        numpy.savetxt("spamPhiFieldDiscreteSimpleLine.tsv", numpy.array([outMatrixDiscreteSimple[0, :]]), fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderDiscreteSimple)
        f5 = spam.helpers.readCorrelationTSV(fileName="spamPhiFieldDiscreteSimpleLine.tsv", readConvergence=True, readError=True, readLabelDilate=True, readPixelSearchCC=True)
        self.assertEqual(1, f5["numberOfLabels"])
        self.assertEqual([1, 1, 1], f5["fieldDims"])
        self.assertAlmostEqual(PhiField[0][0:3, -1].sum(), f5["PhiField"][0][0:3, -1].sum(), places=4)

    def test_TSVtoTIFF(self):
        import tifffile

        # case 1: read a DICregularGrid result
        numpy.savetxt("spamPhiField.tsv", outMatrix, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheader)
        spam.helpers.TSVtoTIFF("./spamPhiField.tsv", returnRS=True)
        self.assertAlmostEqual(tifffile.imread("./spamPhiField-Zdisp.tif")[0, 0, 0], PhiField[0, 0, -1], places=4)

        # case 2: read a DICdiscrete result
        labelled = numpy.zeros((10, 10, 10))
        labelled[1, 1, 3] = 1
        tifffile.imwrite("./labelled.tif", labelled)
        numpy.savetxt("spamPhiFieldDiscrete.tsv", outMatrixDiscrete, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderDiscrete)
        spam.helpers.TSVtoTIFF("./spamPhiFieldDiscrete.tsv", lab="./labelled.tif", returnRS=True)
        self.assertAlmostEqual(tifffile.imread("./spamPhiFieldDiscrete-Zdisp.tif")[1, 1, 3], PhiField[1, 0, -1], places=4)

        # case 2b: read a DICdiscrete result without a labelled image
        spam.helpers.TSVtoTIFF("./spamPhiFieldDiscrete.tsv")

    def test_TSVtoVTK(self):
        import spam.helpers

        # case 1: read a DICregularGrid result
        numpy.savetxt("spamPhiField.tsv", outMatrix, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheader)
        spam.helpers.TSVtoVTK("./spamPhiField.tsv", returnRS=True)
        self.assertAlmostEqual(spam.helpers.readStructuredVTK("./spamPhiField.vtk")["displacements"][10].sum(), PhiField[10, :-1, 3].sum(), places=4)

        # case 2: read a DICdiscrete result
        numpy.savetxt("spamPhiFieldDiscrete.tsv", outMatrixDiscrete, fmt="%.7f", delimiter="\t", newline="\n", comments="", header=TSVheaderDiscrete)
        spam.helpers.TSVtoVTK("./spamPhiFieldDiscrete.tsv", returnRS=True)
        self.assertAlmostEqual(spam.helpers.readStructuredVTK("./spamPhiFieldDiscrete.vtk")["displacements"][9].sum(), PhiField[10, :-1, 3].sum(), places=4)

    def test_readStrainTSV(self):
        import spam.deformation
        import spam.mesh

        # first 1D case as from bagi
        # numpy.random.seed(1)
        pointsRef = numpy.random.randint(-50, 50, (20, 3)).astype("<f4")
        connectivity = spam.mesh.triangulate(pointsRef, alpha=-1)
        displacements = numpy.random.random((20, 3)).astype("<f4")
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, displacements)
        decomposedFfield = spam.deformation.decomposeFfield(Ffield, ["dev", "vol", "devss", "volss", "r", "z", "e", "U"])
        # Positions will be the first point of the connectivity matrix,
        spam.helpers.writeStrainTSV("spam-strain.tsv", numpy.mean(pointsRef[connectivity[:]], axis=1), decomposedFfield, startRow=0)

        # Use the function to read the TSV file
        TSV = spam.helpers.readStrainTSV("spam-strain.tsv")
        # Check
        self.assertAlmostEqual(numpy.mean(TSV["vol"]) - numpy.mean(decomposedFfield["vol"]), 0, places=4)
        self.assertAlmostEqual(numpy.mean(TSV["dev"]) - numpy.mean(decomposedFfield["dev"]), 0, places=4)
        self.assertAlmostEqual(numpy.mean(TSV["devss"]) - numpy.mean(decomposedFfield["devss"]), 0, places=4)
        self.assertAlmostEqual(numpy.mean(TSV["volss"]) - numpy.mean(decomposedFfield["volss"]), 0, places=4)
        for i in range(3):
            self.assertAlmostEqual(numpy.mean(TSV["r"][i]) - numpy.mean(decomposedFfield["r"][i]), 0, places=4)
            self.assertAlmostEqual(numpy.mean(TSV["z"][i]) - numpy.mean(decomposedFfield["z"][i]), 0, places=4)
            for j in range(3):
                self.assertAlmostEqual(numpy.mean(TSV["e"][i][j]) - numpy.mean(decomposedFfield["e"][i][j]), 0, places=4)
                self.assertAlmostEqual(numpy.mean(TSV["U"][i][j]) - numpy.mean(decomposedFfield["U"][i][j]), 0, places=4)

        # Check that it doesn't run for a wrong file
        spam.helpers.readStrainTSV("wrongFile")


if __name__ == "__main__":
    unittest.main()

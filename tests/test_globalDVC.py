import unittest

import numpy
import spam.DIC

# import spam.datasets
# import spam.deformation
# import spam.DIC
# import spam.filters
# import spam.helpers
# import spam.kalisphera
# import spam.label
import spam.mesh

numpy.set_printoptions(precision=3)


class TestAll(spam.helpers.TestSpam):
    def test_helperFunctions(self):

        #################
        # surfaceLabels #
        #################
        # test cuboid
        lengths = [32.55, 72.1, 15.7]
        lc = 10
        origin = [-4.2, 12.5, 78.01]
        points, connectivity = spam.mesh.createCuboid(lengths, lc, origin=origin)

        surfaces = ["z_start", "z_end", "y_start", "y_end", "x_start", "x_end"]
        labels = spam.DIC.surfaceLabels(points, surfaces=surfaces, connectivity=connectivity)

        for i in range(len(surfaces) + 1):
            self.assertTrue(i in labels, msg=f"{surfaces[i - 1]} not found.")

        for zyx, label in [(zyx, label) for zyx, label in zip(points, labels) if label]:
            d = (label - 1) // 2  # z, y, x
            position = origin[d] if label % 2 else origin[d] + lengths[d]
            self.assertAlmostEqual(position, zyx[d])

        # test cylinder
        centre = [-24, 27]
        radius = 12.5
        height = 97.3
        lc = 5
        zOrigin = -4.2
        points, connectivity = spam.mesh.createCylinder(centre, radius, height, lc, zOrigin=zOrigin)

        surfaces = ["z_start", "z_end", "z_lateral"]
        labels = spam.DIC.surfaceLabels(points, surfaces=surfaces, connectivity=connectivity)

        for i in range(len(surfaces) + 1):
            self.assertTrue(i in labels, msg=f"{surfaces[i - 1]} not found.")

        for zyx, label in [(zyx, label) for zyx, label in zip(points, labels) if label]:
            # print(zyx, label)

            if label in [1, 2]:
                position = zOrigin if label == 1 else zOrigin + height
                self.assertAlmostEqual(position, zyx[0])
            elif label in [3]:
                distance = ((zyx[1] - centre[0]) ** 2 + (zyx[2] - centre[1]) ** 2) ** 0.5
                self.assertAlmostEqual(distance, radius)

        #######################
        # projection matrices #
        #######################
        dirichlet = [
            (1, [0, 1, 2], None),
            (3, [1, 2], None),
        ]
        Dm, Ds, Ls = spam.DIC.projectionMatrices(points, connectivity, labels, dirichlet=dirichlet)

        for D, (d, dofs, _) in zip(Ds, dirichlet):
            ndof = len(dofs) * sum(labels == d)
            self.assertEqual(ndof, sum(D.ravel()))

        ##################
        # isochoric field#
        ##################
        kMag, v = spam.DIC.isochoricField(points, periods=3, connectivity=connectivity)
        for value in v[numpy.where(labels == 1), :][0]:
            self.assertAlmostEqual(value[0], 0, 3)
            self.assertAlmostEqual(value[1], -0.514, 3)
            self.assertAlmostEqual(value[2], -0.514, 3)

        for value in v[numpy.where(labels == 2), :][0]:
            self.assertAlmostEqual(value[0], 0, 3)
            self.assertAlmostEqual(value[1], -0.514, 3)
            self.assertAlmostEqual(value[2], -0.514, 3)

    # def test_globalCorrelation(self):
    #
    #     # load reference image
    #     snowRef = spam.datasets.loadSnow()
    #
    #     # create mesh
    #     margin = [5, 10, 10]
    #     meshCharacteristicLength = 15
    #     points, connectivity = spam.mesh.createCuboid(
    #         [
    #             snowRef.shape[0] - 2 * margin[0],
    #             snowRef.shape[1] - 2 * margin[1],
    #             snowRef.shape[2] - 2 * margin[2],
    #         ],  # lengths
    #         meshCharacteristicLength,
    #         origin=margin,
    #     )
    #
    #     # test 1: simple translation
    #     transformation = {"t": [1.3, 0.5, -0.1]}
    #     Phi = spam.deformation.computePhi(transformation)
    #     snowDef = spam.DIC.applyPhi(snowRef, Phi=Phi)
    #
    #     # 1.1: no initial displacement
    #     displacements = spam.DIC.globalCorrelation(
    #         snowRef,
    #         snowDef,
    #         points,
    #         connectivity,
    #         maxIterations=10,
    #         # debugFiles=True
    #     )
    #
    #     # remove noisy displacements from the border
    #     # borders = [
    #     #     [points[:, i].min() for i in range(3)],  # min x, y, z
    #     #     [points[:, i].max() for i in range(3)],  # max x, y, z
    #     # ]
    #     #
    #     # disp_bulk = []
    #     #
    #     # def is_point_on_border(point, borders):
    #     #     is_min = [point[i] - 1e-6 <= borders[0][i] for i in range(3)]
    #     #     is_max = [point[i] + 1e-6 >= borders[1][i] for i in range(3)]
    #     #     return any(is_min + is_max)
    #     #
    #     # for disp, point in zip(displacements, points):
    #     #
    #     #     if is_point_on_border(point, borders):
    #     #         continue
    #     #
    #     #     # node note on border
    #     #     disp_bulk.append(disp)
    #
    #     # test median displacements matches imposed translation
    #     medians = numpy.median(displacements, axis=0)
    #     self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))
    #
    #     # 1.2: with initial displacement (half the solution)
    #     initialDisplacements = numpy.zeros_like(points)
    #     for i in range(3):
    #         initialDisplacements[i] = 0.5 * transformation["t"][i]
    #     displacements = spam.DIC.globalCorrelation(
    #         snowRef,
    #         snowDef,
    #         points,
    #         connectivity,
    #         initialDisplacements=initialDisplacements,
    #     )
    #     # test median displacements matches imposed translation
    #     medians = numpy.median(displacements, axis=0)
    #     self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))
    #
    #     # test 2: test bulk regularisation
    #     regularisation = {
    #         "MESH": {"type": "cuboid"},
    #         "BULK": {"young": 25, "poisson": 0.25, "ksi": 2},
    #         "DIRICHLET": {"ksi": {"z_start": 2, "z_end": 2, "y_start": None}},
    #     }
    #
    #     displacements = spam.DIC.globalCorrelation(
    #         snowRef,
    #         snowDef,
    #         points,
    #         connectivity,
    #         regularisation=regularisation,
    #     )
    #     # test median displacements matches imposed translation
    #     medians = numpy.median(displacements, axis=0)
    #     self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))
    #
    #     spam.helpers.writeUnstructuredVTK(points, connectivity, pointData={"displacements": displacements})


if __name__ == "__main__":
    spam.helpers.TestSpam.DEBUG = True
    unittest.main()

# -*- coding: utf-8 -*-

import unittest
import numpy, random
import spam.orientations
import spam.helpers
import pytest

class TestAll(spam.helpers.TestSpam):

    def test_meanOrientation(self):
        # Generate random main direction
        alpha = random.randrange(30, 330, 1)
        theta = random.randrange(30, 90, 1)
        # Generate n random vector near the main direction
        n = 1000 #number of vectors
        R = 10 #Radius of vectors
        vect = numpy.zeros((n, 3))
        for x in range(n):
            alphaI = numpy.random.normal(numpy.radians(alpha), numpy.radians(5))
            thetaI = numpy.random.normal(numpy.radians(theta), numpy.radians(5))
            RI = numpy.random.normal(R, 2)
            vect[x,0] = RI*numpy.cos(thetaI)
            vect[x,1] = RI*numpy.sin(thetaI)*numpy.sin(alphaI)
            vect[x,2] = RI*numpy.sin(thetaI)*numpy.cos(alphaI)
        orientationVector = spam.orientations.meanOrientation(vect)
        thetaSVD = numpy.degrees(numpy.arccos(orientationVector[0]))
        alphaSVD = numpy.degrees(numpy.arctan2(orientationVector[1], orientationVector[2]))
        if alphaSVD<0: alphaSVD = 360 + alphaSVD
        # Check if the difference between the angles is less than 5 degree
        self.assertLess(numpy.abs(thetaSVD - theta), 5)
        self.assertLess(numpy.abs(alphaSVD - alpha), 5)

    def test_fabricTensor(self):
        # Define number of vector
        n = 1000
        # Create equally-spaced vector
        vectEq = spam.orientations.generateIsotropic(n)
        # Compute fabric
        NEq,FEq,aEq = spam.orientations.fabricTensor(vectEq)
        # Create a set of vectors using a normal distribution centered on a random orientation
        alpha = random.randrange(30, 330, 1)
        theta = random.randrange(30, 90, 1)
        # Create the main direction orientation
        mu = numpy.reshape([numpy.cos(numpy.radians(theta)),
                            numpy.sin(numpy.radians(theta))*numpy.sin(numpy.radians(alpha)),
                            numpy.sin(numpy.radians(theta))*numpy.cos(numpy.radians(alpha))],(1,3))
        # Set a value of Kappa
        K = 5
        # Set number of vectors
        N = 10000
        # Generate the distribution
        orientations = spam.orientations.generateVonMisesFisher(mu = mu, kappa = K, N = N)
        # Compute fabric
        NRand, FRand, aRand = spam.orientations.fabricTensor(orientations)
        # Test the results
        self.assertGreater(aRand, aEq)

    def test_projectVector(self):
        # 0. Usual test for input data
        res = spam.orientations.projectOrientation([0,1], 'cartesian', 'lambert')
        self.assertEqual(res, None)
        res = spam.orientations.projectOrientation([0,1], 'x', 'lambert')
        self.assertEqual(res, None)
        res = spam.orientations.projectOrientation([0,1], 'cartesian', 'x')
        self.assertEqual(res, None)

        # 1. Cartesian coord
        # Lambert projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([0,1,0], 'cartesian', 'lambert')
        # The radius should be sqrt(2)
        self.assertAlmostEqual(theta[1], numpy.sqrt(2), places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), numpy.sqrt(2), places=3)
        # Stereo projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([0,1,0], 'cartesian', 'stereo')
        # The radius should be 1
        self.assertAlmostEqual(theta[1], 1, places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), 1, places=3)
        # Equidistant projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([0,1,0], 'cartesian', 'equidistant')
        # The radius should be 1
        self.assertAlmostEqual(theta[1], 1, places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), 1, places=3)

        # 2. Spherical coordinates
        # Lambert projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([1,0.5*numpy.pi,numpy.pi], 'spherical', 'lambert')
        # The radius should be sqrt(2)
        self.assertAlmostEqual(theta[1], numpy.sqrt(2), places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), numpy.sqrt(2), places=3)
        # Stereo projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([1,0.5*numpy.pi,numpy.pi], 'spherical', 'stereo')
        # The radius should be 1
        self.assertAlmostEqual(theta[1], 1, places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), 1, places=3)
        # Equidistant projection on the edge of the disk
        xy, theta = spam.orientations.projectOrientation([1,0.5*numpy.pi,numpy.pi], 'spherical', 'equidistant')
        # The radius should be 1
        self.assertAlmostEqual(theta[1], 1, places=3)
        self.assertAlmostEqual(numpy.sqrt((xy[0])**2 +(xy[1])**2), 1, places=3)

    def test_generateVonMisesFisher(self):
        #TODO: Check that the imposed KAPPA is a single value
        # 1. Check for the shape of initial orientation vector
        vect = numpy.array([2,1])
        orientations = spam.orientations.generateVonMisesFisher(mu = vect, kappa = 1, N = 10)
        self.assertEqual(orientations, None)
        # 2. Check for value of kappa
        vect = numpy.array([2,1,1])
        #with pytest.raises("spam.orientations.fitVonMisesFisher: The vectors must be an array of Nx3"):
        self.assertRaises(AssertionError, spam.orientations.generateVonMisesFisher, mu = vect, kappa = 0, N = 10)

        # 2. Check for number of vectors
        vect = numpy.array([2,1,1])
        self.assertRaises(AssertionError, spam.orientations.generateVonMisesFisher,mu = vect, kappa = 1, N = 1)

        # 3. Check for main orientation and Kappa
        # Generate random main direction
        alpha = random.randrange(30, 330, 1)
        theta = random.randrange(30, 90, 1)
        # Create the main direction orientation
        mu = numpy.reshape([numpy.cos(numpy.radians(theta)),
                            numpy.sin(numpy.radians(theta))*numpy.sin(numpy.radians(alpha)),
                            numpy.sin(numpy.radians(theta))*numpy.cos(numpy.radians(alpha))],(1,3))
        # Set a value of Kappa
        K = random.randrange(5, 100, 1)
        # Set number of vectors
        N = 10000
        # Generate the distribution
        orientations = spam.orientations.generateVonMisesFisher(mu = mu, kappa = K, N = N)
        # Compute the mean orietation of the subset
        orientationVector = spam.orientations.meanOrientation(orientations)
        thetaSVD = numpy.degrees(numpy.arccos(orientationVector[0]))
        alphaSVD = numpy.degrees(numpy.arctan2(orientationVector[1], orientationVector[2]))
        if alphaSVD<0: alphaSVD = 360 + alphaSVD
        # Check if the difference between the angles is less than 5 degree
        self.assertLess(numpy.abs(thetaSVD - theta), 5)
        self.assertLess(numpy.abs(alphaSVD - alpha), 5)

    def test_fitVonMisesFisher(self):
        # 1. Check that the vectors are 3D
        orientations = numpy.random.rand(10,5)
        self.assertRaises(AssertionError, spam.orientations.fitVonMisesFisher, orientations)
        # 2. Check for the confidence intervals
        orientations = numpy.random.rand(10,3)
        self.assertRaises(AssertionError, spam.orientations.fitVonMisesFisher, orientations, confVMF = 2)

        self.assertRaises(AssertionError, spam.orientations.fitVonMisesFisher, orientations, confMu = 2)

        self.assertRaises(AssertionError, spam.orientations.fitVonMisesFisher, orientations, confKappa = 2)

        # 3. Check for main orientation and Kappa
        # Generate random main direction
        alpha = random.randrange(30, 330, 1)
        theta = random.randrange(30, 90, 1)
        # Create the main direction orientation
        mu = numpy.reshape([numpy.cos(numpy.radians(theta)),
                            numpy.sin(numpy.radians(theta))*numpy.sin(numpy.radians(alpha)),
                            numpy.sin(numpy.radians(theta))*numpy.cos(numpy.radians(alpha))],(1,3))
        # Set a value of Kappa
        K = random.randrange(5, 100, 1)
        # Set number of vectors
        N = 10000
        # Generate the distribution
        orientations = spam.orientations.generateVonMisesFisher(mu = mu, kappa = K, N = N)
        # Fit the vectors
        res = spam.orientations.fitVonMisesFisher(orientations)
        # Check alpha and theta
        self.assertLess(numpy.abs(alpha - res['alpha']), 2)
        self.assertLess(numpy.abs(theta - res['theta']), 2)

    def test_generateIsotropic(self):
        # 1. Check for non-int and negative values of number of vectors
        self.assertRaises(AssertionError, spam.orientations.generateIsotropic, -1)
        self.assertRaises(AssertionError, spam.orientations.generateIsotropic, 4.5)
        self.assertRaises(AssertionError, spam.orientations.generateIsotropic, 'a')

        # 2. Check that it runs and the output makes sense
        vectors = spam.orientations.generateIsotropic(10000)
        _,_,a = spam.orientations.fabricTensor(vectors)
        self.assertLess(a, 0.05)

    def test_generateIcosphere(self):
        # 1. Check for non-int and negative values of number of subDiv
        self.assertRaises(AssertionError, spam.orientations.generateIcosphere, -1)
        self.assertRaises(AssertionError, spam.orientations.generateIcosphere, 4.5)
        self.assertRaises(AssertionError, spam.orientations.generateIcosphere, 'a')

        # 2. Check that it runs and the output makes sense
        vert, face, vect = spam.orientations.generateIcosphere(3)
        _,_,a = spam.orientations.fabricTensor(vert)
        self.assertLess(a, 0.01)
        _,_,a = spam.orientations.fabricTensor(vect)
        self.assertLess(a, 0.01)



if __name__ == '__main__':
        unittest.main()

import os
import subprocess
import unittest

import numpy
import spam.datasets
import spam.deformation
import spam.DIC
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile

# This for hiding script output
FNULL = open(os.devnull, "w")

# This for showing it
# FNULL = None

# We also check 2D slice so don't displace in Z or rotate around an axis other than Z
refTranslation = numpy.array([0.0, 15, 0.0])
refRotation = numpy.array([3.0, 0.0, 0.0])


class TestAll(spam.helpers.TestSpam):

    # def __init__(self, *args, **kwargs):
    #     super(testAll, self).__init__(*args, **kwargs)
    #     coverage.process_startup()
    def test_gridDICtools(self):
        #######################################################
        # Step 1 generate files
        #######################################################
        # Run on snow data, with bin2 registration, without output
        # load 3D image
        snowRef = spam.datasets.loadSnow()
        snowRef2D = snowRef[50]
        # save it locally
        tifffile.imwrite("snow-ref.tif", snowRef)
        os.chmod("snow-ref.tif", 0o666)
        tifffile.imwrite("snow-ref-2D.tif", snowRef2D)
        os.chmod("snow-ref-2D.tif", 0o666)

        # Generate deformed image
        Phi = spam.deformation.computePhi({"t": refTranslation, "r": refRotation})
        snowDef = spam.DIC.applyPhi(snowRef, Phi=Phi)
        # save it locally
        tifffile.imwrite("snow-def.tif", snowDef)
        os.chmod("snow-def.tif", 0o666)
        # Plus 2D version
        # tifffile.imwrite("snow-def-2D.tif", spam.DIC.applyPhiPython(snowRef2D[numpy.newaxis, ...], Phi=Phi))
        tifffile.imwrite("snow-def-2D.tif", snowDef[50])
        os.chmod("snow-def-2D.tif", 0o666)

        snowDefOnlyDisp = spam.DIC.applyPhi(snowRef, Phi=spam.deformation.computePhi({"t": refTranslation}))
        # save it locally
        tifffile.imwrite("snow-def-onlyDisp.tif", snowDefOnlyDisp)
        os.chmod("snow-def.tif", 0o666)

        # Mask for pixel search
        snowMask = numpy.ones_like(snowDef, dtype="<u1")
        snowMask[0] = 0
        snowMask[-1] = 0
        snowMask2D = numpy.ones_like(snowRef2D, dtype="<u1")
        snowMask2D[0] = 0
        snowMask2D[-1] = 0
        tifffile.imwrite("snow-mask-ref.tif", snowMask)
        os.chmod("snow-mask-ref.tif", 0o666)
        tifffile.imwrite("snow-mask-ref-2D.tif", snowMask2D)
        os.chmod("snow-mask-ref-2D.tif", 0o666)
        tifffile.imwrite("snow-mask-def.tif", (snowDef > 0).astype("u1"))
        os.chmod("snow-mask-def.tif", 0o666)
        # tifffile.imwrite("snow-mask-2D.tif", snowMask[50])
        # os.chmod("snow-mask-2D.tif", 0o666)

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"t": 0.8 * refTranslation, "r": 0.8 * refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg-onlyRot.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"r": refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg-onlyDisp.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"t": refTranslation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close in 2D
        spam.helpers.writeRegistrationTSV(
            "snow-grid-2D-ereg.tsv",
            [0, snowRef.shape[1] - 1 / 2, snowRef.shape[2] - 1 / 2],
            {
                "Phi": spam.deformation.computePhi({"r": 0.8 * refRotation, "t": 0.8 * refTranslation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close in 2D
        spam.helpers.writeRegistrationTSV(
            "snow-grid-2D-ereg-onlyRot.tsv",
            [0, snowRef.shape[1] - 1 / 2, snowRef.shape[2] - 1 / 2],
            {
                "Phi": spam.deformation.computePhi({"r": refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        #######################################################
        # Step 2 check spam-reg functionality
        #######################################################
        # Just run a simple registration, with a guess from "ereg"
        exitCode = subprocess.call(
            [
                "spam-reg",
                "-bb",
                "2",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid",
                "-mar",
                "20",
                "-pf",
                "snow-grid-ereg.tsv",
                "-mf1",
                "snow-mask-ref.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        regResult = spam.helpers.readCorrelationTSV("snow-grid-registration.tsv")
        transformation = spam.deformation.decomposePhi(regResult["PhiField"][0])
        # print(transformation['t'])
        # print(transformation['r'])
        self.assertTrue(numpy.allclose(refTranslation, transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(refRotation, transformation["r"], atol=0.01))

        # Just run a simple registration in 2D!
        exitCode = subprocess.call(
            [
                "spam-reg",
                "-bb",
                "2",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-2D",
                "-mar",
                "20",
                "-pf",
                "snow-grid-2D-ereg.tsv",
                "-mf1",
                "snow-mask-ref-2D.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        reg2dResult = spam.helpers.readCorrelationTSV("snow-grid-2D-registration.tsv")
        transformation2d = spam.deformation.decomposePhi(reg2dResult["PhiField"][0])
        # print(transformation['t'])
        # print(transformation['r'])
        self.assertTrue(numpy.allclose(refTranslation, transformation2d["t"], atol=0.01))
        self.assertTrue(numpy.allclose(refRotation, transformation2d["r"], atol=0.01))

        #######################################################
        # Step 3 check spam-pixelSearch functionality
        #######################################################
        # Step 3a load initial guess and just search around it...
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-registration.tsv",
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3a",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3a = spam.helpers.readCorrelationTSV("snow-grid-3a-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3a["pixelSearchCC"][PSresult3a["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3a["PhiField"][PSresult3a["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3b load initial ONLY ROTATION guess and do a big search around the applied displacement
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-ereg-onlyRot.tsv",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3b",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-mf2",
                "snow-mask-def.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3b = spam.helpers.readCorrelationTSV("snow-grid-3b-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3b["pixelSearchCC"][PSresult3b["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3b["PhiField"][PSresult3b["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3c load "only rotation" result from above and just do a +-1 search around it
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-3b-pixelSearch.tsv",
                "-sr",
                # "-3", "3", "-3", "3", "-3", "3",
                "-1",
                "1",
                "-1",
                "1",
                "-1",
                "1",
                # "0", "0", "0", "0", "0", "0",
                "-glt",
                "5000",
                "-hws",
                "10",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3c",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-mf2",
                "snow-mask-def.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3c = spam.helpers.readCorrelationTSV("snow-grid-3c-pixelSearch.tsv", readPixelSearchCC=True)
        print(numpy.median(PSresult3c["pixelSearchCC"][PSresult3c["returnStatus"] == 1]))
        self.assertTrue(0.95 < numpy.median(PSresult3c["pixelSearchCC"][PSresult3c["returnStatus"] == 1]))
        # print(
        #     PSresult3b['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1,
        #     PSresult3c['returnStatus']==1),0:3,-1]-PSresult3c['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1,
        #     PSresult3c['returnStatus']==1),0:3,-1]
        # )
        self.assertTrue(
            0
            == numpy.sum(
                PSresult3b["PhiField"][numpy.logical_and(PSresult3b["returnStatus"] == 1, PSresult3c["returnStatus"] == 1)]
                - PSresult3c["PhiField"][numpy.logical_and(PSresult3b["returnStatus"] == 1, PSresult3c["returnStatus"] == 1)]
            )
        )

        # Step "3d"!!! check 2D mode
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-2D-ereg-onlyRot.tsv",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "5",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-tif",
                "-pre",
                "snow-grid-2d3d",
                "-mf1",
                "snow-mask-ref-2D.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3d = spam.helpers.readCorrelationTSV("snow-grid-2d3d-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.90 < numpy.nanmedian(PSresult3d["pixelSearchCC"][PSresult3d["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3d["PhiField"][PSresult3d["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3e NO ROTATION IMAGE big search around the applied displacement, this allows us to check the measured values of displacement
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3e",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3e = spam.helpers.readCorrelationTSV("snow-grid-3e-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3e["pixelSearchCC"][PSresult3e["returnStatus"] == 1]))
        # Check correct values measured
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 2, -1]),
                atol=0.1,
            )
        )

        # 3f - use spam-passPhiField
        # Now use spam-passPhiField to apply registration to node-spaced-grid and use it to run a tight pixel-search
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-registration.tsv",
                "-ns",
                "20",
                "-im1",
                "snow-ref.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3f",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3f = spam.helpers.readCorrelationTSV("snow-grid-3f-passed-ns20.tsv")
        self.assertTrue(numpy.isclose(0, numpy.median(PSresult3f["PhiField"][:, 0, -1]), atol=1.0))

        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-3f-passed-ns20.tsv",
                "-sr",
                "-1",
                "1",
                "-1",
                "1",
                "-1",
                "1",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3g",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3g = spam.helpers.readCorrelationTSV("snow-grid-3g-pixelSearch.tsv", readPixelSearchCC=True)
        self.assertTrue(0.95 < numpy.median(PSresult3g["pixelSearchCC"][PSresult3g["returnStatus"] == 1]))
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3g["PhiField"][PSresult3g["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )
        # self.assertTrue(0 == numpy.sum(  PSresult3b['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1, PSresult3g['returnStatus']==1)]
        # - PSresult3g['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1, PSresult3g['returnStatus']==1)]))

        #######################################################
        # Step 4 check spam-pixelSearchPropagate
        #######################################################
        # check only with dip
        exitCode = subprocess.call(
            [
                "spam-pixelSearchPropagate",
                "-sp",
                "{}".format(snowRef.shape[0] // 2),
                "{}".format(snowRef.shape[1] // 2),
                "{}".format(snowRef.shape[2] // 2),
                "{}".format(int(refTranslation[0])),
                "{}".format(int(refTranslation[1])),
                "{}".format(int(refTranslation[2])),
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-4a",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        PSresult4a = spam.helpers.readCorrelationTSV("snow-grid-4a-pixelSearchPropagate.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult4a["pixelSearchCC"][PSresult4a["returnStatus"] == 1]))

        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 2, -1]),
                atol=0.1,
            )
        )

        #######################################################
        # Step 5 check spam-ldic loading previous results
        #######################################################

        # Step 5a: load registration
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-registration.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "10",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5a",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-tif",
                "-o",
                "3",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5a = spam.helpers.readCorrelationTSV("snow-grid-5a-ldic.tsv")
        dims = LDICresult5a["fieldDims"]

        # Did more than 80% of points converge?
        returnStatusCropped = LDICresult5a["returnStatus"].reshape(dims)[2:-2, 2:-2, 2:-2]
        self.assertTrue(0.8 < numpy.sum(returnStatusCropped == 2) / returnStatusCropped.size)

        # median Rotation within 0.5 deg?
        PhiFieldCropped = LDICresult5a["PhiField"].reshape(dims[0], dims[1], dims[2], 4, 4)[2:-2, 2:-2, 2:-2, :, :]
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(spam.deformation.decomposePhiField(PhiFieldCropped[returnStatusCropped == 2], components="r")["r"][:, 0]),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PhiFieldCropped[returnStatusCropped == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step 5b: load pixelSearch results with same grid + ug
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-3a-pixelSearch.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5b",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-ug",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5b = spam.helpers.readCorrelationTSV("snow-grid-5b-ldic.tsv")

        # Did more than 80% of points converge?
        self.assertTrue(0.8 < numpy.sum(LDICresult5b["returnStatus"] == 2) / len(LDICresult5b["returnStatus"]))

        # median Rotation within 0.5 deg?
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(
                    spam.deformation.decomposePhiField(LDICresult5b["PhiField"][LDICresult5b["returnStatus"] > 0], components="r",)[
                        "r"
                    ][:, 0]
                ),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5b["PhiField"][LDICresult5b["returnStatus"] == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step "5c" check 2D mode
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-2D-registration.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "5",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-tif",
                "-pre",
                "snow-grid-2d",
                "-mf1",
                "snow-mask-ref-2D.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5c = spam.helpers.readCorrelationTSV("snow-grid-2d-ldic.tsv")

        # Did more than 50% of points converge?
        self.assertTrue(0.5 < numpy.sum(LDICresult5c["returnStatus"] == 2) / len(LDICresult5c["returnStatus"]))

        # median Rotation within 0.5 deg?
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(
                    spam.deformation.decomposePhiField(LDICresult5c["PhiField"][LDICresult5c["returnStatus"] > 0], components="r",)[
                        "r"
                    ][:, 0]
                ),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5c["PhiField"][LDICresult5c["returnStatus"] == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step 5d: load pixelSearchPropagate results with same grid + ug
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-4a-pixelSearchPropagate.tsv",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5d",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5d = spam.helpers.readCorrelationTSV("snow-grid-5d-ldic.tsv")

        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 2, -1]),
                atol=0.1,
            )
        )

        # Step 5e: try to filter this field based on return status, and interpolate
        exitCode = subprocess.call(
            [
                "spam-filterPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-srs",
                "-srst",
                "1",
                "-cint",
                "-F",
                "all",
                "-pre",
                "snow-grid-5e",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        LDICresult5e = spam.helpers.readCorrelationTSV("snow-grid-5e-filtered.tsv")
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # Test 5f: use -regs to subtract registration from ldic.
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5a-ldic.tsv",
                "-regs",
                "snow-grid-registration.tsv",
                "-pre",
                "snow-grid-5f",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5f = spam.helpers.readCorrelationTSV("snow-grid-5f-passed-regs-rigid.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5g Same with the full Phi (no difference here)
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5a-ldic.tsv",
                "-regs",
                "snow-grid-registration.tsv",
                "-regsF",
                "all",
                "-pre",
                "snow-grid-5g",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5g = spam.helpers.readCorrelationTSV("snow-grid-5g-passed-regs-all.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5h Same with only displacements
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-regs",
                "snow-grid-ereg-onlyDisp.tsv",
                "-regsF",
                "no",
                "-pre",
                "snow-grid-5h",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5h = spam.helpers.readCorrelationTSV("snow-grid-5h-passed-regs-disp.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5i 2D check
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-2d-ldic.tsv",
                "-regs",
                "snow-grid-2D-registration.tsv",
                "-regsF",
                "rigid",
                "-pre",
                "snow-grid-5i",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5i = spam.helpers.readCorrelationTSV("snow-grid-5i-passed-regs-rigid.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5j Check -skp

        # Use spam-passPhiField to filter the result based on the RS

        exitCode = subprocess.call(
            [
                "spam-filterPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-srs",
                "-cint",
                "-F",
                "no",
                "-od",
                ".",
                "-pre",
                "snow-grid-5j-Guess",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Run ldic with -skp
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-5j-Guess-filtered.tsv",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5j",
                "-skp",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Read the filtered file
        tsvPre = spam.helpers.readCorrelationTSV("snow-grid-5j-Guess-filtered.tsv", readError=1)
        # Get the converged nodes
        convNodes = numpy.where(tsvPre["returnStatus"] == 2)[0]
        # Read the last file
        tsvPost = spam.helpers.readCorrelationTSV("snow-grid-5j-ldic.tsv", readError=1)
        # Check that the general information is preserved
        self.assertTrue((tsvPre["fieldDims"] == tsvPost["fieldDims"]).all())
        self.assertTrue((tsvPre["fieldCoords"] == tsvPost["fieldCoords"]).all())
        # Check the converged nodes
        for node in convNodes:
            # Check returnStatus
            self.assertTrue(tsvPre["returnStatus"][node] == tsvPost["returnStatus"][node])
            # Check deltaPhiNorm
            self.assertTrue(tsvPre["deltaPhiNorm"][node] == tsvPost["deltaPhiNorm"][node])
            # Check iterations
            self.assertTrue(tsvPre["iterations"][node] == tsvPost["iterations"][node])
            # Check error
            self.assertTrue(tsvPre["error"][node] == tsvPost["error"][node])
            # Check PhiField
            self.assertTrue((tsvPre["PhiField"][node] == tsvPost["PhiField"][node]).all())

        #######################################################
        # Step 6 onto the regularStrain
        #######################################################
        # Step 6a Now let's calculate rotation vector with Geers elements (radius=1) and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "r",
                "-r",
                "1",
                "-od",
                ".",
                "-pre",
                "snow-grid-6a",
                "-tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # Now read output
        strain6a = spam.helpers.readStrainTSV("snow-grid-6a-strain-Geers.tsv")
        dims6a = [
            strain6a["fieldDims"][0],
            strain6a["fieldDims"][1],
            strain6a["fieldDims"][2],
            3,
        ]

        # Check each rotation component while having a 2-pixel crop of the boundaries
        for i in range(3):
            # print(numpy.nanmean(strain6a['r'].reshape(dims6a)[2:-2, 2:-2, 2:-2, i]))
            self.assertTrue(
                numpy.isclose(
                    numpy.nanmean(strain6a["r"].reshape(dims6a)[2:-2, 2:-2, 2:-2, i]),
                    refRotation[i],
                    atol=0.5,
                )
            )

        # Step 6b Now let's calculate small and large strains with Q8 elements and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "--Q8",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "U",
                "e",
                "vol",
                "volss",
                "dev",
                "devss",
                "-vtk",
                "-od",
                ".",
                "-pre",
                "snow-grid-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        strain6b = spam.helpers.readStrainTSV("snow-grid-6b-strain-Q8.tsv")
        dims6b = [
            strain6b["fieldDims"][0],
            strain6b["fieldDims"][1],
            strain6b["fieldDims"][2],
            3,
            3,
        ]
        # Check small strains, should be ~0
        self.assertTrue(
            numpy.allclose(
                numpy.nanmean(strain6b["e"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2)),
                numpy.zeros((3, 3)),
                atol=0.02,
            )
        )

        Umean6b = numpy.nanmean(strain6b["U"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2))
        self.assertTrue(numpy.allclose(Umean6b, numpy.eye(3), atol=0.02))

        # dev and vol both ~0
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["vol"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["volss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["dev"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["devss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )

        # VTK6b = spam.helpers.readStructuredVTK("snow-grid-6b-strain-Q8.vtk")
        # for component in ["U", "e", "vol", "volss", "dev", "devss"]:
        # self.assertTrue(numpy.nanmean(strain6b[component]), numpy.nanmean(VTK6b[component]), places=2)

        # Step 6b Now let's calculate small and large strains with Q8 elements and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "-raw",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "U",
                "e",
                "vol",
                "volss",
                "dev",
                "devss",
                "-vtk",
                "-od",
                ".",
                "-pre",
                "snow-grid-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        strain6b = spam.helpers.readStrainTSV("snow-grid-6b-strain-raw.tsv")
        dims6b = [
            strain6b["fieldDims"][0],
            strain6b["fieldDims"][1],
            strain6b["fieldDims"][2],
            3,
            3,
        ]
        # Check small strains, should be ~0
        self.assertTrue(
            numpy.allclose(
                numpy.nanmean(strain6b["e"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2)),
                numpy.zeros((3, 3)),
                atol=0.02,
            )
        )

        Umean6b = numpy.nanmean(strain6b["U"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2))
        self.assertTrue(numpy.allclose(Umean6b, numpy.eye(3), atol=0.02))

        # dev and vol both ~0
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["vol"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["volss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["dev"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["devss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )


if __name__ == "__main__":
    unittest.main()

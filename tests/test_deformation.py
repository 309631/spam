import subprocess
import unittest

import numpy
import scipy.ndimage
import spam.datasets
import spam.deformation
import spam.DIC
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile


class TestAll(spam.helpers.TestSpam):
    def test_computePhi(self):
        # U MUST be symmetric, make sure this is checked and sets off an assertion
        badU = numpy.eye(3)
        badU[2, 1] = 5
        self.assertRaises(AssertionError, spam.deformation.computePhi, {"U": badU})

        trans1 = {"t": [0.0, 3.0, 3.0]}
        trans2 = {"r": [-5.0, 0.0, 0.0]}
        trans3 = {"z": [2, 2, 2]}
        trans4 = {"s": [0.9, 0.8, 0.7]}
        Phi1 = spam.deformation.computePhi(trans1)
        self.assertEqual(numpy.sum([Phi1[0, -1], Phi1[1, -1], Phi1[2, -1]]), 6)
        Phi2 = spam.deformation.computePhi(trans2)
        self.assertEqual(numpy.sum([Phi2[0, -1], Phi2[1, -1], Phi2[2, -1]]), 0)
        Phi3 = spam.deformation.computePhi(trans2, PhiCentre=[50.0, 50.0, 50.0], PhiPoint=[50.0, 16.0, 84.0])
        self.assertAlmostEqual(numpy.sum([Phi3[0, -1], Phi3[1, -1], Phi3[2, -1]]), 5.926, places=2)
        Phi4 = spam.deformation.computePhi(trans3)
        self.assertEqual([Phi4[0, 0], Phi4[1, 1], Phi4[2, 2]], [2.0, 2.0, 2.0])
        Phi5 = spam.deformation.computePhi(trans4)
        self.assertEqual(Phi5[0, 1], Phi5[1, 0], 0.9)
        self.assertEqual(Phi5[0, 2], Phi5[2, 0], 0.8)
        self.assertEqual(Phi5[1, 3], Phi5[3, 1], 0.7)
        Phi6 = spam.deformation.computePhi({"U": numpy.eye(3)})
        self.assertEqual(Phi6.tolist(), numpy.eye(4).tolist())
        U = numpy.eye(3)
        U[0, 0] = 0.8
        Phi7 = spam.deformation.computePhi({"U": U})
        self.assertEqual(Phi7[0, 0], 0.8)

    def test_decomposePhi(self):
        # CASE 1: singular Phi
        Phi = numpy.zeros((4, 4))
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phi)["t"]).sum(), 3)
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phi)["r"]).sum(), 3)

        # CASE 2: Phi contains nans
        Phin = numpy.eye(4)
        Phin[0, 3] = numpy.nan
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phin)["t"]).sum(), 3)
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phin)["r"]).sum(), 3)

        # CASE 3: Phi contains inf
        Phii = numpy.eye(4)
        Phii[0, 3] = numpy.inf
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phii)["t"]).sum(), 3)
        self.assertEqual(numpy.isnan(spam.deformation.decomposePhi(Phii)["r"]).sum(), 3)

        # CASE 4: negative eigenvalues in transpose(Phi).Phi
        Phib = numpy.array(
            [
                [
                    0.25383295079467,
                    -0.028793281703941,
                    0.05805266499330,
                    10.722113444672508,
                ],
                [
                    -0.03543852689277,
                    0.004282141971071,
                    -0.008072359472034,
                    18.14869851125678,
                ],
                [
                    0.041196242920521,
                    -0.004961945218488,
                    0.009385858417623,
                    17.144152186875889,
                ],
                [0, 0, 0, 1],
            ]
        )
        self.assertAlmostEqual(
            spam.deformation.decomposePhi(Phib)["t"].tolist(),
            Phib[0:3, -1].tolist(),
            places=5,
        )

        # CASE 5: error in inverting U to create the rotation matrix
        # it doesn't seem to pass the test on gricad...
        # Phib = numpy.array([[0.000000001, 0.000000001, 0.000000001, 1],
        # [0.000000001, 0.000000001, 0.000000001, 1],
        # [0.000000001, 0.0, 0.000000001, 1],
        # [0, 0, 0, 1]])
        # self.assertEqual(spam.deformation.decomposePhi(Phib)["t"], [0,0,0])

        # CASE 6: back calculation of transformation
        transIn = {"t": [4.6, 11.2, 0.3], "r": [-5.0, 2.0, 8.0]}
        a = numpy.random.uniform(1, 100, 3)
        b = numpy.random.uniform(1, 100, 3)
        Phi = spam.deformation.computePhi(transIn, PhiCentre=a, PhiPoint=b)
        transBack = spam.deformation.decomposePhi(Phi, PhiCentre=b, PhiPoint=a)
        self.assertTrue(numpy.linalg.norm(numpy.subtract(transBack["t"], transIn["t"])) < 1e-5)
        self.assertTrue(numpy.linalg.norm(numpy.subtract(transBack["r"], transIn["r"])) < 1e-5)

    def test_Q8(self):
        # case 0: 2D field with nans under large strains
        dispField0 = numpy.zeros((4, 3))
        dispField0[:, 1] = [20, 20, -20, numpy.nan]  # dipsY
        dispField0[:, 2] = [20, -20, 20, numpy.nan]  # dipsX

        F0 = spam.deformation.FfieldRegularQ8(dispField0.reshape(1, 2, 2, 3), nodeSpacing=[0, 100, 100], verbose = 1)
        decomposedF = spam.deformation.decomposeF(F0)
        self.assertTrue(numpy.isnan((decomposedF["U"]).sum()))  # return nan

        # case 0b: 2D field with nans under small strains
        self.assertTrue(numpy.isnan((decomposedF["e"]).sum()))  # return nan

        # case 1: 2D field of 1 square with isotropic compression under large strains
        dispField1 = numpy.zeros((4, 3))
        dispField1[:, 1] = [20, 20, -20, -20]  # dipsY
        dispField1[:, 2] = [20, -20, 20, -20]  # dipsX

        Vo1 = 100 * 100  # initial volume
        Vf1 = 60 * 60  # final volume

        F1 = spam.deformation.FfieldRegularQ8(dispField1.reshape(1, 2, 2, 3), nodeSpacing=[0, 100, 100], verbose=False)
        decomposedF1 = spam.deformation.decomposeF(F1[0, 0, 0], twoD=True)

        self.assertAlmostEqual(decomposedF1["vol"], (Vf1 - Vo1) / float(Vo1), places=3)  # volStrain should be the volume change
        self.assertAlmostEqual(decomposedF1["dev"], 0, places=6)  # devStrain must be 0

        # case 1b: 2D field of 1 rectangle with isotropic compression under small strains
        dispField1b = numpy.zeros((4, 3))
        dispField1b[:, 1] = [1, 1, -1, -1]  # dipsY
        dispField1b[:, 2] = [1, -1, 1, -1]  # dipsX

        Vo1b = 100 * 100  # initial volume
        Vf1b = 98 * 98  # final volume

        F1b = spam.deformation.FfieldRegularQ8(dispField1b.reshape(1, 2, 2, 3), nodeSpacing=[0, 100, 100], verbose=False)
        decomposedF1b = spam.deformation.decomposeF(F1b[0, 0, 0], twoD=True)

        self.assertAlmostEqual(decomposedF1b["volss"], (Vf1b - Vo1b) / float(Vo1b), places=3)  # volStrain should be the volume change
        self.assertEqual(decomposedF1b["devss"], 0)  # devStrain must be 0

        # case 2: 3D field of 1 cell (2x2x2nodes) with isotropic compression under large strains
        dispField2 = numpy.zeros((8, 3))
        dispField2[:, 0] = [20, 20, 20, 20, -20, -20, -20, -20]  # dipsZ
        dispField2[:, 1] = [20, 20, -20, -20, 20, 20, -20, -20]  # dipsY
        dispField2[:, 2] = [20, -20, 20, -20, 20, -20, 20, -20]  # dipsX

        Vo2 = 100 * 100 * 100  # initial volume
        Vf2 = 60 * 60 * 60  # final volume

        F2 = spam.deformation.FfieldRegularQ8(dispField2.reshape(2, 2, 2, 3), nodeSpacing=[100, 100, 100], verbose=False)
        decomposedF2 = spam.deformation.decomposeF(F2[0, 0, 0])
        self.assertAlmostEqual(decomposedF2["vol"], (Vf2 - Vo2) / float(Vo2), places=3)  # volStrain should be the volume change
        self.assertAlmostEqual(decomposedF2["dev"], 0, places=6)  # devStrain must be 0

        # case 2b: 3D field of 1 cell (2x2x2nodes) with isotropic compression under small strains
        dispField2b = numpy.zeros((8, 3))
        dispField2b[:, 0] = [1, 1, 1, 1, -1, -1, -1, -1]  # dipsZ
        dispField2b[:, 1] = [1, 1, -1, -1, 1, 1, -1, -1]  # dipsY
        dispField2b[:, 2] = [1, -1, 1, -1, 1, -1, 1, -1]  # dipsX

        Vo2b = 100 * 100 * 100  # initial volume
        Vf2b = 98 * 98 * 98  # final volume

        F2b = spam.deformation.FfieldRegularQ8(dispField2b.reshape(2, 2, 2, 3), nodeSpacing=[100, 100, 100], verbose=False)
        decomposedF2b = spam.deformation.decomposeF(F2b[0, 0, 0])
        self.assertAlmostEqual(decomposedF2b["volss"], (Vf2b - Vo2b) / float(Vo2b), places=2)
        self.assertEqual(decomposedF2b["devss"], 0)

        # case 3: 2D field of 1 rectangle with shear under large strains
        dispField3 = numpy.zeros((4, 3))
        dispField3[:, 2] = [0, 0, 20, 20]  # dipsX

        F3 = spam.deformation.FfieldRegularQ8(dispField3.reshape(1, 2, 2, 3), nodeSpacing=[0, 100, 100], verbose=False)
        decomposedF3 = spam.deformation.decomposeF(F3[0, 0, 0], twoD=True)
        self.assertEqual(decomposedF3["vol"], 0)  # volStrain must be 0
        self.assertAlmostEqual(decomposedF3["U"][1, 2], decomposedF3["U"][2, 1], places=4)  # diag strain matrix
        self.assertAlmostEqual(abs(decomposedF3["U"][1, 2] - ((20 / 100.0) * 0.5)), 0, places=3)

        # case 3b: 2D field of 1 rectangle with shear under small strains
        dispField3b = numpy.zeros((4, 3))
        dispField3b[:, 2] = [0, 0, 2, 2]  # dipsX

        F3b = spam.deformation.FfieldRegularQ8(dispField3b.reshape(1, 2, 2, 3), nodeSpacing=[0, 100, 100], verbose=False)
        decomposedF3b = spam.deformation.decomposeF(F3b[0, 0, 0], twoD=True)
        self.assertEqual(decomposedF3b["volss"], 0)  # volStrain must be 0
        self.assertEqual(decomposedF3b["e"][1, 2], decomposedF3b["e"][2, 1])  # diag strain matrix
        self.assertEqual(decomposedF3b["e"][1, 2], (2 / 100.0) * 0.5)

        # case 4: 3D field with shear under large strains
        dispField4 = numpy.zeros((8, 3))
        dispField4[:, 2] = [0, 0, 20, 20, 0, 0, 20, 20]  # dipsX

        F4 = spam.deformation.FfieldRegularQ8(dispField4.reshape(2, 2, 2, 3), nodeSpacing=[100, 100, 100], verbose=False)
        decomposedF4 = spam.deformation.decomposeF(F4[0, 0, 0])
        self.assertEqual(decomposedF4["vol"], 0)  # volStrain must be 0
        self.assertAlmostEqual(decomposedF4["U"][1, 2], decomposedF4["U"][2, 1], places=4)  # diag strain matrix
        self.assertAlmostEqual(abs(decomposedF4["U"][1, 2] - (20 / 100.0) * 0.5), 0, places=3)

        # case 4b: 3D field with shear under small strains
        dispField4b = numpy.zeros((8, 3))
        dispField4b[:, 2] = [0, 0, 2, 2, 0, 0, 2, 2]  # dipsX

        F4b = spam.deformation.FfieldRegularQ8(dispField4b.reshape(2, 2, 2, 3), nodeSpacing=[100, 100, 100], verbose=False)
        decomposedF4b = spam.deformation.decomposeF(F4b[0, 0, 0])
        self.assertEqual(decomposedF4b["volss"], 0)  # volStrain must be 0
        self.assertEqual(decomposedF4b["e"][1, 2], decomposedF4b["e"][2, 1])  # diag strain matrix
        self.assertEqual(decomposedF4b["e"][1, 2], (2 / 100.0) * 0.5)

        # nodeSpacing = [150,150,150]
        nodeSpacing = [200, 200, 200]
        imSize = [2000, 1000, 1000]
        # Random points
        pointsRef, dims = spam.DIC.grid.makeGrid(imSize, nodeSpacing=nodeSpacing)

        ######################################################
        # Dilation of points for grain strain calc
        #######################################################
        # Test with 1% dilation
        dilation = 1.01

        # make sure average volumetric strain is the same as the projected one
        pointsDef = pointsRef * dilation
        displacements = (pointsDef - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)

        ev = spam.deformation.decomposeFfield(Ffield, ["vol"], verbose=False)["vol"]
        # print("\tU trace mean new:",numpy.array(U)[:,[0,1,2],[0,1,2]].mean())
        # print("\tev",numpy.array(ev))
        # print("\tev",numpy.array(ev).mean())

        self.assertAlmostEqual(ev.mean(), 3.0 * (dilation - 1), places=3)
        # self.assertAlmostEqual(ev.mean(), 3. * (dilation - 1), places=3)

        ########################################################
        # Z-stretch of points for strain calc
        ########################################################
        zStretch = 1.02
        # print("\n\nSpreading points in Z by by 1.2")
        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        displacements = (pointsRef * [zStretch, 1.0, 1.0] - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)
        # only compute on internal nodes, we know there's a problem at the edges
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 0, 0].mean(), zStretch, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 1, 1].mean(), 1.0, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 2, 2].mean(), 1.0, places=3)

        ########################################################
        # Y-stretch of points for strain calc
        ########################################################
        yStretch = 1.02
        # print("\n\nSpreading points in Y by by 1.2")
        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        displacements = (pointsRef * [1.0, yStretch, 1.0] - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)
        # only compute on internal nodes, we know there's a problem at the edges
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 0, 0].mean(), 1.0, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 1, 1].mean(), yStretch, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 2, 2].mean(), 1.0, places=3)

        #######################################################
        # Rotation of points for grain strain calc
        #######################################################
        pointsRotated = pointsRef.copy()
        rotAngle = 3.0
        for n, point in enumerate(pointsRef):
            Phi = spam.deformation.computePhi(
                {"r": [rotAngle, 0.0, 0.0]},
                PhiCentre=numpy.array(((numpy.array(imSize) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsRotated[n] += Phi[0:3, -1]
            # print( point, pointsRotated[n], '\n\n' )

        displacements = (pointsRotated - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)

        # Compute strains
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["r", "vol", "dev"], verbose=False)

        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 0].mean(), rotAngle, places=3)
        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 1].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 2].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["vol"][1:-1, 1:-1, 1:-1].mean(), 0, places=3)
        self.assertAlmostEqual(decomposedF["dev"][1:-1, 1:-1, 1:-1].mean(), 0, places=3)

        #######################################################
        # Shear of points for grain strain calc:
        #   N.B.: this is a shear which results in a SYMMETRIC F
        #######################################################
        pointsRotated = pointsRef.copy()
        shearVal = 0.1
        for n, point in enumerate(pointsRef):
            Phi = spam.deformation.computePhi(
                {"s": [shearVal, 0.0, 0.0]},
                PhiCentre=numpy.array(((numpy.array(imSize) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsRotated[n] += Phi[0:3, -1]
            # print( point, pointsRotated[n], '\n\n' )

        displacements = (pointsRotated - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)

        # Compute strains
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["U"], verbose=False)

        UmeanWithoutBoundaries = numpy.mean(decomposedF["U"][1:-1, 1:-1, 1:-1], axis=(0, 1, 2))
        for i in range(3):
            for j in range(i, 3):
                self.assertAlmostEqual(UmeanWithoutBoundaries[i, j], UmeanWithoutBoundaries[j, i], places=3)

        #######################################################
        # Homogeneous shrinking together
        #######################################################
        shrink = 0.99
        # Deform the points by pushing them closer together -- this is compressing so  negative volumetric strain
        displacements = (pointsRef * shrink - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularQ8(displacements, nodeSpacing, verbose=False)
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["vol"], verbose=False)
        self.assertAlmostEqual(decomposedF["vol"][1:-1, 1:-1, 1:-1].mean(), 3.0 * (shrink - 1.0), places=3)

    def test_geers(self):
        nodeSpacing = [100, 100, 100]
        imSize = [2000, 1000, 1000]
        # 2D
        nodeSpacing2D = [1, 50, 50]
        imSize2D = [1, 1000, 1000]

        # Random points
        pointsRef, dims = spam.DIC.makeGrid(imSize, nodeSpacing=nodeSpacing)
        pointsRef2D, dims2D = spam.DIC.makeGrid(imSize2D, nodeSpacing=nodeSpacing2D)

        ########################################################
        # Dilation of points for grain strain calc
        #######################################################
        # Test with 1% dilation
        dilation = 1.01

        # make sure average volumetric strain is the same as the projected one
        pointsDef = pointsRef * dilation
        pointsDef2D = pointsRef2D * dilation
        displacements = (pointsDef - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        displacements2D = (pointsDef2D - pointsRef2D).reshape(dims2D[0], dims2D[1], dims2D[2], 3)

        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, verbose=True)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D, verbose=False)

        ev = spam.deformation.decomposeFfield(Ffield, ["vol"], verbose=False)["vol"]
        ev2D = spam.deformation.decomposeFfield(Ffield2D, ["vol"], verbose=False)["vol"]

        self.assertAlmostEqual(ev.mean(), 3.0 * (dilation - 1), places=3)
        self.assertAlmostEqual(ev2D.mean(), 2.0 * (dilation - 1), places=3)

        #########################################################
        # Z-stretch of points for strain calc
        #########################################################
        zStretch = 1.02

        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        displacements = (pointsRef * [zStretch, 1.0, 1.0] - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, verbose=False)

        # only compute on internal nodes, we know there's a problem at the edges
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 0, 0].mean(), zStretch, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 1, 1].mean(), 1.0, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 2, 2].mean(), 1.0, places=3)

        ########################################################
        # Y-stretch of points for strain calc
        ########################################################
        yStretch = 1.02

        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        displacements = (pointsRef * [1.0, yStretch, 1.0] - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        displacements2D = (pointsRef2D * [1.0, yStretch, 1.0] - pointsRef2D).reshape(dims2D[0], dims2D[1], dims2D[2], 3)
        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, verbose=False)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D, verbose=False)

        # only compute on internal nodes, we know there's a problem at the edges
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 0, 0].mean(), 1.0, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 1, 1].mean(), yStretch, places=3)
        self.assertAlmostEqual(Ffield[1:-1, 1:-1, 1:-1, 2, 2].mean(), 1.0, places=3)
        self.assertAlmostEqual(Ffield2D[:, 1:-1, 1:-1, 1, 1].mean(), yStretch, places=3)
        self.assertAlmostEqual(Ffield2D[:, 1:-1, 1:-1, 2, 2].mean(), 1.0, places=3)

        ########################################################
        # Rotation of points for grain strain calc
        ########################################################
        pointsRotated = pointsRef.copy()
        pointsRotated2D = pointsRef2D.copy()
        rotAngle = 3.0
        for n, point in enumerate(pointsRef):
            Phi = spam.deformation.computePhi(
                {"r": [rotAngle, 0.0, 0.0]},
                PhiCentre=numpy.array(((numpy.array(imSize) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsRotated[n] += Phi[0:3, -1]
            # print( point, pointsRotated[n], '\n\n' )

        for n, point in enumerate(pointsRef2D):
            Phi = spam.deformation.computePhi(
                {"r": [rotAngle, 0.0, 0.0]},
                PhiCentre=numpy.array(((numpy.array(imSize2D) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsRotated2D[n] += Phi[0:3, -1]
            # print( point, pointsRotated[n], '\n\n' )

        displacements = (pointsRotated - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        displacements2D = (pointsRotated2D - pointsRef2D).reshape(dims2D[0], dims2D[1], dims2D[2], 3)
        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, verbose=False)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D, verbose=False)

        # Compute strains
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["r", "vol", "dev"], verbose=False)
        decomposedF2D = spam.deformation.decomposeFfield(Ffield2D, ["r", "vol", "dev"], verbose=False)

        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 0].mean(), rotAngle, places=3)
        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 1].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["r"][1:-1, 1:-1, 1:-1, 2].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["vol"][1:-1, 1:-1, 1:-1].mean(), 0, places=3)
        self.assertAlmostEqual(decomposedF["dev"][1:-1, 1:-1, 1:-1].mean(), 0, places=3)
        self.assertAlmostEqual(decomposedF2D["r"][:, 1:-1, 1:-1, 0].mean(), rotAngle, places=3)
        self.assertAlmostEqual(decomposedF2D["r"][:, 1:-1, 1:-1, 1].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF2D["r"][:, 1:-1, 1:-1, 2].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF2D["vol"][:, 1:-1, 1:-1].mean(), 0, places=3)
        self.assertAlmostEqual(decomposedF2D["dev"][:, 1:-1, 1:-1].mean(), 0, places=3)

        ########################################################
        # Shear of points for grain strain calc:
        #   N.B.: this is a shear which results in a SYMMETRIC F
        ########################################################
        pointsSheared = pointsRef.copy()
        shearVal = 0.1
        for n, point in enumerate(pointsRef):
            Phi = spam.deformation.computePhi(
                {"s": [shearVal, 0.0, 0.0]},
                PhiCentre=numpy.array(((numpy.array(imSize) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsSheared[n] += Phi[0:3, -1]

        pointsSheared2D = pointsRef2D.copy()
        for n, point in enumerate(pointsRef2D):
            Phi = spam.deformation.computePhi(
                {"s": [0.0, 0.0, shearVal]},
                PhiCentre=numpy.array(((numpy.array(imSize2D) - 1) / 2) - 1),
                PhiPoint=point,
            )
            pointsSheared2D[n] += Phi[0:3, -1]

        displacements = (pointsSheared - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        displacements2D = (pointsSheared2D - pointsRef2D).reshape(dims2D[0], dims2D[1], dims2D[2], 3)

        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D)

        # Compute strains
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["U"])
        decomposedF2D = spam.deformation.decomposeFfield(Ffield2D, ["U"])

        UmeanWithoutBoundaries = numpy.mean(decomposedF["U"][1:-1, 1:-1, 1:-1], axis=(0, 1, 2))
        UmeanWithoutBoundaries2D = numpy.mean(decomposedF2D["U"][0, 1:-1, 1:-1], axis=(0, 1))

        for i in range(3):
            for j in range(i, 3):
                self.assertAlmostEqual(UmeanWithoutBoundaries[i, j], UmeanWithoutBoundaries[j, i], places=3)
        self.assertAlmostEqual(UmeanWithoutBoundaries[0, 1], shearVal, places=3)

        for i in range(3):
            for j in range(i, 3):
                self.assertAlmostEqual(
                    UmeanWithoutBoundaries2D[i, j],
                    UmeanWithoutBoundaries2D[j, i],
                    places=3,
                )
        self.assertAlmostEqual(UmeanWithoutBoundaries2D[2, 1], shearVal, places=3)

        ########################################################
        # Homogeneous shrinking together
        ########################################################
        shrink = 0.99
        # Deform the points by pushing them closer together -- this is compressing so  negative volumetric strain
        displacements = (pointsRef * shrink - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing)
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["vol"])
        self.assertAlmostEqual(decomposedF["vol"][1:-1, 1:-1, 1:-1].mean(), 3.0 * (shrink - 1.0), places=3)
        import tifffile

        tifffile.imwrite("/tmp/vol.tif", decomposedF["vol"])

        ########################################################
        # Increasing neighbourhood radius should decrease noise
        ########################################################
        # Add noise to displacements
        displacementAmplitude = displacements.max() - displacements.min()
        displacements += numpy.random.random(displacements.shape) * 0.1 * displacementAmplitude
        FfieldR1 = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, neighbourRadius=1)
        decomposedFR1 = spam.deformation.decomposeFfield(FfieldR1, ["vol"])
        tifffile.imwrite("/tmp/decomposedFR1.tif", decomposedFR1["vol"])

        FfieldR1p5 = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, neighbourRadius=1.5)
        decomposedFR1p5 = spam.deformation.decomposeFfield(FfieldR1p5, ["vol"])
        tifffile.imwrite("/tmp/decomposedFR1p5.tif", decomposedFR1p5["vol"])

        FfieldR2 = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, neighbourRadius=2)
        decomposedFR2 = spam.deformation.decomposeFfield(FfieldR2, ["vol"])
        tifffile.imwrite("/tmp/decomposedFR2.tif", decomposedFR2["vol"])

        # FfieldR3 = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing, neighbourRadius=3)
        # decomposedFR3 = spam.deformation.decomposeFfield(FfieldR3, ["vol"])
        std1 = decomposedFR1["vol"][1:-1, 1:-1, 1:-1].std()
        std1p5 = decomposedFR1p5["vol"][1:-1, 1:-1, 1:-1].std()
        std2 = decomposedFR2["vol"][1:-1, 1:-1, 1:-1].std()
        # compute stdev of vol strain (most sensitive!!) and try for three different neighbourhoodRadii
        # print("\n\n\n\n\n\n\n\n", std1, std1p5, std2)
        self.assertEqual(std1 > std1p5, True)
        self.assertEqual(std1p5 > std2, True)

        ########################################################
        # Application of mask
        ########################################################
        xStretch = 1.02
        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        displacements = (pointsRef * [1.0, 1.0, xStretch] - pointsRef).reshape(dims[0], dims[1], dims[2], 3)
        displacements2D = (pointsRef2D * [1.0, 1.0, xStretch] - pointsRef2D).reshape(dims2D[0], dims2D[1], dims2D[2], 3)

        # manually add a NaN in the middle of the displacement field
        displacements[dims[0] // 2, dims[1] // 2, dims[2] // 2] = [
            numpy.nan,
            numpy.nan,
            numpy.nan,
        ]
        displacements2D[0, dims2D[1] // 2, dims2D[2] // 2] = [
            numpy.nan,
            numpy.nan,
            numpy.nan,
        ]

        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D)
        # only compute on internal nodes, we know there's a problem at the edges
        self.assertEqual(numpy.isnan(Ffield[:, :, :, 2, 2]).sum(), 1)
        self.assertEqual(numpy.isnan(Ffield2D[:, :, :, 2, 2]).sum(), 1)

        Ffield = spam.deformation.FfieldRegularGeers(displacements, nodeSpacing)
        Ffield2D = spam.deformation.FfieldRegularGeers(displacements2D, nodeSpacing2D)
        self.assertEqual(numpy.isnan(Ffield[:, :, :, 2, 2]).sum(), 1)
        self.assertEqual(numpy.isnan(Ffield2D[:, :, :, 2, 2]).sum(), 1)

    def test_bagi(self):
        #######################################################
        # Check triangulation and strains
        #######################################################
        # New example with larger distance
        # pointsRef = numpy.array([[  0,   0,   0],
        # [100,   0,   0],
        # [  0, 100,   0],
        # [100, 100,   0],
        # [  0,   0, 100],
        # [100,   0, 100],
        # [  0, 100, 100],
        # [100, 100, 100]], dtype='<f4')

        #######################################################
        # Check connectivityFromVoronoi
        #######################################################
        # Check alpha functionality
        numpy.random.seed(1)
        pointsRef = numpy.random.randint(-20, 20, (20, 3)).astype("<f4")
        connectivity = spam.mesh.triangulate(pointsRef, alpha=-1)

        self.assertTrue(connectivity.shape[0] == 19)
        self.assertEqual(connectivity.shape[1], 4)

        # Random points
        pointsRef = numpy.random.randint(-20, 20, (20, 3)).astype("<f4")

        # Our C++ CGAL function without weights
        connectivity = spam.mesh.triangulate(pointsRef)

        # Make sure it's broken with weights of different sizes
        with self.assertRaises(Exception) as context:
            spam.mesh.triangulate(pointsRef, weights=numpy.array([2, 2]))
        self.assertTrue("weights array dim1 != points array dim1" in str(context.exception))

        # Check size of connectivity
        self.assertTrue(connectivity.shape[0] >= 6)
        self.assertEqual(connectivity.shape[1], 4)

        #######################################################
        # Dilation of points for grain strain calc
        #######################################################
        # Test with 1% dilation
        dilation = 1.01

        # make sure average volumetric strain is the same as the projected one
        pointsDef = pointsRef * dilation
        tetVolumesRef = spam.mesh.tetVolumes(pointsRef, connectivity)
        tetVolumesDef = spam.mesh.tetVolumes(pointsDef, connectivity)
        # print("volStrains@tet:", (tetVolumesDef-tetVolumesRef)/tetVolumesRef)
        self.assertAlmostEqual(
            numpy.mean((tetVolumesDef - tetVolumesRef) / tetVolumesRef),
            3.0 * (dilation - 1),
            places=3,
        )
        ####
        # Check spam.deformation.FfieldBagi
        ###
        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        # U, ev, ed = spam.deformation.FfieldBagi(points, connectivity, points - points)
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, pointsDef - pointsRef, verbose = 1)

        ev = spam.deformation.decomposeFfield(Ffield, ["vol"], verbose=True)["vol"]
        # print("\tU trace mean new:",numpy.array(U)[:,[0,1,2],[0,1,2]].mean())
        # print("\tev",numpy.array(ev))
        # print("\tev",numpy.array(ev).mean())

        self.assertAlmostEqual(ev.mean(), 3.0 * (dilation - 1), places=3)
        # print(spam.deformation.decomposeFfield(Ffield, ['vol']))
        # self.assertAlmostEqual(ev.mean(), 3. * (dilation - 1), places=3)

        ########################################################
        # Z-stretch of points for grain strain calc
        ########################################################
        zStretch = 1.02
        # print("\n\nSpreading points in Z by by 1.2")
        # Deform the points by spreading them apart -- this is dilating so positive volumetric strain
        Ffield = spam.deformation.FfieldBagi(
            pointsRef,
            connectivity,
            pointsRef * [zStretch, 1.0, 1.0] - pointsRef,
            verbose=False,
        )
        self.assertAlmostEqual(Ffield[:, 0, 0].mean(), zStretch, places=3)

        # Make sure that a single NaN does not pollute strains and is safely ignored
        pointsRefOneBadPoint = pointsRef.copy()
        badPointCoord = pointsRef.shape[0] // 2
        pointsRefOneBadPoint[badPointCoord] = numpy.nan
        Ffield = spam.deformation.FfieldBagi(
            pointsRefOneBadPoint,
            connectivity,
            pointsRef * [zStretch, 1.0, 1.0] - pointsRef,
            verbose=False,
        )
        # make sure that the nans in the strain are where there is our bad particle
        self.assertEqual(
            list(numpy.isnan(Ffield[:, 0, 0])),
            list(numpy.sum(connectivity == badPointCoord, axis=1) > 0),
        )
        # check the mean strain without the bad ones
        self.assertAlmostEqual(numpy.nanmean(Ffield[:, 0, 0]), zStretch, places=3)

        # Same as above with all strain components (F and R) to pass them to grain projector
        Ffield = spam.deformation.FfieldBagi(
            pointsRef,
            connectivity,
            pointsRef * [zStretch, 1.0, 1.0] - pointsRef,
            verbose=False,
        )
        self.assertAlmostEqual(Ffield[:, 0, 0].mean(), zStretch, places=3)

        # Project F back to grains
        grainF = spam.mesh.projectTetFieldToGrains(pointsRef * [zStretch, 1.0, 1.0], connectivity, Ffield)
        self.assertAlmostEqual(numpy.mean(grainF[:, 0, 0]), zStretch, places=3)
        self.assertAlmostEqual(numpy.mean(grainF[:, 1, 1]), 1.00, places=3)
        self.assertAlmostEqual(numpy.mean(grainF[:, 2, 2]), 1.00, places=3)

        #######################################################
        # Rotation of points for grain strain calc
        #######################################################
        pointsRotated = pointsRef.copy()
        rotAngle = 3.0
        for n, point in enumerate(pointsRef):
            pointPad = numpy.ones(4)
            pointPad[0:3] = point
            # pointsRotated[n] = point + spam.DIC.computeTransformationOperator(  {'r': [10.0,0.0,0.0]},
            Phi = spam.deformation.computePhi({"r": [rotAngle, 0.0, 0.0]}, PhiCentre=[0.0, 50.0, 50.0], PhiPoint=point)
            # pointsRotated[n] = numpy.dot(F,pointPad)[0:3]
            pointsRotated[n] += Phi[0:3, -1]
            # print( point, pointsRotated[n], '\n\n' )

        # Compute strains
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, pointsRotated - pointsRef, verbose=False)
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["r", "vol", "dev"], verbose=False)

        self.assertAlmostEqual(decomposedF["r"][:, 0].mean(), rotAngle, places=3)
        self.assertAlmostEqual(decomposedF["r"][:, 1].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["r"][:, 2].mean(), 0.0, places=3)
        self.assertAlmostEqual(decomposedF["vol"].mean(), 0, places=3)
        self.assertAlmostEqual(decomposedF["dev"].mean(), 0, places=3)

        # Project to grains
        grain_r = spam.mesh.projectTetFieldToGrains(pointsRotated, connectivity, decomposedF["r"])
        self.assertAlmostEqual(numpy.mean(grain_r[:, 0]), rotAngle, places=3)
        self.assertAlmostEqual(numpy.mean(grain_r[:, 1]), 0.00, places=3)
        self.assertAlmostEqual(numpy.mean(grain_r[:, 2]), 0.00, places=3)

        ######################################################
        # Homogeneous shrinking together
        #######################################################
        shrink = 0.99
        # Deform the points by pushing them closer together -- this is compressing so  negative volumetric strain
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, pointsRef * shrink - pointsRef, verbose=False)
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["vol"], verbose=False)
        self.assertAlmostEqual(decomposedF["vol"].mean(), 3.0 * (shrink - 1.0), places=3)

        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, pointsRef * shrink - pointsRef, verbose=False)
        # Project F back to grains
        grainF = spam.mesh.projectTetFieldToGrains(pointsRef * shrink, connectivity, Ffield)
        self.assertAlmostEqual(numpy.mean(grainF[:, 0, 0]), shrink, places=3)
        self.assertAlmostEqual(numpy.mean(grainF[:, 1, 1]), shrink, places=3)
        self.assertAlmostEqual(numpy.mean(grainF[:, 2, 2]), shrink, places=3)

        # Attempt a grid projection
        # grid_bounds, num_in_grid, gridStrain = spam.mesh.projectBagiStrainToGrid(pointsRef * shrink, connectivity, F, nx=3, ny=3, nz=3)
        # print(grid_bounds)
        # print(num_in_grid)
        # print(gridStrain)

        #######################################################
        # Move one point and see if the right elements strain
        #######################################################
        # make sure that the connectivity is well respected in the output  --  move only one point,
        #   and make sure only the elements concerned are moved.
        # Move point two:
        pointToMove = 2
        newPoints = pointsRef.copy()
        # newPoints[pointToMove] += numpy.random.randint( -10, 10, (3))
        newPoints[pointToMove] += [10, 10, 10]

        # list of numbers
        tetsWithPointToMove = numpy.where(connectivity == pointToMove)[0]
        Ffield = spam.deformation.FfieldBagi(pointsRef, connectivity, newPoints - pointsRef, verbose=False)
        decomposedF = spam.deformation.decomposeFfield(Ffield, ["dev"], verbose=False)

        # full length indexing bool array
        otherTets = numpy.ones(connectivity.shape[0], dtype=bool)
        otherTets[tetsWithPointToMove] = 0

        self.assertEqual(
            decomposedF["dev"][otherTets].tolist(),
            numpy.zeros(otherTets.sum()).tolist(),
        )
        self.assertTrue(numpy.sum(decomposedF["dev"][tetsWithPointToMove] != 0) == len(tetsWithPointToMove))

    def test_computeRigidPhi(self):
        transformation = {"t": [5.0, 0.0, 0.0], "r": [3.0, 0.0, 0.0]}
        PhiR1 = spam.deformation.computePhi(transformation)
        PhiR2 = spam.deformation.computeRigidPhi(PhiR1)
        for i in range(4):
            for j in range(4):
                self.assertAlmostEqual(PhiR1[i, j], PhiR2[i, j], places=2)

        transformation = {
            "t": [5.0, 0.0, 0.0],
            "r": [3.0, 0.0, 0.0],
            "z": [1.01, 1.01, 1.01],
        }
        PhiR1 = spam.deformation.computePhi(transformation)
        PhiR2 = spam.deformation.computeRigidPhi(PhiR1)
        for i in range(4):
            for j in range(4):
                if i == j and i < 3:
                    self.assertEqual(PhiR1[i, j] > PhiR2[i, j], True)
                else:
                    self.assertAlmostEqual(PhiR1[i, j], PhiR2[i, j], places=2)

    def test_decomposeF(self):
        # Test that it runs without problems
        F = numpy.array(([5, 2, 3], [2, 5, 1], [3, 1, 5]))
        res = spam.deformation.deformationFunction.decomposeF(F)
        self.assertIsNotNone(res)
        # Test for singular F
        F = numpy.array(([1, 2, 3], [2, 4, 6], [3, 6, 9]))
        res = spam.deformation.deformationFunction.decomposeF(F)
        self.assertTrue(numpy.isnan(res["r"][0]))
        # Test for nan
        F = numpy.array(([1, 2, 3], [2, 4, 6], [3, numpy.nan, 9]))
        res = spam.deformation.deformationFunction.decomposeF(F)
        self.assertTrue(numpy.isnan(res["r"][0]))
        # Test for inf
        F = numpy.array(([1, 2, 3], [2, 4, 6], [3, numpy.inf, 9]))
        res = spam.deformation.deformationFunction.decomposeF(F)
        self.assertTrue(numpy.isnan(res["r"][0]))

    def test_decomposePhiField(self):
        PhiField = numpy.array([numpy.eye(4) for point in range(3)])

        decomposedPhiField = spam.deformation.decomposePhiField(PhiField, ["vol", "dev", "volss", "devss", "t", "r", "z", "U", "e"], verbose = True)

        for point in range(PhiField.shape[0]):
            self.assertTrue(numpy.allclose(decomposedPhiField["t"][point], numpy.zeros(3)))
            self.assertTrue(numpy.allclose(decomposedPhiField["r"][point], numpy.zeros(3)))
            self.assertTrue(numpy.allclose(decomposedPhiField["U"][point], numpy.eye(3)))
            self.assertTrue(numpy.allclose(decomposedPhiField["vol"][point], 0.0))
            self.assertTrue(numpy.allclose(decomposedPhiField["dev"][point], 0.0))
            self.assertTrue(numpy.allclose(decomposedPhiField["e"][point], numpy.zeros((3, 3))))
            self.assertTrue(numpy.allclose(decomposedPhiField["volss"][point], 0.0))
            self.assertTrue(numpy.allclose(decomposedPhiField["devss"][point], 0.0))

    def test_getDisplacementFromNeighbours(self):

        # Create a TSV and Lab file just as in test_merge

        #######################################################
        # We're using the DDIC test from scripts here, lightly modified
        #######################################################
        # First we need to create some data using DEM dataset
        pixelSize = 0.0001
        blurSTD = 0.8
        noiseSTD = 0.01
        boxSizeDEM, centres, radii = spam.datasets.loadUniformDEMboxsizeCentreRadius()

        # put 0 in the middle
        centres -= numpy.mean(centres, axis=0)
        rMax = numpy.amax(radii)

        # pad box size
        boxSizeDEM = boxSizeDEM + 5 * rMax

        # add half box size to centres
        centres += numpy.array(boxSizeDEM) / 2.0
        boxSizePx = (boxSizeDEM / pixelSize).astype(int)
        centresPx = centres / pixelSize
        radiiPx = radii / pixelSize
        box = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(box, centresPx, radiiPx)
        box[numpy.where(box > 1.0)] = 1.0
        box[numpy.where(box < 0.0)] = 0.0
        box = box * 0.5
        box = box + 0.25
        box = scipy.ndimage.gaussian_filter(box, sigma=blurSTD)
        box = numpy.random.normal(box, scale=noiseSTD)
        binIm0 = box >= 0.5
        # Run watershed
        labIm0 = spam.label.ITKwatershed.watershed(binIm0)
        # Save images
        tifffile.imwrite("Step0.tif", box.astype("<f4"))
        tifffile.imwrite("Lab0.tif", labIm0.astype(spam.label.labelType))

        # test of rigid translation and rotation
        # Create Phi and Apply (25 px displacement on Y-axis, and 0 degree rotation along Z axis)
        translationStep1 = [5, 2, 0]
        rotationStep1 = [0, 0, 0]
        transformation = {"t": translationStep1, "r": rotationStep1}
        Phi = spam.deformation.computePhi(transformation)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]
        boxDeformed = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(boxDeformed, centresPxDeformed, radiiPx)
        boxDeformed[numpy.where(boxDeformed > 1.0)] = 1.0
        boxDeformed[numpy.where(boxDeformed < 0.0)] = 0.0
        boxDeformed = boxDeformed * 0.5
        boxDeformed = boxDeformed + 0.25
        boxDeformed = scipy.ndimage.gaussian_filter(boxDeformed, sigma=blurSTD)
        boxDeformed = numpy.random.normal(boxDeformed, scale=noiseSTD)
        # Save images
        tifffile.imwrite("Step1.tif", boxDeformed.astype("<f4"))

        #######################################################
        # Now use the ddic and ldic
        #######################################################
        exitCode = subprocess.call(["spam-reg", "-bb", "2", "-be", "2", "Step0.tif", "Step1.tif", "-od", "."])
        self.assertEqual(exitCode, 0)
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-glt",
                "0.5",
                "-hws",
                "10",
                "-ns",
                "10",
                # "-it", "5",
                "-pf",
                "Step0-Step1-registration.tsv",
                "Step0.tif",
                "Step1.tif",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-ld",
                "2",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        # Read lab file and TSV from previous code
        imLab = tifffile.imread("Lab0.tif")
        TSV = spam.helpers.readCorrelationTSV(
            "Step0-Step1-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        # Manually change the RS of one grain
        TSV["returnStatus"][10] = -1

        # Case 1: Run the function normally and check the displacement and F
        exitCode = spam.DIC.getDisplacementFromNeighbours(imLab, TSV, "./TSV_getDisplacementFromNeighbours.tsv", neighboursRange=3)
        print(exitCode)
        # Read the resulting TSV
        tsvRes = spam.helpers.readCorrelationTSV(
            "TSV_getDisplacementFromNeighbours.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        # Get the mean translation
        meanT = spam.deformation.deformationFunction.decomposePhi(tsvRes["PhiField"][10])["t"]
        # Check the values (The values are linked to the displacement imposed at the beginning of the code on test_merge)
        self.assertAlmostEqual(meanT[0], 5, places=2)
        self.assertAlmostEqual(meanT[1], 2, places=2)
        self.assertAlmostEqual(meanT[2], 0, places=2)
        # Check that the F matrix is equal to numpy.eye(3)
        self.assertTrue((tsvRes["PhiField"][10][:-1, :-1] == numpy.eye(3)).all())

        # Case 2: Run the function using the same TSV as the previous step to preserve F
        spam.DIC.getDisplacementFromNeighbours(imLab, TSV, "./TSV_getDisplacementFromNeighbours.tsv", previousDVC=TSV)
        # Read the resulting TSV
        tsvRes = spam.helpers.readCorrelationTSV(
            "TSV_getDisplacementFromNeighbours.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        # Get the mean translation
        meanT = spam.deformation.deformationFunction.decomposePhi(tsvRes["PhiField"][10])["t"]
        # Check the values (The values are linked to the displacement imposed at the beginning of the code on test_merge)
        numpy.testing.assert_almost_equal(meanT, [5, 2, 0], 2)
        # Check that the F matrix is equal to the initial
        self.assertTrue((tsvRes["PhiField"][10][:-1, :-1] == TSV["PhiField"][10][:-1, :-1]).all())

        # Case 3: Run the function with an incomplete TSV file
        TSV = spam.helpers.readCorrelationTSV(
            "Step0-Step1-ddic.tsv",
            readConvergence=False,
            readError=False,
            readLabelDilate=False,
            readPixelSearchCC=False,
        )
        res = spam.DIC.getDisplacementFromNeighbours(imLab, TSV, "./TSV_getDisplacementFromNeighbours.tsv", previousDVC=TSV)
        self.assertIsNone(res)

    def test_mergeRegistrationAndDiscreteFields(self):

        # Create an initial Lab

        #######################################################
        # We're using the DDIC test from scripts here, lightly modified
        #######################################################
        # First we need to create some data using DEM dataset
        pixelSize = 0.0001
        blurSTD = 0.8
        noiseSTD = 0.01
        boxSizeDEM, centres, radii = spam.datasets.loadUniformDEMboxsizeCentreRadius()

        # put 0 in the middle
        centres -= numpy.mean(centres, axis=0)
        rMax = numpy.amax(radii)

        # pad box size
        boxSizeDEM = boxSizeDEM + 5 * rMax

        # add half box size to centres
        centres += numpy.array(boxSizeDEM) / 2.0
        boxSizePx = (boxSizeDEM / pixelSize).astype(int)
        centresPx = centres / pixelSize
        radiiPx = radii / pixelSize
        box = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(box, centresPx, radiiPx)
        box[numpy.where(box > 1.0)] = 1.0
        box[numpy.where(box < 0.0)] = 0.0
        box = box * 0.5
        box = box + 0.25
        box = scipy.ndimage.gaussian_filter(box, sigma=blurSTD)
        box = numpy.random.normal(box, scale=noiseSTD)
        binIm0 = box >= 0.5
        # Run watershed
        labIm0 = spam.label.ITKwatershed.watershed(binIm0)
        # Save images
        tifffile.imwrite("Step0.tif", box.astype("<f4"))
        tifffile.imwrite("Lab0.tif", labIm0.astype(spam.label.labelType))

        # Create the first step

        # Create Phi and Apply (25 px displacement on Y-axis, and 0 degree rotation along Z axis)
        translationStep1 = [5, 2, 0]
        rotationStep1 = [0, 0, 0]
        transformation = {"t": translationStep1, "r": rotationStep1}
        Phi = spam.deformation.computePhi(transformation)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]
        boxDeformed = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(boxDeformed, centresPxDeformed, radiiPx)
        boxDeformed[numpy.where(boxDeformed > 1.0)] = 1.0
        boxDeformed[numpy.where(boxDeformed < 0.0)] = 0.0
        boxDeformed = boxDeformed * 0.5
        boxDeformed = boxDeformed + 0.25
        boxDeformed = scipy.ndimage.gaussian_filter(boxDeformed, sigma=blurSTD)
        boxDeformed = numpy.random.normal(boxDeformed, scale=noiseSTD)
        # Save images
        tifffile.imwrite("Step1.tif", boxDeformed.astype("<f4"))

        # Create the second step

        # Create Phi and Apply (25 px displacement on Y-axis, and 5 degree rotation along Z axis)
        translationStep2 = [10, 4, 0]
        rotationStep2 = [0, 0, 0]
        transformation = {"t": translationStep2, "r": rotationStep2}
        Phi = spam.deformation.computePhi(transformation)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]
        boxDeformed = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(boxDeformed, centresPxDeformed, radiiPx)
        boxDeformed[numpy.where(boxDeformed > 1.0)] = 1.0
        boxDeformed[numpy.where(boxDeformed < 0.0)] = 0.0
        boxDeformed = boxDeformed * 0.5
        boxDeformed = boxDeformed + 0.25
        boxDeformed = scipy.ndimage.gaussian_filter(boxDeformed, sigma=blurSTD)
        boxDeformed = numpy.random.normal(boxDeformed, scale=noiseSTD)
        # Save images
        tifffile.imwrite("Step2.tif", boxDeformed.astype("<f4"))

        # Create the TSV from macro reg between 0 and 1
        exitCode = subprocess.call(["spam-reg", "-bb", "2", "-be", "2", "Step0.tif", "Step1.tif", "-od", "."])
        self.assertEqual(exitCode, 0)
        # exitCode = subprocess.call(["spam-ldic",
        # "-glt", "0.5",
        # "-hws", "10",
        # "-ns", "10",
        # "-it", "5",
        # "-pf", "Step0-Step1-registration.tsv",
        # "Step0.tif", "Step1.tif",
        # "-od", "."])
        # self.assertEqual(exitCode, 0)

        # Create the TSV from ddic between 0 and 1

        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-ld",
                "2",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        # Create the TSV from macro reg between 1 and 2
        exitCode = subprocess.call(["spam-reg", "-bb", "2", "-be", "2", "Step1.tif", "Step2.tif", "-od", "."])
        self.assertEqual(exitCode, 0)
        # exitCode = subprocess.call(["spam-ldic",
        # "-glt", "0.5",
        # "-hws", "10",
        # "-ns", "10",
        # "-it", "5",
        # "-pf", "Step1-Step2-registration.tsv",
        # "Step1.tif", "Step2.tif",
        # "-od", "."])
        # self.assertEqual(exitCode, 0)

        # Read ddic TSV
        discreteTSV = spam.helpers.readCorrelationTSV(
            "Step0-Step1-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )

        # Read macro TSV
        macroTSV = spam.helpers.readCorrelationTSV("Step1-Step2-registration.tsv")
        # Run function
        spam.DIC.mergeRegistrationAndDiscreteFields(macroTSV, discreteTSV, "TSV_mergeRegistrationAndDiscreteFields.tsv")
        # Read the resulting TSV
        tsvRes = spam.helpers.readCorrelationTSV("TSV_mergeRegistrationAndDiscreteFields.tsv")
        # Check the results
        for i in range(tsvRes["numberOfLabels"]):
            # disp = spam.deformation.deformationFunction.decomposePhi(tsvRes['PhiField'][i])['t']
            disp = tsvRes["PhiField"][i, 0:3, -1]
            print(disp)
            self.assertTrue(numpy.allclose(disp, translationStep2, atol=1))


if __name__ == "__main__":
    unittest.main()

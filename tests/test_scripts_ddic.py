import os
import subprocess
import unittest

import numpy
import scipy
import spam.datasets
import spam.deformation
import spam.DIC
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile

# This for hiding script output
FNULL = open(os.devnull, "w")

# This for showing it
# FNULL = None

# We also check 2D slice so don't displace in Z or rotate around an axis other than Z
refTranslation = numpy.array([0.0, 15, 0.0])
refRotation = numpy.array([3.0, 0.0, 0.0])


class TestAll(spam.helpers.TestSpam):
    def test_ITKwatershed(self):
        # Run on snow data
        # load 3D image
        snowRef = spam.datasets.loadSnow()
        snowRefBin = snowRef > numpy.mean(spam.helpers.findHistogramPeaks(snowRef))

        # save it locally
        tifffile.imwrite("snow-ref-bin.tif", snowRefBin.astype("<u1") * 255)

        exitCode = subprocess.call(
            ["spam-ITKwatershed", "snow-ref-bin.tif", "-pre", "snow-ref-watershed"],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # Load result and make sure it looks like a labelled image
        lab = tifffile.imread("snow-ref-watershed.tif")
        self.assertEqual(lab.max() > 10, True)

    def test_discreteDICtools(self):
        #######################################################
        # 1. Generate Data
        #######################################################
        # First we need to create some data using DEM dataset
        pixelSize = 0.0001
        blurSTD = 0.8
        noiseSTD = 0.01
        boxSizeDEM, centres, radii = spam.datasets.loadUniformDEMboxsizeCentreRadius()

        # put 0 in the middle
        centres -= numpy.mean(centres, axis=0)
        rMax = numpy.amax(radii)

        # pad box size
        boxSizeDEM = boxSizeDEM + 5 * rMax

        # add half box size to centres
        centres += numpy.array(boxSizeDEM) / 2.0
        boxSizePx = (boxSizeDEM / pixelSize).astype(int)
        centresPx = centres / pixelSize
        radiiPx = radii / pixelSize
        box = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPx,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Run watershed
        labIm0 = spam.label.ITKwatershed.watershed(box >= 0.5)
        # Save images
        tifffile.imwrite("Step0.tif", box.astype("<f4"))
        tifffile.imwrite("Lab0.tif", labIm0.astype(spam.label.labelType))

        # test of rigid translation and rotation
        # Create Phi and Apply (2 px displacement on X-axis, and 5 degree rotation along Z axis)
        # --> Change whatever you want but onyl rotations around Z-axis!!!
        translationStep1 = numpy.array([0.0, 0.0, 2.0])
        rotationStep1 = numpy.array([3.0, 0.0, 0.0])
        transformation1 = {"t": translationStep1, "r": rotationStep1}
        Phi1 = spam.deformation.computePhi(transformation1)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi1, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]

        boxDeformed = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPxDeformed,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Save images
        tifffile.imwrite("Step1.tif", boxDeformed.astype("<f4"))

        # test of rigid translation and rotation
        translationStep2 = [0, 0, 18]
        rotationStep2 = [0, 0, 0]
        transformation2 = {"t": translationStep2, "r": rotationStep2}
        Phi2 = spam.deformation.computePhi(transformation2)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi2, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]

        boxDeformed = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPxDeformed,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Save images
        tifffile.imwrite("Step2.tif", boxDeformed.astype("<f4"))

        del boxDeformed, centresPxDeformed, radiiPx

        #######################################################
        # 2. Run ddic -- Step0 -> Step1 with reg
        #######################################################
        # 2a registration
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(["spam-reg", "-bb", "2", "Step0.tif", "Step1.tif", "-od", "."])
        self.assertEqual(exitCode, 0)
        reg01 = spam.helpers.readCorrelationTSV("Step0-Step1-registration.tsv")
        reg01transformation = spam.deformation.decomposePhi(reg01["PhiField"][0])
        # print(reg01transformation['t'])
        # print(reg01transformation['r'])
        self.assertTrue(numpy.allclose(translationStep1, reg01transformation["t"], atol=0.05))
        self.assertTrue(numpy.allclose(rotationStep1, reg01transformation["r"], atol=0.05))

        # 2b ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
                "-pre",
                "balls-2b",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult2b = spam.helpers.readCorrelationTSV("balls-2b-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2b["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult2b["returnStatus"]) == 2)

        # 2c now use spam-passPhiField to apply the registration to the labelled image as an input
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-2c-reg",
                "-F",
                "rigid",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        DDICresult2c = spam.helpers.readCorrelationTSV("balls-2c-reg-passed-labelled.tsv")
        # TODO: Here you *could* check the rigid rotation's been applied to each particle
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2c["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )

        # 2d ddic from reg applied to lab
        # Repeat ddic but with this as initial guess
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-2c-reg-passed-labelled.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
                "-pre",
                "balls-2d",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult2d = spam.helpers.readCorrelationTSV("balls-2d-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2d["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        # TODO: Here you *could* check the rigid rotation's been applied to each particle
        self.assertTrue(numpy.median(DDICresult2d["returnStatus"]) == 2)

        ########################################################
        # 3. Rerun DDIC -- Step0 -> Step1 with prev result
        ########################################################
        # 3a
        # Just run a simple DVC with no outputs except TSV + multiscale
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-2b-ddic.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-msb",
                "2",
                "-nr",
                "-ld",
                "2",
                "-od",
                ".",
                "-pre",
                "balls-3a",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult3a = spam.helpers.readCorrelationTSV("balls-3a-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult3a["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult3a["returnStatus"]) == 2)

        ########################################################
        # Now check with label mover -- easier if there are no rotations
        ########################################################
        exitCode = subprocess.call(["spam-moveLabels", "Lab0.tif", "balls-3a-ddic.tsv", "-rst", "2"])
        self.assertEqual(exitCode, 0)

        imDef = tifffile.imread("Lab0-displaced.tif")
        COMref = spam.label.centresOfMass(labIm0)
        COMdef = spam.label.centresOfMass(imDef)

        # Load displaced labelled image
        labelDisp = numpy.nanmean(COMdef - COMref[0 : COMdef.shape[0]], axis=0)
        # Go from 0:COMdef.shape[0] in case any labels are lost
        self.assertAlmostEqual(translationStep1[0], labelDisp[0], places=0)
        self.assertAlmostEqual(translationStep1[1], labelDisp[1], places=0)
        self.assertAlmostEqual(translationStep1[2], labelDisp[2], places=0)

        #######################################################
        # 5a Run ddic -- Step0 -> Step2 with pixel search
        #######################################################
        # Just run a simple DVC with no outputs
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-sr",
                "{}".format(int(translationStep2[0] - 2)),
                "{}".format(int(translationStep2[0] + 2)),
                "{}".format(int(translationStep2[1] - 2)),
                "{}".format(int(translationStep2[1] + 2)),
                "{}".format(int(translationStep2[2] - 2)),
                "{}".format(int(translationStep2[2] + 2)),
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-5a",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult5a = spam.helpers.readCorrelationTSV("balls-5a-pixelSearch.tsv")
        self.assertTrue(numpy.sum(PSresult5a["returnStatus"]) == len(radii))
        # Z-displacements should be not modified by the rotation, so can be checked
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )

        # 5b ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(["spam-ddic", "-pf", "balls-5a-pixelSearch.tsv", "Step0.tif", "Lab0.tif", "Step2.tif", "-od", ".", "-pre", "balls-5b", "-o", "3"])
        self.assertEqual(exitCode, 0)

        DDICresult5b = spam.helpers.readCorrelationTSV("balls-5b-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult5b["returnStatus"]) == 2)

        ######################################################
        # 5c Run ddic -- Step0 -> Step2 with pixel search propagate
        #######################################################
        # Just run a simple DVC with no outputs
        exitCode = subprocess.call(
            [
                "spam-pixelSearchPropagate",
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-sp",
                "{}".format(int(COMref[1, 0])),
                "{}".format(int(COMref[1, 1])),
                "{}".format(int(COMref[1, 2])),
                "{}".format(int(translationStep2[0])),
                "{}".format(int(translationStep2[1])),
                "{}".format(int(translationStep2[2])),
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-5c",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult5c = spam.helpers.readCorrelationTSV("balls-5c-pixelSearchPropagate.tsv")
        self.assertTrue(numpy.sum(PSresult5c["returnStatus"]) == len(radii))
        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )

        # 5d ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-5c-pixelSearchPropagate.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step2.tif",
                "-od",
                ".",
                "-pre",
                "balls-5d",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult5d = spam.helpers.readCorrelationTSV(
            "balls-5d-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult5d["returnStatus"]) == 2)

        # 5e ddic result passed to same label image with spam-passPhiField (ideally it would be differently labelled)
        # ...

        #######################################################
        # 6. Check -skp
        #######################################################
        # Manually modify the RS of the TSV from 5d
        DDICresult5d["returnStatus"][-3:] = -3
        # Save the new TSV2
        outMatrix = numpy.array(
            [
                numpy.array(range(DDICresult5d["numberOfLabels"])),
                DDICresult5d["fieldCoords"][:, 0],
                DDICresult5d["fieldCoords"][:, 1],
                DDICresult5d["fieldCoords"][:, 2],
                DDICresult5d["PhiField"][:, 0, 3],
                DDICresult5d["PhiField"][:, 1, 3],
                DDICresult5d["PhiField"][:, 2, 3],
                DDICresult5d["PhiField"][:, 0, 0],
                DDICresult5d["PhiField"][:, 0, 1],
                DDICresult5d["PhiField"][:, 0, 2],
                DDICresult5d["PhiField"][:, 1, 0],
                DDICresult5d["PhiField"][:, 1, 1],
                DDICresult5d["PhiField"][:, 1, 2],
                DDICresult5d["PhiField"][:, 2, 0],
                DDICresult5d["PhiField"][:, 2, 1],
                DDICresult5d["PhiField"][:, 2, 2],
                DDICresult5d["pixelSearchCC"],
                DDICresult5d["error"],
                DDICresult5d["iterations"],
                DDICresult5d["returnStatus"],
                DDICresult5d["deltaPhiNorm"],
                DDICresult5d["LabelDilate"],
            ]
        ).T
        numpy.savetxt(
            "balls-6a-ddic.tsv",
            outMatrix,
            fmt="%.7f",
            delimiter="\t",
            newline="\n",
            comments="",
            header="Label\tZpos\tYpos\tXpos\t"
            + "Zdisp\tYdisp\tXdisp\t"
            + "Fzz\tFzy\tFzx\t"
            + "Fyz\tFyy\tFyx\t"
            + "Fxz\tFxy\tFxx\t"
            + "PSCC\terror\titerations\treturnStatus\tdeltaPhiNorm\tLabelDilate",
        )
        # Run
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-6a-ddic.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step2.tif",
                "-od",
                ".",
                "-skp",
                "-pre",
                "balls-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Read both TSV files
        DDICresult6a = spam.helpers.readCorrelationTSV(
            "balls-6a-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        DDICresult6b = spam.helpers.readCorrelationTSV(
            "balls-6b-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        # Check that both files are identical for the converged grains of DDICresult6a
        self.assertTrue(DDICresult6a["fieldDims"] == DDICresult6b["fieldDims"])
        self.assertTrue((DDICresult6a["fieldCoords"] == DDICresult6b["fieldCoords"]).all())
        self.assertTrue(DDICresult6a["numberOfLabels"] == DDICresult6b["numberOfLabels"])
        convLabDDICresult6a = numpy.where(DDICresult6a["returnStatus"] == 2)[0]
        # Loop through each label and verify each key
        for label in convLabDDICresult6a:
            # Check returnStatus
            self.assertTrue(DDICresult6a["returnStatus"][label] == DDICresult6b["returnStatus"][label])
            # Check deltaPhiNorm
            self.assertTrue(DDICresult6a["deltaPhiNorm"][label] == DDICresult6b["deltaPhiNorm"][label])
            # Check iterations
            self.assertTrue(DDICresult6a["iterations"][label] == DDICresult6b["iterations"][label])
            # Check error
            self.assertTrue(DDICresult6a["error"][label] == DDICresult6b["error"][label])
            # Check pixelSearchCC
            self.assertTrue(DDICresult6a["pixelSearchCC"][label] == DDICresult6b["pixelSearchCC"][label])
            # Chekc LabelDilate
            self.assertTrue(DDICresult6a["LabelDilate"][label] == DDICresult6b["LabelDilate"][label])
            # Check PhiField
            self.assertTrue((DDICresult6a["PhiField"][label] == DDICresult6b["PhiField"][label]).all())

        #######################################################
        # 7. Rerun ddic -- Step0 -> Step2 with prev result
        #######################################################
        # 7a repeat pixelSearch to give exactly same result
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "balls-5a-pixelSearch.tsv",
                "-sr",
                "-3",
                "3",
                "-3",
                "3",
                "-3",
                "3",
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-7a",
            ]
        )
        PSresult6a = spam.helpers.readCorrelationTSV("balls-7a-pixelSearch.tsv")
        self.assertTrue(numpy.sum(PSresult6a["returnStatus"]) == len(radii))
        # Al displacements should be exactly the same as before
        # print(PSresult5a['PhiField'][1:,0:3,-1])
        # print(PSresult6a['PhiField'][1:,0:3,-1])
        self.assertTrue(0 == numpy.sum(PSresult5a["PhiField"][1:, 0:3, -1] - PSresult6a["PhiField"][1:, 0:3, -1]))

        #######################################################
        # 4. 2020-09-05 EA and OS: New "extreme" test with particles touching the boundaries
        #######################################################
        im1 = spam.kalisphera.makeBlurryNoisySphere(
            [100, 100, 100],
            [[50, 50, 50], [50, 10, 10]],
            [10, 10],
            0.5,
            0.01,
            background=0.0,
            foreground=1.0,
        )
        im2 = spam.kalisphera.makeBlurryNoisySphere(
            [100, 100, 100],
            [[54, 54, 54], [54, 14, 14]],
            [10, 10],
            0.5,
            0.01,
            background=0.0,
            foreground=1.0,
        )
        im1lab = scipy.ndimage.label(im1 > 0.5)[0]

        tifffile.imwrite("extreme-im1.tif", im1)
        tifffile.imwrite("extreme-im1lab.tif", im1lab)
        tifffile.imwrite("extreme-im2.tif", im2)

        # Just run a simple DVC
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "extreme-im1.tif",
                "extreme-im1lab.tif",
                "extreme-im2.tif",
                "-m",
                "10",
                "-ld",
                "2",
            ]
        )
        self.assertEqual(exitCode, 0)
        TSVextreme = spam.helpers.readCorrelationTSV("extreme-im1-extreme-im2-ddic.tsv")
        # self.assertAlmostEqual(TSV['PhiField'][i][u,-1], TSV2['PhiField'][i][u,-1], places=1)
        self.assertEqual(list(TSVextreme["returnStatus"][1:]), [2, 2])
        for axis in range(3):
            for label in [1, 2]:
                self.assertAlmostEqual(
                    numpy.abs(TSVextreme["PhiField"][label, axis, -1] - 4.0),
                    0.0,
                    places=1,
                )

        #######################################################
        # 5. 2D DDIC - simple version
        #######################################################
        # Create images
        imGrey1 = spam.kalisphera.makeBlurryNoisySpheroid([40, 80, 40], [[20, 40, 20]], [[10, 20]], [[0, 1, 0]], blur=0.8, noise=0.03)
        imGrey2 = spam.kalisphera.makeBlurryNoisySpheroid(
            [40, 80, 40],
            [[20, 45, 20]],
            [[10, 20]],
            [[0, 0.96, 0.28]],
            blur=0.8,
            noise=0.03,
        )
        # Create 2D images
        imGrey1 = imGrey1[19, :, :]
        imGrey2 = imGrey2[19, :, :]
        imLab = (imGrey1 > 0.5).astype(int)
        tifffile.imwrite("imGrey1_2D.tif", imGrey1.astype(numpy.float32))
        tifffile.imwrite("imGrey2_2D.tif", imGrey2.astype(numpy.float32))
        tifffile.imwrite("imLab_2D.tif", imLab.astype("<u2"))
        # Run DDIC
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "imGrey1_2D.tif",
                "imLab_2D.tif",
                "imGrey2_2D.tif",
                "-od",
                ".",
                "-pre",
                "2D",
            ]
        )
        self.assertEqual(exitCode, 0)
        res = spam.helpers.readCorrelationTSV("2D-ddic.tsv")
        res1transformation = spam.deformation.decomposePhi(res["PhiField"][1])
        magDisp = numpy.linalg.norm(res1transformation["t"])
        numpy.linalg.norm(res1transformation["r"])
        self.assertLessEqual(numpy.abs(magDisp - 5), 2)

    def test_discreteStrain(self):
        # make sure it runs the help without error
        exitCode = subprocess.call(["spam-discreteStrain", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        nPoints = 30
        points = numpy.random.randint(low=0, high=100, size=(nPoints, 3)).astype("<f4")
        pointsDef = points * 1.01
        displacements = pointsDef - points
        radii = numpy.ones(nPoints)
        tri = spam.mesh.triangulate(points)

        spam.helpers.writeUnstructuredVTK(
            points,
            tri,
            pointData={"radius": radii, "displacements": displacements},
            fileName="test.vtk",
        )

        exitCode = subprocess.call(
            [
                "spam-discreteStrain",
                "test.vtk",
                "-pre",
                "test-strain",
                "-comp",
                "dev",
                "vol",
                "U",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        VTK = spam.helpers.readUnstructuredVTK("test-strain.vtk")
        self.assertAlmostEqual(VTK[3]["dev"].mean(), 0, places=3)
        self.assertAlmostEqual(VTK[3]["vol"].mean(), 3 * (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['xx'].mean(), (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['yy'].mean(), (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['zz'].mean(), (1.01 - 1.00), places=3)


if __name__ == "__main__":
    unittest.main()

import unittest

import meshio
import numpy
import spam.filters
import spam.helpers
import spam.kalisphera
import spam.mesh

PLAN_VECTOR_SOLUTION = [-1.0, 0.0, 0.0]
MESH_CUBE = spam.mesh.createCuboid([1, 1, 1], 0.1)
MESH_PLAN = spam.mesh.createCuboid([1.0, 3.0, 5.0], 0.1)


class TestAll(spam.helpers.TestSpam):
    """Test the projection tools (projmorpho)
    The two main situations tested are:
    1. test canonical functions of projmorpho
    2. tests on simple calls of projectObjects
    3. tests on simple calls of projectFields
    4. test multiple spheres
    4. test multiple cylinders
    """

    def test_projmorpho(self):
        """Directly test cpp functions of projmorpho
        - TEST 1: two tetrahedra simple field
        - TEST 2: two tetrahedra one sphere
        """

        def _voleqtet(d):
            """Compute the volume of an equilateral tetrahedron of side d"""
            b = numpy.sqrt(3) * d**2 / 4  # base (equilateral triangle of side d)
            h = numpy.sqrt(2 / 3) * d  # height

            return b * h / 3

        # Define the two tetrahedra
        dx = numpy.sqrt(0.75)
        dz = numpy.sqrt(1 - (2 * dx / 3) ** 2)
        mesh = {
            "points": numpy.array(
                [
                    [0.0, 0.0, 0.0],
                    [dx, 0.5, 0.0],
                    [0.0, 1.0, 0.0],
                    [dx / 3, 0.5, dz],
                    [1, 1, 2 * dz / 3],
                ]
            ),
            "cells": numpy.array(
                [
                    [0, 1, 2, 3],
                    [1, 4, 2, 3],
                ]
            ),
        }
        for p1 in mesh["points"][:4]:
            for p2 in [p for p in mesh["points"][:4] if str(p) != str(p1)]:
                p1 = numpy.array(p1)
                p2 = numpy.array(p2)
                # check that all edges are length 1.0
                self.assertAlmostEqual(numpy.linalg.norm(p1 - p2), 1.0, 6)

        # TEST 1: predefined field (linear interpolation)
        mesh_values = [[1.0, 1.0, 1.0, -1.0, -0.5]]  # 1 phase x 4 nodes
        # vTot = _voleqtet(1.0)
        # vTotM = _voleqtet(0.5)
        # vTotP = vTot - vTotM
        # print(f"Tet volume = {vTot}")
        # print(f"Tet subvolume (+) = {vTotP}")
        # print(f"Tet subvolume (-) = {vTotM}")
        # print(f'Tet volume = {spam.mesh.tetVolume(mesh["points"][:-1])}')

        # init projmorpho
        pr = spam.mesh.meshToolkit.projmorpho(name="projmorpho")

        # set mesh (cells need to be raveled and start with 1)
        cells = numpy.array(mesh["cells"], dtype=int).ravel() + 1
        pr.setMesh(numpy.array(mesh["points"]) - dx / 2, cells)
        pr.setField(mesh_values)

        pr.projection()
        pr.writeFEAP()
        pr.writeVTK()
        pr.writeInterfacesVTK()

        # check normal and volumes
        materials = pr.getMaterials()
        self.assertAlmostEqual(materials[0][1], _voleqtet(0.5), 6)  # sub volume (-) equilateral tetrahedron
        self.assertAlmostEqual(materials[0][2], 0.0, 6)  # normal x
        self.assertAlmostEqual(materials[0][3], 0.0, 6)  # normal y
        self.assertAlmostEqual(materials[0][4], 1.0, 6)  # normal z
        self.assertAlmostEqual(materials[1][1], 0.0366452001035213, 6)  # sub volume (-) second tetrahedron
        self.assertAlmostEqual(materials[1][2], 0.0472687073051929, 6)  # normal x
        self.assertAlmostEqual(materials[1][3], 0.0818718001246452, 6)  # normal y
        self.assertAlmostEqual(materials[1][4], 0.9955213069915771, 6)  # normal z

        # TEST 1: one sphere
        spheres = [
            [0.5, 0.8, 0, 0, 1],  # sphere 1 phase 1
        ]

        # init projmorpho
        pr = spam.mesh.meshToolkit.projmorpho(name="projmorpho_one_sphere")
        # set mesh (cells need to be raveled and start with 1)
        cells = numpy.array(mesh["cells"], dtype=int).ravel() + 1
        pr.setMesh(numpy.array(mesh["points"]) - dx / 2, cells)

        # set objects and compute distance field
        pr.setObjects(spheres)
        pr.computeFieldFromObjects()

        pr.projection(analytical_orientation=True)
        pr.writeFEAP()
        pr.writeVTK()
        pr.writeInterfacesVTK()

    def test_projection_sphere(self):
        """Project directly from spheres
        - TEST 1: one centered sphere
        - TEST 2: one sphere far away (plan)
        """

        # TEST 1: one centered sphere

        # create mesh for the projection (lenght 1x1x1 origin 0,0,0)
        points, cells = MESH_CUBE
        mesh = {"points": points, "cells": cells}

        # create objects
        sphere = [
            [0.2, 0.8, 0.1, 0.1, 1],  # one centered sphere (0.5, 0.5, 0.5) of radius 0.2 (phase 1)
        ]

        # project field
        materials = spam.mesh.projectObjects(
            mesh,
            sphere,
            analyticalOrientation=True,
            writeConnectivity="projobjects-sphere",
            vtkMesh=True,
        )

        sphereO = numpy.array(sphere[0][1:4])

        # check that normals points to the center of the sphere
        for i, (nodes_numbers, material) in enumerate(zip(mesh["cells"], materials)):

            # skip non interface
            if material[0] != 3:
                continue

            # normal from projmorph
            normalPR = material[2:5]
            self.assertAlmostEqual(numpy.linalg.norm(normalPR), 1.0, 6)

            # centroid
            point = spam.mesh.tetCentroid(points[nodes_numbers - 1])

            # STEP 1: compute OM
            OM = point - sphereO

            # normalized normal
            normalCH = OM / numpy.linalg.norm(OM)

            # Check same normals
            numpy.testing.assert_almost_equal(normalCH, normalPR, 6)

        # check that interface nodes are on the sphere
        mesh_int = meshio.read("projobjects-sphere_interfaces.vtk")
        for point in mesh_int.points:
            # interfaces.vtk is written in x, y, z. It has to be swapped to math spam convention.
            d = numpy.linalg.norm(point[::-1] - numpy.array(sphereO))
            self.assertAlmostEqual(d, 0.2, 2)

        # TEST 2: one plan
        # create mesh for the projection (lenght 1.0, 3.0, 5.0 origin 0,0,0)
        mesh = {"points": MESH_PLAN[0], "cells": MESH_PLAN[1]}

        # create objects
        d = 1e6
        sphere = [
            [d - 0.5, d, 1.5, 2.5, 1],  # one sphere far away (d) that cut the cube in the middle
        ]

        # project field
        materials = spam.mesh.projectObjects(
            mesh,
            sphere,
            analyticalOrientation=True,
            writeConnectivity="projobjects-plan",
            vtkMesh=True,
        )

        # check that normal vectors are aligned in the z direction
        sum1 = numpy.array([0.0, 0.0, 0.0])
        sum2 = numpy.array([0.0, 0.0, 0.0])
        n = 0
        for i, material in enumerate(materials):
            if material[0] != 3:
                continue

            # normal from projmorph
            normalPR = material[2:5]
            self.assertAlmostEqual(numpy.linalg.norm(normalPR), 1.0, 6)
            numpy.testing.assert_almost_equal(normalPR, PLAN_VECTOR_SOLUTION, 5)

            for i in range(3):
                sum1[i] += normalPR[i]
                sum2[i] += normalPR[i] ** 2
            n += 1

        mean = sum1 / n
        variance = (sum2 - sum1**2 / n) / n
        numpy.testing.assert_almost_equal(mean, PLAN_VECTOR_SOLUTION, 6)
        numpy.testing.assert_almost_equal(variance, [0.0, 0.0, 0.0], 6)

        # check that interface nodes are on the plan
        mesh = meshio.read("projobjects-plan_interfaces.vtk")
        for point in mesh.points:
            # interfaces.vtk is written in x, y, z. It has to be swapped to math spam convention.
            self.assertAlmostEqual(point[2], 0.5, 5)

    def test_projection_image(self):
        """Project from image (using spam distance field)
        - TEST 1: one centered sphere
        - TEST 2: one plan
        """

        # TEST 1: one centered sphere

        # create mesh for the projection (lenght 1x1x1 origin 0,0,0)
        points, cells = MESH_CUBE
        mesh = {"points": points, "cells": cells}

        # create distance field
        nCells = 100
        one_sphere_binary = numpy.zeros([nCells + 1] * 3, dtype=float)
        spam.kalisphera.makeSphere(one_sphere_binary, [50.5] * 3, 20)
        one_sphere_field = [spam.filters.distanceField(one_sphere_binary.astype(bool).astype(int))]

        # project field
        fields = {
            "origin": [0] * 3,  # coordinates of the origin of the field (3 x 1)
            "lengths": [1] * 3,  # lengths of fields domain of definition  (3 x 1)
            "values": one_sphere_field,  # list of fields
        }
        materials = spam.mesh.projectField(mesh, fields, writeConnectivity="projimage-sphere", vtkMesh=True)

        # check that normals points to the center of the sphere
        for nodes_numbers, material in zip(mesh["cells"], materials):

            # skip non interface
            if material[0] != 3:
                continue

            # normal from projmorph
            normalPR = material[2:5]
            self.assertAlmostEqual(numpy.linalg.norm(normalPR), 1.0, 6)

            # center of the sphere
            sphereCentre = numpy.array([0.5, 0.5, 0.5])

            # centroid
            point = spam.mesh.tetCentroid(points[nodes_numbers - 1])

            # normalized normal
            normalCH = sphereCentre - point
            normalCH /= -numpy.linalg.norm(normalCH)

            # Check same normals
            numpy.testing.assert_almost_equal(normalCH, normalPR, 0)

        # check that interface nodes are on the sphere
        mesh = meshio.read("projimage-sphere_interfaces.vtk")
        for point in mesh.points:
            d = numpy.linalg.norm(point - numpy.array([0.5, 0.5, 0.5]))
            self.assertAlmostEqual(d, 0.2, 1)

        # TEST 2: one plan
        nCells = 100
        lengths = [1.0, 3.0, 5.0]

        # create mesh for the projection (lenght 1.0, 3.0, 5.0 origin 0,0,0)
        points, cells = MESH_PLAN
        mesh = {"points": points, "cells": cells}

        d = 1e6
        one_sphere_binary = numpy.zeros([nCells + 1] * 3, dtype=float)
        spam.kalisphera.makeSphere(one_sphere_binary, [d, nCells // 2, nCells // 2], d - nCells // 2)  # one sphere far away (d) that cut the cube in the middle
        one_sphere_field = [spam.filters.distanceField(one_sphere_binary.astype(bool).astype(int))]

        # project field
        fields = {
            "origin": [0] * 3,  # coordinates of the origin of the field (3 x 1)
            "lengths": lengths,  # lengths of fields domain of definition  (3 x 1)
            "values": one_sphere_field,  # list of fields
        }
        materials = spam.mesh.projectField(mesh, fields, writeConnectivity="projimage-plan", vtkMesh=True)

        # check that normal vectors are aligned in the z direction
        sum1 = numpy.array([0.0, 0.0, 0.0])
        sum2 = numpy.array([0.0, 0.0, 0.0])
        n = 0
        for i, material in enumerate(materials):
            if material[0] != 3:
                continue

            # normal from projmorph
            normalPR = material[2:5]
            self.assertAlmostEqual(numpy.linalg.norm(normalPR), 1.0, 6)
            numpy.testing.assert_almost_equal(normalPR, PLAN_VECTOR_SOLUTION, 0)

            for i in range(3):
                sum1[i] += normalPR[i]
                sum2[i] += normalPR[i] ** 2
            n += 1

        mean = sum1 / n
        variance = (sum2 - sum1**2 / n) / n
        numpy.testing.assert_almost_equal(mean, PLAN_VECTOR_SOLUTION, 2)
        numpy.testing.assert_almost_equal(variance, [0.0, 0.0, 0.0], 2)

        # check that interface nodes are on the plan
        mesh = meshio.read("projimage-plan_interfaces.vtk")
        for point in mesh.points:
            # interfaces.vtk is written in x, y, z. It has to be swapped to math spam convention.
            self.assertAlmostEqual(point[2], 0.5, 1)

    def test_multi_spheres(self):
        """Test projection with multiple spheres"""

        # mesh for the projection (lenght 1x1x1 origin 0,0,0)
        points, cells = MESH_CUBE
        mesh = {"points": points, "cells": cells}

        # create spheres
        spheres = [
            [0.1, 0.1, 0.1, 0.1, 1],  # sphere 1 phase 1
            [0.5, 1, 1, 1, 1],  # sphere 2 phase 1
            [0.2, 0.9, 0.1, 0.2, 2],  # sphere 3 phase 2
        ]
        spam.mesh.projectObjects(mesh, spheres, analyticalOrientation=True)  # , writeConnectivity="projspheres", vtkMesh=True)
        spam.mesh.projectObjects(mesh, spheres, analyticalOrientation=False)  # , writeConnectivity="projspheres", vtkMesh=True)

    def test_cylinders(self):
        """Test cylinders
        TEST 1: four cylinders
        TEST 2: one cylinder (testing interface position)
        """

        # create mesh for the projection (lenght 1x1x1 origin 0,0,0)
        points, cells = MESH_CUBE
        mesh = {"points": points, "cells": cells}

        # TEST 1: four cylinder
        # create cylinders
        # for each cylinder:
        #     position_z, position_y, position_x, direction_z, direction_y, direction_x, radius, phase
        cylinders = [
            [0.1, 0, 0.2, 0.2, 1, 0, 0, 1],  # cylinder 1 phase 1
            [0.1, 0, 0.2, 0.8, 1, 0, 0, 1],  # cylinder 2 phase 1
            [0.075, 0, 0.8, 0.3, 1, 0.2, 0, 2],  # cylinder 1 phase 2
            [0.075, 0, 0.8, 0.7, 1, 0.2, 0, 2],  # cylinder 2 phase 2
        ]
        materials = spam.mesh.projectObjects(mesh, cylinders, writeConnectivity="projcylinders", vtkMesh=True)

        # TEST 2: one cylinder
        cylinders = [
            [0.1, 0.5, 0.4, 0.2, 1, -0.2, 0.5, 1],  # cylinder 1 phase 1
        ]
        materials = spam.mesh.projectObjects(mesh, cylinders, cutoff=1e-12, writeConnectivity="projcylinder", vtkMesh=True)

        mesh_int = meshio.read("projcylinder_interfaces.vtk")

        cylinderR = cylinders[0][0]
        cylinderO = numpy.array(cylinders[0][1:4])
        cylinderN = numpy.array(cylinders[0][4:7])

        # check that all interface nodes are
        sum1 = 0.0
        sum2 = 0.0
        n = len(mesh_int.points)
        for p in mesh_int.points:
            # interfaces.vtk is written in x, y, z. It has to be swapped to math spam convention.
            point = numpy.array(p[:3])[::-1]
            OM = point - cylinderO
            d = numpy.linalg.norm(numpy.cross(OM, cylinderN)) / numpy.linalg.norm(cylinderN)
            sum1 += d
            sum2 += d**2
            self.assertAlmostEqual(d, cylinderR, 1)
        mean = sum1 / n
        variance = (sum2 - sum1**2 / n) / n
        self.assertAlmostEqual(mean, 0.1, 2)
        self.assertAlmostEqual(variance, 0.0, 5)

        # check that normals points to the generatice of the cylinder
        for i, (nodes_numbers, material) in enumerate(zip(mesh["cells"], materials)):

            # skip non interface
            if material[0] != 3:
                continue

            # normal from projmorph
            normalPR = material[2:5]
            self.assertAlmostEqual(numpy.linalg.norm(normalPR), 1.0, 6)

            # centroid
            point = spam.mesh.tetCentroid(points[nodes_numbers - 1])

            # STEP 1: compute OM
            OM = point - cylinderO
            # STEP 2: compute OH as projection of OM onto line ported by n OH = (OM.n) x n / |n|^2
            OH = numpy.dot(OM, cylinderN) / numpy.linalg.norm(cylinderN) ** 2 * cylinderN
            # STEP 3: compute HM = -OH + OM
            HM = -OH + OM
            # STEP 4: compute normal as normalized HM
            normalCH = HM / numpy.linalg.norm(HM)

            numpy.testing.assert_almost_equal(normalCH, normalPR, 6)

    def test_debug(self):
        """element with value falling exactly on the threshold"""

        mesh = {"points": numpy.array([[1, 1, 0.5], [0.942321, 0.94731, 0.502018], [1, 0.9194, 0.443077], [1, 0.899134, 0.537372]]), "cells": numpy.array([0, 1, 2, 3])}

        spheres = [
            [0.5, 1, 1, 1, 1],
        ]
        spam.mesh.projectObjects(mesh, spheres, analyticalOrientation=False, writeConnectivity="debug", vtkMesh=True)

        points, connectivity = spam.mesh.createCylinder([0.0, 0.0], 0.5, 4.0, 0.1)
        spam.helpers.writeUnstructuredVTK(points, connectivity, fileName="cylinder.vtk")

        # create distance field
        nCells = numpy.array([200, 50, 50])
        one_sphere_binary = numpy.zeros(nCells + 1, dtype=float)
        spam.kalisphera.makeSphere(one_sphere_binary, [100.5, 25.5, 25.5], 10)
        one_sphere_field = spam.filters.distanceField(one_sphere_binary.astype(bool).astype(int))

        # project field
        fields = {
            "origin": [0, -0.5, -0.5],  # coordinates of the origin of the field (3 x 1)
            "lengths": [4, 1, 1],  # lengths of fields domain of definition  (3 x 1)
            "values": [one_sphere_field],  # list of fields
        }
        mesh = {"points": points, "cells": connectivity}

        spam.mesh.projectField(mesh, fields, writeConnectivity="projimage-sphere", vtkMesh=True)


if __name__ == "__main__":
    # spam.helpers.TestSpam.DEBUG = True
    unittest.main()

# import math
# import os
# import random
import unittest

import numpy

# import scipy.ndimage
import skimage.morphology
import spam.datasets
import spam.kalisphera
import spam.label
import spam.label.contacts as con
import spam.plotting

# import tifffile

VERBOSE = True

# small labelled volume is a 3x3x3 with a single 1 in the middle
threeCubedLabelVol = numpy.zeros((3, 3, 3), dtype=spam.label.labelType)
threeCubedLabelVol[1, 1, 1] = 1

longSingle = numpy.zeros((9, 6, 4), dtype=spam.label.labelType)
longSingle[1:9, 1:6, 1:3] = 1


class TestAll(spam.helpers.TestSpam):
    def test_boundingBoxes(self):
        BB = spam.label.boundingBoxes(threeCubedLabelVol)
        self.assertEqual(numpy.zeros(6, dtype=spam.label.labelType).tolist(), BB[0].tolist())
        self.assertEqual(numpy.ones(6, dtype=spam.label.labelType).tolist(), BB[1].tolist())

        BB = spam.label.boundingBoxes(longSingle)
        self.assertEqual(
            numpy.array([1, 8, 1, 5, 1, 2], dtype=spam.label.labelType).tolist(),
            BB[1].tolist(),
        )

        #GP: Adding the 2D case
        square = threeCubedLabelVol[:,1,:]
        BB = spam.label.boundingBoxes(square)
        self.assertEqual(numpy.zeros(6, dtype=spam.label.labelType).tolist(), BB[0].tolist())
        self.assertEqual([0,0,1,1,1,1], BB[1].tolist())

    def test_centresOfMass(self):
        # without pre-computation of BB
        COM = spam.label.centresOfMass(threeCubedLabelVol)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), COM[0].tolist())
        self.assertEqual(numpy.ones(3, dtype="<f4").tolist(), COM[1].tolist())

        # put in min vol filter
        COM = spam.label.centresOfMass(threeCubedLabelVol, minVol=2)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), COM[0].tolist())
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), COM[1].tolist())

        # with pre-computation of BB
        BB = spam.label.boundingBoxes(threeCubedLabelVol)
        COM = spam.label.centresOfMass(threeCubedLabelVol, boundingBoxes=BB)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), COM[0].tolist())
        self.assertEqual(numpy.ones(3, dtype="<f4").tolist(), COM[1].tolist())

        #GP: Adding the 2D case
        square = threeCubedLabelVol[:,1,:]
        BB = spam.label.boundingBoxes(square)
        COM = spam.label.centresOfMass(square, boundingBoxes=BB)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), COM[0].tolist())
        self.assertEqual([0,1,1], COM[1].tolist())

    def test_volumes(self):
        # without pre-computation of BB
        volumes = spam.label.volumes(threeCubedLabelVol)
        self.assertEqual(0, volumes[0])
        self.assertEqual(1, volumes[1])

        # with pre-computation of BB
        BB = spam.label.boundingBoxes(threeCubedLabelVol)
        volumes = spam.label.volumes(threeCubedLabelVol, boundingBoxes=BB)
        self.assertEqual(0, volumes[0])
        self.assertEqual(1, volumes[1])

    def test_equivalentRadii(self):
        # without pre-computation of BB or volumes
        radii = spam.label.equivalentRadii(threeCubedLabelVol)
        self.assertAlmostEqual(0.0, radii[0], places=7)
        self.assertAlmostEqual(((3.0 * 1.0) / (4.0 * numpy.pi)) ** (1.0 / 3.0), radii[1], places=7)

        volumes = spam.label.volumes(threeCubedLabelVol)
        radii = spam.label.equivalentRadii(threeCubedLabelVol, volumes=volumes)
        self.assertAlmostEqual(0.0, radii[0], places=7)
        self.assertAlmostEqual(((3.0 * 1.0) / (4.0 * numpy.pi)) ** (1.0 / 3.0), radii[1], places=7)

        BB = spam.label.boundingBoxes(threeCubedLabelVol)
        radii = spam.label.equivalentRadii(threeCubedLabelVol, boundingBoxes=BB)
        self.assertAlmostEqual(0.0, radii[0], places=7)
        self.assertAlmostEqual(((3.0 * 1.0) / (4.0 * numpy.pi)) ** (1.0 / 3.0), radii[1], places=7)

    def test_momentOfInertia(self):
        MOIval, MOIvec = spam.label.momentOfInertia(longSingle)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), MOIval[0].tolist())
        self.assertEqual(numpy.zeros(9, dtype="<f4").tolist(), MOIvec[0].tolist())
        self.assertTrue(MOIval[1, 0] > MOIval[1, 1])
        self.assertTrue(MOIval[1, 1] > MOIval[1, 2])
        self.assertTrue(MOIval[1, 2] > 0.0)
        self.assertEqual(numpy.array([0, 0, 1], dtype="<f4").tolist(), MOIvec[1, 0:3].tolist())
        self.assertEqual(numpy.array([0, 1, 0], dtype="<f4").tolist(), MOIvec[1, 3:6].tolist())
        self.assertEqual(numpy.array([1, 0, 0], dtype="<f4").tolist(), MOIvec[1, 6:9].tolist())

        BB = spam.label.boundingBoxes(longSingle)
        MOIval, MOIvec = spam.label.momentOfInertia(longSingle, boundingBoxes=BB)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), MOIval[0].tolist())
        self.assertEqual(numpy.zeros(9, dtype="<f4").tolist(), MOIvec[0].tolist())
        self.assertTrue(MOIval[1, 0] > MOIval[1, 1])
        self.assertTrue(MOIval[1, 1] > MOIval[1, 2])
        self.assertTrue(MOIval[1, 2] > 0.0)
        self.assertEqual(numpy.array([0, 0, 1], dtype="<f4").tolist(), MOIvec[1, 0:3].tolist())
        self.assertEqual(numpy.array([0, 1, 0], dtype="<f4").tolist(), MOIvec[1, 3:6].tolist())
        self.assertEqual(numpy.array([1, 0, 0], dtype="<f4").tolist(), MOIvec[1, 6:9].tolist())

        COM = spam.label.centresOfMass(longSingle, boundingBoxes=BB)
        MOIval, MOIvec = spam.label.momentOfInertia(longSingle, centresOfMass=COM)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), MOIval[0].tolist())
        self.assertEqual(numpy.zeros(9, dtype="<f4").tolist(), MOIvec[0].tolist())
        self.assertTrue(MOIval[1, 0] > MOIval[1, 1])
        self.assertTrue(MOIval[1, 1] > MOIval[1, 2])
        self.assertTrue(MOIval[1, 2] > 0.0)
        self.assertEqual(numpy.array([0, 0, 1], dtype="<f4").tolist(), MOIvec[1, 0:3].tolist())
        self.assertEqual(numpy.array([0, 1, 0], dtype="<f4").tolist(), MOIvec[1, 3:6].tolist())
        self.assertEqual(numpy.array([1, 0, 0], dtype="<f4").tolist(), MOIvec[1, 6:9].tolist())

    def test_ellipseAxes(self):
        BB = spam.label.boundingBoxes(longSingle)
        EA = spam.label.ellipseAxes(longSingle)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), EA[0].tolist())
        self.assertTrue(EA[1, 0] > EA[1, 1])
        self.assertTrue(EA[1, 1] > EA[1, 2])
        self.assertTrue(EA[1, 2] > 0.0)
        self.assertTrue(EA[1, 0] < (BB[1, 1] - BB[1, 0]))
        self.assertTrue(EA[1, 0] > (BB[1, 1] - BB[1, 0]) / 2.0)
        self.assertTrue(EA[1, 1] < (BB[1, 3] - BB[1, 2]))
        self.assertTrue(EA[1, 1] > (BB[1, 3] - BB[1, 2]) / 2.0)
        # self.assertTrue( EA[1,2]<(BB[1,5]-BB[1,4]))
        self.assertTrue(EA[1, 2] > (BB[1, 5] - BB[1, 4]) / 2.0)

        EA = spam.label.ellipseAxes(longSingle, enforceVolume=False)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), EA[0].tolist())
        self.assertTrue(EA[1, 0] > EA[1, 1])
        self.assertTrue(EA[1, 1] > EA[1, 2])
        self.assertTrue(EA[1, 2] > 0.0)
        self.assertTrue(EA[1, 0] < (BB[1, 1] - BB[1, 0]))
        self.assertTrue(EA[1, 0] > (BB[1, 1] - BB[1, 0]) / 2.0)
        self.assertTrue(EA[1, 1] < (BB[1, 3] - BB[1, 2]))
        self.assertTrue(EA[1, 1] > (BB[1, 3] - BB[1, 2]) / 2.0)
        # self.assertTrue( EA[1,2]<(BB[1,5]-BB[1,4]))
        self.assertTrue(EA[1, 2] > (BB[1, 5] - BB[1, 4]) / 2.0)

        MOIval = spam.label.momentOfInertia(longSingle)[0]
        EA = spam.label.ellipseAxes(longSingle, MOIeigenValues=MOIval)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), EA[0].tolist())
        self.assertTrue(EA[1, 0] > EA[1, 1])
        self.assertTrue(EA[1, 1] > EA[1, 2])
        self.assertTrue(EA[1, 2] > 0.0)
        self.assertTrue(EA[1, 0] < (BB[1, 1] - BB[1, 0]))
        self.assertTrue(EA[1, 0] > (BB[1, 1] - BB[1, 0]) / 2.0)
        self.assertTrue(EA[1, 1] < (BB[1, 3] - BB[1, 2]))
        self.assertTrue(EA[1, 1] > (BB[1, 3] - BB[1, 2]) / 2.0)
        # self.assertTrue( EA[1,2]<(BB[1,5]-BB[1,4]))
        self.assertTrue(EA[1, 2] > (BB[1, 5] - BB[1, 4]) / 2.0)

        volumes = spam.label.volumes(longSingle)
        EA = spam.label.ellipseAxes(longSingle, volumes=volumes)
        self.assertEqual(numpy.zeros(3, dtype="<f4").tolist(), EA[0].tolist())
        self.assertTrue(EA[1, 0] > EA[1, 1])
        self.assertTrue(EA[1, 1] > EA[1, 2])
        self.assertTrue(EA[1, 2] > 0.0)
        self.assertTrue(EA[1, 0] < (BB[1, 1] - BB[1, 0]))
        self.assertTrue(EA[1, 0] > (BB[1, 1] - BB[1, 0]) / 2.0)
        self.assertTrue(EA[1, 1] < (BB[1, 3] - BB[1, 2]))
        self.assertTrue(EA[1, 1] > (BB[1, 3] - BB[1, 2]) / 2.0)
        # self.assertTrue( EA[1,2]<(BB[1,5]-BB[1,4]))
        self.assertTrue(EA[1, 2] > (BB[1, 5] - BB[1, 4]) / 2.0)

    def test_convertLabelToFloat(self):
        f = spam.label.convertLabelToFloat(threeCubedLabelVol, numpy.array([0.0, numpy.pi]))
        self.assertAlmostEqual(f[1, 1, 1], float(numpy.pi), places=5)

    def test_makeLabelsSequential(self):
        # threeCubedLabelVolDouble = threeCubedLabelVol.copy() * 2
        f = spam.label.makeLabelsSequential(threeCubedLabelVol)
        self.assertEqual(f[1, 1, 1], 1)

    def test_getLabel(self):
        # Test wrong label
        gottenLabel = spam.label.getLabel(threeCubedLabelVol, 2)
        self.assertTrue(gottenLabel == None)
        # No-option get label
        gottenLabel = spam.label.getLabel(threeCubedLabelVol, 1)
        self.assertEqual(numpy.array([[[1]]], dtype=bool), gottenLabel["subvol"].tolist())
        self.assertEqual(gottenLabel["slice"][0].start, 1)
        self.assertEqual(gottenLabel["slice"][0].stop, 2)
        self.assertEqual(gottenLabel["slice"][1].start, 1)
        self.assertEqual(gottenLabel["slice"][1].stop, 2)
        self.assertEqual(gottenLabel["slice"][2].start, 1)
        self.assertEqual(gottenLabel["slice"][2].stop, 2)

        self.assertEqual(gottenLabel["boundingBox"][0], 1)
        self.assertEqual(gottenLabel["boundingBox"][1], 1)
        self.assertEqual(gottenLabel["boundingBox"][2], 1)
        self.assertEqual(gottenLabel["boundingBox"][3], 1)
        self.assertEqual(gottenLabel["boundingBox"][4], 1)
        self.assertEqual(gottenLabel["boundingBox"][5], 1)

        COM = spam.label.centresOfMass(threeCubedLabelVol)
        self.assertEqual(COM[1].tolist(), gottenLabel["centreOfMassABS"].tolist())
        self.assertEqual(
            (COM[1] - numpy.array((1, 1, 1))).tolist(),
            gottenLabel["centreOfMassREL"].tolist(),
        )
        self.assertEqual(gottenLabel["volumeInitial"], 1)

        BB = spam.label.boundingBoxes(threeCubedLabelVol)
        # With extract cube
        gottenLabel = spam.label.getLabel(threeCubedLabelVol, 1, boundingBoxes=BB, centresOfMass=COM, extractCube=True)
        self.assertEqual(gottenLabel["sliceCube"][0].start, 1)
        self.assertEqual(gottenLabel["sliceCube"][0].stop, 2)
        self.assertEqual(gottenLabel["sliceCube"][1].start, 1)
        self.assertEqual(gottenLabel["sliceCube"][1].stop, 2)
        self.assertEqual(gottenLabel["sliceCube"][2].start, 1)

        self.assertEqual(gottenLabel["boundingBoxCube"][0], 1)
        self.assertEqual(gottenLabel["boundingBoxCube"][1], 1)
        self.assertEqual(gottenLabel["boundingBoxCube"][2], 1)
        self.assertEqual(gottenLabel["boundingBoxCube"][3], 1)
        self.assertEqual(gottenLabel["boundingBoxCube"][4], 1)
        self.assertEqual(gottenLabel["boundingBoxCube"][5], 1)

        # print gottenLabel['slice']
        COM = spam.label.centresOfMass(threeCubedLabelVol)
        self.assertEqual(COM[1].tolist(), gottenLabel["centreOfMassABS"].tolist())
        self.assertEqual(
            (COM[1] - numpy.array((1, 1, 1))).tolist(),
            gottenLabel["centreOfMassREL"].tolist(),
        )
        self.assertEqual(gottenLabel["volumeInitial"], 1)

        # Asking for a label that is not there
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol,
            100,
            boundingBoxes=BB,
            centresOfMass=COM,
            extractCube=True,
        )
        self.assertEqual(gottenLabel, None)

        # Just run the remaining cases
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol,
            1,
            boundingBoxes=BB,
            centresOfMass=COM,
            extractCube=True,
            extractCubeSize=10,
        )
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol,
            1,
            boundingBoxes=BB,
            centresOfMass=COM,
            extractCube=True,
            extractCubeSize=0,
        )
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol,
            1,
            boundingBoxes=BB,
            centresOfMass=COM,
            margin=1,
            labelDilate=-2,
        )
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol,
            1,
            boundingBoxes=BB,
            centresOfMass=COM,
            margin=2,
            labelDilate=2,
        )
        volRef = gottenLabel["volumeInitial"]

        # New case for "labelDilateMaskOtherLabels"
        threeCubedLabelVol2 = threeCubedLabelVol.copy()
        threeCubedLabelVol2[0, 1, 1] = 2
        gottenLabel = spam.label.getLabel(
            threeCubedLabelVol2,
            1,
            boundingBoxes=BB,
            centresOfMass=COM,
            margin=2,
            labelDilate=2,
            labelDilateMaskOtherLabels=True,
        )
        # print(gottenLabel['volume'], volRef)
        self.assertEqual(gottenLabel["volumeDilated"] > volRef, True)

        # GP: With extract cube size - check when it is too small
        gottenLabel = spam.label.getLabel(longSingle, 1, extractCube=True, extractCubeSize = 1)
        self.assertEqual(gottenLabel["sliceCube"][0].start, 3)
        self.assertEqual(gottenLabel["sliceCube"][0].stop, 6)
        self.assertEqual(gottenLabel["sliceCube"][1].start, 2)
        self.assertEqual(gottenLabel["sliceCube"][1].stop, 5)
        self.assertEqual(gottenLabel["sliceCube"][2].start, 0)

        self.assertEqual(gottenLabel["boundingBoxCube"][0], 3)
        self.assertEqual(gottenLabel["boundingBoxCube"][1], 5)
        self.assertEqual(gottenLabel["boundingBoxCube"][2], 2)
        self.assertEqual(gottenLabel["boundingBoxCube"][3], 4)
        self.assertEqual(gottenLabel["boundingBoxCube"][4], 0)
        self.assertEqual(gottenLabel["boundingBoxCube"][5], 2)

    def test_labelsOnEdges(self):
        labelled = numpy.zeros((5, 5, 5), dtype="<u2")
        labelled[1, 2, 1] = 1
        labelled[1, 1, 1] = 2
        labelled[3, 3, 3] = 2
        labelled[3, 3, 2] = 4
        labelled[1, 1, 2] = 4
        labelled[2, 2, 2] = 5
        labelled[0, 0, 0] = 6
        labelled[0, 4, 4] = 7

        loe = spam.label.labelsOnEdges(labelled)
        self.assertEqual(loe.tolist(), [0, 6, 7])

    def test_removeLabels(self):
        f = spam.label.removeLabels(longSingle, numpy.array([1]))
        self.assertEqual(f.max(), 0)

    #     def test_feretDiameters(self):
    #     import scipy.ndimage
    #     # Very simple test for radii tested on a sphere
    #     radius = 20
    #     im = numpy.zeros((50, 50, 50), dtype='<f8')
    #     spam.kalisphera.makeSphere(im, (25,25,25), radius)
    #     im = im > 0.5
    #     diams = spam.label.feretDiameters(im)[0][1]
    #     self.assertAlmostEqual(diams[0], radius*2, delta=1)
    #     self.assertAlmostEqual(diams[1], radius*2, delta=1)
    #
    #     imSquish = scipy.ndimage.zoom(im, (0.5, 1, 1), order=0)
    #     imSquish = imSquish > 0.5
    #     diams = spam.label.feretDiameters(imSquish)[0][1]
    #     self.assertAlmostEqual(diams[0], radius*2, delta=1)
    #     self.assertAlmostEqual(diams[1], radius*2/2, delta=1)

    def test_trueSphericity(self):
        def kaliToTS(v, r):
            vol = numpy.zeros((v, v, v), dtype="f8")
            spam.kalisphera.makeSphere(vol, (v / 2, v / 2, v / 2), r)
            vol = vol > 0.5
            return spam.label.trueSphericity(vol, gaussianFilterSigma=0.75)[1]

        # This is too small, should be ignored
        self.assertEqual(kaliToTS(50, 2), 0.0)
        self.assertAlmostEqual(kaliToTS(50, 5), 1.0, places=1)
        self.assertAlmostEqual(kaliToTS(50, 10), 1.0, places=1)
        self.assertAlmostEqual(kaliToTS(50, 15), 1.0, places=1)

        # Make sure with a box
        vol = numpy.zeros((50, 50, 50), dtype=spam.label.labelType)
        vol[0:40, 0:40, 0:40] = 1
        self.assertAlmostEqual(
            spam.label.trueSphericity(vol, gaussianFilterSigma=0.5)[1],
            (36.0 * numpy.pi * (vol.sum() ** 2.0)) ** (1 / 3.0) / (6 * vol.sum() ** (2 / 3.0)),
            places=1,
        )

    #     def test_loadDEMdata(self):
    #         import spam.datasets
    #
    #         data = spam.datasets.loadDEMtouchingGrainsAndBranchVector()

    #     def
    #         print gl['subvol'].tolist()
    #         print spam.label.ellipseAxes(longSingle, enforceVolume=False )
    # class TestFonctionGet(unittest.TestCase):
    #
    #     def tearDown(self):
    #         try:
    #             pass
    #         except OSError:
    #             pass
    #
    #     def test_volumesCOM( self ):
    #         self.assertEqual(0,0)
    #
    #         # Define test volume
    #         testVolDims = ( 50, 50, 50 )
    #         margin = 1
    #         minRadius = 3
    #
    #         ### Kalisphera setup for random radius and centre
    #         # Random parameters for sphere, making sure it fits in the box and is bigger than 3px:
    #         radius = minRadius + ( numpy.random.rand() * ( ( numpy.min( testVolDims ) / 2.0 ) - minRadius ) * 0.8 )
    #
    #         #randomThree = numpy.array([0.5,0.5,0.5])
    #         randomThree = numpy.random.rand(3)
    #
    #         # Generate random centre with margin
    #         #      ---offset to corner---     --- three random numbers * difference between vol dims and 2*radius ---
    #         centre = 3*[radius+margin]     +  ( randomThree * ( numpy.array( testVolDims ) - 2.0 * numpy.array( ( 3*[radius+margin] )  ) ) )
    #         #print centre
    #
    #         # Create empy array
    #         perfectSphere =  numpy.zeros( testVolDims, dtype="<f8" )
    #
    #         # Call kalisphera to fill in array
    #         if VERBOSE: print( "\ttest_label: Creating greyscale sphere of radius {:0.2f} at {}".format( radius, centre ) )
    #         kalisphera.run( perfectSphere, numpy.array( centre ).astype('<f8'), float(radius) )
    #
    #         ### Check kalisphera created greyscale volume
    #         # Let's check the volume
    #         theoreticalVolume = (4.0/3.0) * numpy.pi * radius**3.0
    #         greyscaleVolume = perfectSphere.sum()
    #         normGreyDifference = ( (theoreticalVolume-greyscaleVolume)/theoreticalVolume )
    #
    #         if VERBOSE: print( "\ttest_label: theoretical volume = {:0.2f}vx, created volume = {:0.2f}vx".format( theoreticalVolume, greyscaleVolume ) )
    #         if VERBOSE: print( "\ttest_label: Difference = {:0.5f}%\n".format( 100.0*normGreyDifference ) )
    #
    #         ### Binary tests
    #         # binarise this sphere at partial volume of 0.5s
    #         binary = perfectSphere > 0.5
    #         binaryVolume = binary.sum()
    #         normBinDifference = ( (theoreticalVolume-binaryVolume)/theoreticalVolume )
    #         if VERBOSE: print( "\ttest_label: binary volume = {:0.2f}vx".format( binaryVolume ) )
    #         if VERBOSE: print( "\ttest_label: Difference = {:0.5f}%".format( 100.0*normBinDifference ) )
    #         ltkVolume = ltk.getVolumes( binary )[1]
    #         if VERBOSE: print( "\ttest_label: difference between binary sum and ltk.getVolumes(): {} (should be zero and integer)\n".format( binaryVolume - ltkVolume ) )
    #
    #         binaryCOM = ltk.getCentresOfMass( binary )[1]
    #         if VERBOSE: print( "\ttest_label: difference between binary and imposed sphere centre Z,Y,X: {}".format( numpy.subtract( centre, binaryCOM  ) ) )
    #
    #     def test_boundingBoxes( self ):
    #         ### new test: on a 3x3x3 array, test all single-pixel labels and check that label parameters
    #         ###  i.e., boundingBoxes, COM, and volume are correctly labelled.
    #         for z in [ 0,1,2 ]:
    #             for y in [ 0,1,2 ]:
    #                 for x in [ 0,1,2 ]:
    #                     nugget = numpy.zeros( ( 3, 3, 3 ) )
    #                     nugget[ z, y, x ] = 1
    #
    #                     bbo = ltk.getBoundingBoxes( nugget )[1]
    #                     com = ltk.getCentresOfMass( nugget )[1]
    #                     vol = ltk.getVolumes( nugget )[1]
    #
    #                     if ( bbo != numpy.array( [z,z,y,y,x,x] ) ).any():
    #                         print "boundingBox mismatch", bbo
    #
    #                     if ( com != numpy.array( [z+0.5,y+0.5,x+0.5] )).any():
    #                         print "com problem", com
    #
    #                     if ( vol != 1 ).any():
    #                         print "vol != 1", vol
    #
    #     def test_missingLabel( self ):
    #         nugget = numpy.zeros( ( 3, 3, 3 ) )
    #         nugget[ 1, 1, 1 ] = 2
    #         #print ltk.getBoundingBoxes( nugget )
    #
    #     def test_iterator( self ):
    #         ### new test: on a 3x3x3 array, test all single-pixel labels and check that label parameters
    #         ###  i.e., boundingBoxes, COM, and volume are correctly labelled.
    #         labelled = numpy.zeros( (5,5,5), dtype="<u2" )
    #         labelled[ 1,2,1 ] = 1
    #         labelled[ 1,1,1 ] = 2
    #         labelled[ 3,3,3 ] = 2
    #         labelled[ 3,3,2 ] = 4
    #         labelled[ 1,1,2 ] = 4
    #         labelled[ 2,2,2 ] = 5
    #
    #         bbo = ltk.getBoundingBoxes( labelled )
    #         com = ltk.getCentresOfMass( labelled )
    #         vol = ltk.getVolumes( labelled )
    #
    #         #for label in range( 1, labelled.max()+3 ):
    #         for label in [5]:
    #             returns = ltk.getLabel( labelled, label, boundingBoxes = bbo, centresOfMass = com, extractCube=False, margin=1 )
    #
    #             if type(returns) == dict:
    #                 for key in returns.keys():
    #                     print key, returns[key]
    #
    #             #print returns['subvol'].shape
    #             #print numpy.where( returns['subvol'] == label )

    #     def _test_Spheroid(self):
    #     # Test for Oblate (Lentil-shaped)
    #
    #     # Generate random direction
    #     theta = numpy.radians(random.randrange(0,360,1))
    #     phi = numpy.radians(random.randrange(0,90,1))
    #     vect = numpy.array([numpy.cos(phi), numpy.sin(phi)*numpy.sin(theta), numpy.sin(phi)*numpy.cos(theta)])
    #     if vect[0]<0: vect[:]=-1*vect[:]
    #     # Generate two random semi-axis values
    #     a = random.randrange(20,40,1)
    #     c = random.randrange(10,20,1)
    #     #Generate the spheroid
    #     spheroid = spam.label.label.Spheroid(a, c, numpy.asarray(vect)).digitize()
    #     # Compute its semi-axis
    #     semiAxis = spam.label.ellipseAxes(spheroid)
    #     # Compare the semi-axis
    #     self.assertLess(numpy.abs(numpy.max(semiAxis[1])-numpy.maximum(a,c)),2)
    #     self.assertLess(numpy.abs(numpy.min(semiAxis[1])-numpy.minimum(a,c)),2)
    #     # Compute the orientation
    #     eigenVal, eigenVect = spam.label.momentOfInertia(spheroid)
    #     eigenVal = eigenVal / numpy.max(eigenVal)
    #     # Get main orientation
    #     mainVect = eigenVect[1][0:3]
    #     if mainVect[0]<0: mainVect[:]=-1*mainVect[:]
    #     # Compute the angle between them
    #     c = numpy.dot(vect,mainVect)/numpy.linalg.norm(vect)/numpy.linalg.norm(mainVect)
    #     angle = numpy.degrees(numpy.arccos(numpy.clip(c, -1, 1)))
    #     # Check angle less than 2 degree
    #     self.assertLess(angle,2)
    #
    #     # Test for Prolate (Rice-shaped)
    #
    #     # Generate random direction
    #     theta = numpy.radians(random.randrange(0,360,1))
    #     phi = numpy.radians(random.randrange(0,90,1))
    #     vect = numpy.array([numpy.cos(phi), numpy.sin(phi)*numpy.sin(theta), numpy.sin(phi)*numpy.cos(theta)])
    #     if vect[0]<0: vect[:]=-1*vect[:]
    #     # Generate two random semi-axis values
    #     a = random.randrange(10,20,1)
    #     b = random.randrange(20,40,1)
    #     # Generate the spheroid
    #     spheroid = spam.label.label.Spheroid(a, b, numpy.asarray(vect)).digitize()
    #     # Compute its semi-axis
    #     semiAxis = spam.label.ellipseAxes(spheroid)
    #     # Compare the semi-axis
    #     self.assertLess(numpy.abs(numpy.max(semiAxis[1])-numpy.maximum(a,b)),2)
    #     self.assertLess(numpy.abs(numpy.min(semiAxis[1])-numpy.minimum(a,b)),2)
    #     # Compute the orientation
    #     eigenVal, eigenVect = spam.label.momentOfInertia(spheroid)
    #     eigenVal = eigenVal / numpy.max(eigenVal)
    #     # Get main orientation
    #     mainVect = eigenVect[1][6:9]
    #     if mainVect[0]<0: mainVect[:]=-1*mainVect[:]
    #     # Compute the angle between them
    #     c = numpy.dot(vect,mainVect)/numpy.linalg.norm(vect)/numpy.linalg.norm(mainVect)
    #     angle = numpy.degrees(numpy.arccos(numpy.clip(c, -1, 1)))
    #     # Check angle less than 2 degree
    #     self.assertLess(angle,2)
    #
    #     # Check that raises an error when the vector and dim are passed along
    #     with self.assertRaises(ValueError): spam.label.label.Spheroid(10, 20, numpy.asarray([0,1,0]), dim = 1).digitize()
    #     # Check that it runs even without a vector
    #     res = spam.label.label.Spheroid(10, 20, dim = 3).digitize()
    #     self.assertIsNotNone(res)
    #
    #     def _test_FixUnderSegmentation(self):
    #     # Generate two prolate grains (rice-like)
    #     grain1 = spam.label.label.Spheroid(10, 20, numpy.asarray([0,1,0])).digitize()
    #     grain2 = spam.label.label.Spheroid(10, 20, numpy.asarray([0,1,0])).digitize()
    #     # Create the bigger  labelled image
    #     grainIm = numpy.concatenate((grain1,grain2))
    #     grainIm = numpy.zeros(grainIm.shape)
    #     # Add the grains to the bigger image
    #     grainIm[:grain1.shape[0]-1,:,:] = grain1[:grain1.shape[0]-1,:,:]
    #     grainIm[grain2.shape[0]-5:-5,:,:] =  grainIm[grain2.shape[0]-5:-5,:,:] + grain1[:,:,:]
    #     # Set all the labels to 1
    #     grainIm = numpy.where(grainIm >= 1, 3, grainIm)
    #     # Pad a border
    #     grainIm = numpy.pad(grainIm, pad_width=10, mode='constant', constant_values = 0)
    #     # Create the 'greyScale' image
    #     greyIm = numpy.where(grainIm == 3, 30000, 10000)
    #
    #     # Check that the greyscale image is normalized
    #     res1 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.5, 20*0.5)
    #     self.assertEqual(res1, None)
    #     # Check that a or c is valid
    #     res2 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], numpy.nan, numpy.nan)
    #     self.assertEqual(res2, None)
    #     # Check that a or c are positive
    #     res3 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], -1, 10)
    #     self.assertEqual(res3, None)
    #
    #     # Run fixUnderSegmentation
    #     greyIm = numpy.where(grainIm == 3, 0.75, 0.25)
    #     res4 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.8, 20*0.8, vect = [[0,1,0]])
    #     # Check that there are two grains
    #     self.assertEqual(numpy.max(numpy.unique(res4)), 2)
    #     # Check that it runs even if the label does not exist
    #     res5 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.8, 20*0.8, vect = [[0,1,0]])
    #     self.assertIsNotNone(res5)
    #     # Check for a vect that is not a list
    #     res6 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.8, 20*0.8, vect = (0,1,0))
    #     self.assertIsNone(res6)
    #     # Check that it works without the input vect
    #     res7 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.8, 20*0.8, numVect = 1)
    #     self.assertIsNotNone(res7)
    #     # Check that it works with verbose = True
    #     res8 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [3], 10*0.8, 20*0.8, numVect = 1, verbose = True)
    #     self.assertIsNotNone(res8)
    #     # Check that it works even for a non-existing label
    #     res9 = spam.label.label.fixUndersegmentation(grainIm, greyIm, [5], 10*0.8, 20*0.8, numVect = 1)
    #     self.assertIsNotNone(res9)

    def test_contactOrientations(self):
        # Create two rice-like grain
        imGrey = spam.kalisphera.makeBlurryNoisySpheroid(
            [40, 80, 40],
            [[11, 40, 20], [29, 40, 20]],
            [[10, 20], [10, 20]],
            [[0, 1, 0], [0, 1, 0]],
        )
        # Binary
        imBin = imGrey > 0.5
        # Label
        imLab = spam.label.watershed(imBin)
        # Run ITK and check angle of contact
        contactNormal, intervox, NotTreatedContact = con.contactOrientations(imBin, imLab, watershed="ITK")
        if contactNormal[0] < 0:
            contactNormal = contactNormal * -1
        c = numpy.dot(contactNormal, [1, 0, 0]) / numpy.linalg.norm(contactNormal) / numpy.linalg.norm([1, 0, 0])
        angle = numpy.degrees(numpy.arccos(numpy.clip(c, -1, 1)))
        self.assertFalse(NotTreatedContact)
        self.assertLess(angle, 1.0)
        # Run RW and check angle of contact
        contactNormal, intervox, NotTreatedContact = con.contactOrientations(imBin, imLab, watershed="RW")
        if contactNormal[0] < 0:
            contactNormal = contactNormal * -1
        c = numpy.dot(contactNormal, [1, 0, 0]) / numpy.linalg.norm(contactNormal) / numpy.linalg.norm([1, 0, 0])
        angle = numpy.degrees(numpy.arccos(numpy.clip(c, -1, 1)))
        self.assertFalse(NotTreatedContact)
        self.assertLess(angle, 1.0)

    def test_setVoronoi(self):
        box = numpy.zeros((100, 100, 100), dtype="<f8")
        spam.kalisphera.makeSphere(box, [[50, 50, 50 - 20], [50, 50, 50 + 20]], [20, 20])
        box = box > 0.5
        lab = spam.label.watershed(box)
        labVolumes = spam.label.volumes(lab)

        setVoronoi = spam.label.setVoronoi(lab)
        setVoronoiVolumes = spam.label.volumes(setVoronoi)
        self.assertEqual(setVoronoiVolumes[1] > labVolumes[1], True)
        self.assertEqual(setVoronoiVolumes[2] > labVolumes[2], True)

    def test_convexVolume(self):
        # Get a random separation between 1 and 10
        dist = int(10 * numpy.random.rand())
        # Create a rectangular image
        im = numpy.zeros((15, 15, 30))
        # Create a first block of 5x5x5
        im[5:10, 5:10, 5:10] = 1
        # Create second block of 5x5x5 located at dist
        im[5:10, 5:10, 10 + dist : 15 + dist] = 1
        # Compute convex volume
        im = im.astype("<u4")
        convexVol = spam.label.convexVolume(im)
        # Compute theoretical volume
        volOr = numpy.sum(im) + 5 * 5 * dist
        self.assertEqual(volOr, convexVol[-1])
        # Check that it shows the error for particles smaller than 3 voxels
        im = numpy.zeros((15, 15, 30))
        im[10, 10, 10] = 1
        # Compute convex volume
        im = im.astype("<u4")
        convexVol = spam.label.convexVolume(im)
        self.assertEqual(0, convexVol[-1])

    def test_moveLabels(self):
        # Create the sphere
        imLab = skimage.morphology.ball(20)
        # Pad with zero at each boundaries
        imLab = numpy.pad(imLab, (10), "constant", constant_values=(0))
        # Compute initial volume
        iniVol = spam.label.volumes(imLab)[-1]
        # Compute initial COM
        iniCOM = spam.label.centresOfMass(imLab)[-1]
        # Compute boundingBoxes
        boundingBoxes = spam.label.boundingBoxes(imLab)
        # Compute centre of Mass
        centresOfMass = spam.label.centresOfMass(imLab)
        # Create empty PhiField
        PhiField = numpy.zeros((2, 4, 4))

        # Test #1 -> COM=iniCOM, vol = iniVol for no dilate and Phi = I
        transformation = {"t": [0, 0, 0]}
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniVol - newVol))
        self.assertEqual(0, numpy.sum(iniCOM - newCOM))
        # Test #2 -> COM=iniCOM, vol = +10% volIni for a dilation of 10%
        transformation = {"z": [1.1, 1, 1]}
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniCOM - newCOM))
        self.assertAlmostEqual(1.1, newVol / iniVol, places=1)
        # Test #3 -> COM=iniCOM and vol/ iniVol > 1 for dilate = 1 and Phi = I
        transformation = {"t": [0, 0, 0]}
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(
            imLab,
            PhiField,
            boundingBoxes=boundingBoxes,
            centresOfMass=centresOfMass,
            labelDilate=1,
        )
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniCOM - newCOM))
        self.assertGreater(newVol / iniVol, 1.0)
        # Test#4 -> vol=iniVol and COM follows the 5vox displacement
        transformation = {"t": [5, 5, 5]}
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniVol - newVol))
        self.assertTrue(((newCOM - iniCOM) == 5).all())
        # Test 5 -> move grain half out and check volume is near 0.5
        transformation = {
            "t": [
                30,
                0,
                0,
            ]
        }
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertAlmostEqual(0.5, newVol / iniVol, places=1)
        # Test 6A -> Check that it runs when the PhiField has less labels than the labelled image
        PhiField = numpy.zeros((1, 4, 4))
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        self.assertIsNotNone(imLab2)
        # Test 6B -> Check that it runs when the PhiField has more labels than the labelled image
        PhiField = numpy.zeros((3, 4, 4))
        imLab2 = spam.label.moveLabels(imLab, PhiField, boundingBoxes=boundingBoxes, centresOfMass=centresOfMass)
        self.assertIsNotNone(imLab2)

        # Test 7A -> Check that it works if the return status is equal to 2
        transformation = {"t": [5, 5, 5]}
        PhiField[1] = spam.deformation.computePhi(transformation)
        imLab2 = spam.label.moveLabels(
            imLab,
            PhiField,
            returnStatus=numpy.array([0, 2]),
            boundingBoxes=boundingBoxes,
            centresOfMass=centresOfMass,
        )
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniVol - newVol))
        self.assertTrue(((newCOM - iniCOM) == 5).all())
        # Test 7B -> Check that it works if the return status is different to 2
        imLab2 = spam.label.moveLabels(
            imLab,
            PhiField,
            returnStatus=numpy.array([0, 0]),
            boundingBoxes=boundingBoxes,
            centresOfMass=centresOfMass,
        )
        self.assertEqual(1, len(numpy.unique(imLab2)))

    def test_erodeLabels(self):
        # Create the sphere
        imLab = skimage.morphology.ball(20)
        # Pad with zero at each boundaries
        imLab = numpy.pad(imLab, (10), "constant", constant_values=(0))
        # Compute initial volume
        iniVol = spam.label.volumes(imLab)[-1]
        # Compute initial COM
        iniCOM = spam.label.centresOfMass(imLab)[-1]
        # Test #1 -> COM=iniCOM, vol = iniVol for no dilate and Phi = I
        imLab2 = spam.label.erodeLabels(imLab, erosion=0)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniVol - newVol))
        self.assertEqual(0, numpy.sum(iniCOM - newCOM))
        # Test #2 -> COM=iniCOM and vol/ iniVol < 1 for dilate = 1 and Phi = I
        imLab2 = spam.label.erodeLabels(imLab, erosion=1)
        # Compute Volume and COM
        newVol = spam.label.volumes(imLab2)[-1]
        newCOM = spam.label.centresOfMass(imLab2)[-1]
        self.assertEqual(0, numpy.sum(iniCOM - newCOM))
        self.assertGreater(1.0, newVol / iniVol)

    def test_convexFillHoles(self):
        # Generate two spheres with Kalisphera
        grainIm = spam.kalisphera.makeBlurryNoisySphere([60, 60, 60], [[15, 30, 30], [45, 30, 30]], [10, 10])
        # Label
        grainIm = spam.label.watershed(grainIm > 0.5)
        # Compute initial volume & COM
        COM = spam.label.centresOfMass(grainIm)
        iniVol = spam.label.volumes(grainIm).astype(int)
        # Dry run of the function
        res1 = spam.label.convexFillHoles(grainIm)
        # Compute new volume
        res1Vol = spam.label.volumes(res1).astype(int)
        # Test#1 -> Check that the volumes are the same
        self.assertEqual(res1Vol[1], iniVol[1])
        self.assertEqual(res1Vol[2], iniVol[2])
        # Add a marker in the center of one of the labels
        grainIm[COM[1][0].astype(int), COM[1][1].astype(int), COM[1][2].astype(int)] = 2
        # Run again
        res2 = spam.label.convexFillHoles(grainIm)
        # Compute final volume
        endVol = spam.label.volumes(res2).astype(int)
        # Compare volumes
        normVol1 = (endVol[1] - iniVol[1]) / iniVol[1]
        normVol2 = (endVol[2] - iniVol[2]) / iniVol[2]
        # Test#2 -> Check that Vol1 is the same and Vol2 increased
        self.assertAlmostEqual(normVol1, 0, places=3)
        self.assertGreater(normVol2, 0)

    def test_getNeighbours(self):
        # Create a box with 9 spheres
        # Radius
        r = 10
        # Coordinates
        a = 4 * r / numpy.sqrt(3)
        coord = numpy.zeros((9, 3))
        coord[0] = [0, 0, 0]
        coord[1] = [0, 0, a]
        coord[2] = [0, a, 0]
        coord[3] = [0, a, a]
        coord[4] = [a / 2, a / 2, a / 2]
        coord[5] = [a, 0, 0]
        coord[6] = [a, 0, a]
        coord[7] = [a, a, 0]
        coord[8] = [a, a, a]
        coord = coord + r
        # Create the box
        box = spam.kalisphera.makeBlurryNoisySphere(numpy.ceil(numpy.max(coord + r, axis=0)).astype(int), coord, r)
        # Segment
        labBox = spam.label.watershed(box > 0.5)
        # Compute neighbours for the center sphere with both methods
        neighbours1 = spam.label.getNeighbours(labBox, [5], method="mesh")
        neighbours2 = spam.label.getNeighbours(labBox, [5], method="getLabel")
        # Check that the result is the same for both methods
        self.assertTrue(numpy.array_equal(neighbours1, neighbours2))
        # Compute neighbours for the corner sphere with both methods
        neighbours1 = spam.label.getNeighbours(labBox, [1], method="mesh")
        neighbours2 = spam.label.getNeighbours(labBox, [1], method="getLabel")
        # Check that the result is not the same for both methods
        self.assertFalse(numpy.array_equal(neighbours1, neighbours2))
        # GP: Adding test for wrong method
        neighbours1 = spam.label.getNeighbours(labBox, [1], method="meshs")
        self.assertEqual(len(neighbours1), 0)

    def test_detectFixOverSegmentation(self):
        # 1. Detect!
        # Create the seeds empty box
        box = numpy.zeros((100, 100, 100), dtype="<f8")
        # Create the 3 seeds
        spam.kalisphera.makeSphere(box, [[50, 50, 30], [50, 50, 50], [50, 50, 60]], [15, 5, 5])
        # Labels the seeds
        imMarkLab = spam.label.watershed(box > 0.5)
        # Create label empty box
        binIm = numpy.zeros((100, 100, 100), dtype="<f8")
        # Create 2 particles
        spam.kalisphera.makeSphere(binIm, [[50, 50, 30], [50, 50, 60]], [15, 15])
        # Binarise
        binIm = binIm > 0.5
        # Label the 2 particles with the 3 seeds - creating oversegmentation of one particle!
        imLabel = spam.label.watershed(binIm, markers=imMarkLab)
        # Compute oversegmentation coefficient
        overSegCoeff, sharedLabel = spam.label.detectOverSegmentation(imLabel)
        # Check
        self.assertEqual(numpy.argmax(overSegCoeff), 2)
        # 2. Fix
        imTest = spam.label.fixOversegmentation(imLabel, [2], sharedLabel)
        # Check
        overSegCoeff, sharedLabel = spam.label.detectOverSegmentation(imTest)
        self.assertAlmostEqual(overSegCoeff[1], overSegCoeff[2], places=2)

    def test_detectFixUnderSegmentation(self):
        # 1. Detect
        # Create the box with two spheres
        box = spam.kalisphera.makeBlurryNoisySphere([100, 100, 100], [[50, 50, 30], [50, 50, 60]], [15, 15], blur=1)
        # Segment
        imLabel = spam.label.watershed(box > 0.5)
        # Change the labels to generate only one
        imSeed = numpy.where(imLabel == 2, 1, imLabel)
        # Segment with the wrong seeds
        imLabel = spam.label.watershed(box > 0.5, markers=imSeed)
        # Run the detection of undersegmentation
        underSegCoeff = spam.label.detectUnderSegmentation(imLabel, verbose=True)
        # Check
        self.assertTrue(numpy.max(underSegCoeff) > 1)
        # 2. Fix it
        imTest = spam.label.fixUndersegmentation(imLabel, box, [1], underSegCoeff, imShowProgress=False)
        # Run the detection of undersegmentation
        underSegCoeff = spam.label.detectUnderSegmentation(imTest, verbose=True)
        # Check
        self.assertAlmostEqual(underSegCoeff[1], underSegCoeff[2], places=2)
        # 3. Check for the case where the greyscale are not normalised
        res = spam.label.fixUndersegmentation(imLabel, 2*box, [1], underSegCoeff, imShowProgress=False)
        self.assertTrue(res == None)

    def test_shuffleLabels(self):

        # Create a label image as per kalisphera example
        pixelSize = 60.e-6
        blurSTD = 0.8
        noiseSTD = 0.03
        boxSizeDEM, centres, radii = spam.datasets.loadDEMboxsizeCentreRadius()
        rMax = numpy.amax(radii)
        boxSize = boxSizeDEM + 3 * rMax
        centres[:, :] = centres[:, :] + 1.5 * rMax
        boxSize = int(numpy.ceil(numpy.max(boxSize[:]) / pixelSize))
        centres = centres / pixelSize
        radii = radii / pixelSize
        Box = numpy.zeros((boxSize, boxSize, boxSize), dtype="<f8")
        spam.kalisphera.makeSphere(Box, centres, radii)
        imLab = spam.label.watershed(Box > 0.5)

        # Run the function
        newLab = spam.label.shuffleLabels(imLab)

        # 1. Check that the two volumes are the same
        self.assertTrue(numpy.sum(imLab > 0) == numpy.sum(newLab > 0))
        # 2. Check that we have the same labels
        self.assertTrue(all(numpy.unique(imLab) == numpy.unique(newLab)))


if __name__ == "__main__":
    unittest.main()

import subprocess
import unittest

import numpy
import scipy.ndimage
import spam.datasets
import spam.deformation
import spam.DIC
import spam.filters
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile

numpy.set_printoptions(precision=3)

# load 3D image
im = spam.datasets.loadSnow()
im = im[:50, :50, :50]
imCentre = (numpy.array(im.shape) - 1) / 2.0

# input field
grid = numpy.mgrid[1:10:2, 1:5:2, 1:5:2]

fieldCoords = numpy.zeros((int(grid.shape[1] * grid.shape[2] * grid.shape[3]), 3))
fieldCoords[:, 0] = grid[0].ravel()
fieldCoords[:, 1] = grid[1].ravel()
fieldCoords[:, 2] = grid[2].ravel()

RS = numpy.full((fieldCoords.shape[0]), 2)
DPhi = numpy.full((fieldCoords.shape[0]), 0.0001)
IT = numpy.zeros_like(RS)
PSCC = numpy.zeros_like(DPhi)

PhiField = numpy.zeros((fieldCoords.shape[0], 4, 4))
for n in range(PhiField.shape[0]):
    PhiField[n] = numpy.eye(4)
    PhiField[n][0:3, -1] = [1, 1, 1]

for badPoint in range(1, RS.shape[0], 3):
    RS[badPoint] = -1
    PhiField[badPoint][0:3, -1] = [0, 0, 0]

RS[0] = RS[-1] = -5  # background points
TSVheader = "NodeNumber\tZpos\tYpos\tXpos\tF11\tF12\tF13\tZdisp\tF21\tF22\tF23\tYdisp\tF31\tF32\tF33\tXdisp\treturnStatus\tdeltaPhiNorm\titerations\tPSCC"
outMatrix = numpy.array(
    [
        numpy.array(range(PhiField.shape[0])),
        fieldCoords[:, 0],
        fieldCoords[:, 1],
        fieldCoords[:, 2],
        PhiField[:, 0, 0],
        PhiField[:, 0, 1],
        PhiField[:, 0, 2],
        PhiField[:, 0, 3],
        PhiField[:, 1, 0],
        PhiField[:, 1, 1],
        PhiField[:, 1, 2],
        PhiField[:, 1, 3],
        PhiField[:, 2, 0],
        PhiField[:, 2, 1],
        PhiField[:, 2, 2],
        PhiField[:, 2, 3],
        RS,
        DPhi,
        IT,
        PSCC,
    ]
).T
# numpy.savetxt("spamPhiFieldCF.tsv", outMatrix, fmt='%.7f', delimiter='\t', newline='\n', comments='', header=TSVheader)


class TestAll(spam.helpers.TestSpam):
    def test_applyPhi(self):
        a = numpy.random.rand(10, 10, 10)
        Phi = spam.deformation.computePhi({"t": [0.0, 3.0, 3.0]})
        aDefC = spam.DIC.applyPhi(a, Phi=Phi)
        aDefP = spam.DIC.applyPhiPython(a, Phi=Phi, interpolationOrder=1)
        self.assertAlmostEqual(a[5, 5, 5], aDefC[5, 8, 8], places=3)
        self.assertAlmostEqual(a[5, 5, 5], aDefP[5, 8, 8], places=3)

        # some silly tests, what if you don't pass phi?
        aDefC = spam.DIC.applyPhi(a)
        self.assertAlmostEqual(numpy.sum((a - aDefC)[1:-1, 1:-1, 1:-1]), 0, places=3)
        aDefC = spam.DIC.applyPhi(a, Phi=numpy.zeros((4, 4)))
        self.assertAlmostEqual(numpy.sum((a - aDefC)[1:-1, 1:-1, 1:-1]), 0, places=3)

        aDefP = spam.DIC.applyPhiPython(a)
        self.assertAlmostEqual(numpy.sum((a - aDefP)[1:-1, 1:-1, 1:-1]), 0, places=3)
        aDefP = spam.DIC.applyPhiPython(a, Phi=numpy.zeros((4, 4)))
        self.assertAlmostEqual(numpy.sum((a - aDefP)[1:-1, 1:-1, 1:-1]), 0, places=3)

        # Ask C for more than order 1, it should just return
        ret = spam.DIC.applyPhi(a, Phi=Phi, interpolationOrder=5)
        self.assertEqual(ret is None, True)

        # 2D test for C version, it's not implemented
        twoDtest = spam.DIC.applyPhi(a[5], Phi=Phi)
        self.assertEqual(twoDtest is None, True)
        # GP 2/13/2024: 2D test with Python
        twoDtest = spam.DIC.applyPhiPython(a[5], Phi=Phi)
        self.assertAlmostEqual(a[5, 5, 5], twoDtest [8, 8], places=3)
        self.assertAlmostEqual(a[5, 5, 5], twoDtest [8, 8], places=3)



    def test_applyPhiField(self):
        Phi = spam.deformation.computePhi({"r": [10, 0, 0], "t": [2, 1, -1]})
        # PhiInv = numpy.linalg.inv(Phi)

        snow = spam.datasets.loadSnow()[0:20, 0:20, 0:20]

        snowCentreRef = (numpy.array(snow.shape) - 1) / 2
        # snowCentreDef = (
        #     snowCentreRef
        #     + spam.deformation.decomposePhi(
        #         Phi, PhiCentre=snowCentreRef, PhiPoint=snowCentreRef
        #     )["t"]
        # )

        snowDefPhi = spam.DIC.applyPhi(snow, Phi=Phi)
        snowDefPhiMask = snowDefPhi > 8000
        # tifffile.imwrite("snowDefPhi.tif", snowDefPhi.astype('<f4'))

        nodePositionsRef, nodesDimRef = spam.DIC.makeGrid(snow.shape, 2)

        PhiField = spam.DIC.applyRegistrationToPoints(Phi, snowCentreRef, nodePositionsRef)

        # dispField = PhiField[:, 0:3, -1]
        # Finv = PhiInv[0:3, 0:3].copy()
        # traInv = PhiInv[0:3, 3].copy()

        snowDefDisp = spam.DIC.applyPhiField(
            snow,
            nodePositionsRef,
            PhiField,
            displacementMode="interpolate",
            verbose=True,
        )

        # GL = 32k in solid and 11k in void, insist on very low error (500/px)
        self.assertTrue((numpy.sum(numpy.abs(snowDefPhi[snowDefPhiMask] - snowDefDisp[snowDefPhiMask])) / numpy.sum(snowDefPhiMask)) < 500)

        snowDefApply = spam.DIC.applyPhiField(snow, nodePositionsRef, PhiField, displacementMode="applyPhi", verbose=True)

        # GL = 32k in solid and 11k in void, insist on very low error (500/px)
        self.assertTrue((numpy.sum(numpy.abs(snowDefPhi[snowDefPhiMask] - snowDefApply[snowDefPhiMask])) / numpy.sum(snowDefPhiMask)) < 500)

        # Create a DEFORMED mask, and only look up those voxels
        cylMask = spam.mesh.createCylindricalMask(snow.shape, radius=snow.shape[1] * 0.4)

        snowDefDispMasked = spam.DIC.applyPhiField(
            snow,
            nodePositionsRef,
            PhiField,
            imMaskDef=cylMask,
            displacementMode="interpolate",
            verbose=False,
        )
        # Make sure that in the mask voxels this is identical to its unmasked equivalent
        self.assertTrue(numpy.allclose(snowDefDispMasked[cylMask], snowDefDisp[cylMask]))

        snowDefApplyMasked = spam.DIC.applyPhiField(
            snow,
            nodePositionsRef,
            PhiField,
            imMaskDef=cylMask,
            displacementMode="applyPhi",
            verbose=False,
        )
        # Make sure that in the mask voxels this is identical to its unmasked equivalent
        self.assertTrue(numpy.allclose(snowDefApplyMasked[cylMask], snowDefApply[cylMask]))

    def test_register(self):
        # create desired transformation
        # we want a 2deg rotation around the centre
        transIn = {"t": [0, 0, 0], "r": [2.0, 0, 0]}
        # compute Phi
        # Phi = spam.deformation.computePhi(transIn, PhiCentre=imCentre, PhiPoint=imCentre) #should be applied with applyPhi(im, Phi=Phi, PhiPoint=imCentre)
        Phi = spam.deformation.computePhi(transIn)  # should be applied with applyPhi(im, Phi=Phi, PhiPoint=[0,0,0])

        # deform image with Phi
        # imDef = spam.DIC.applyPhi(im, Phi=Phi, PhiCentre=imCentre)
        imDef = spam.DIC.applyPhi(im, Phi=Phi)

        # run register (which returns Phi applied on the centre) -- adding verbose to test it too
        # CASE 1: im1.shape == im2.shape, let's check margin=0 at the same time
        returns1 = spam.DIC.register(
            imDef,
            im,
            margin=[3, 3, 3],
            interpolationOrder=1,
            maxIterations=20,
            verbose=True,
        )
        returns1["transformation"] = spam.deformation.decomposePhi(returns1["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns1["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns1["transformation"]["r"][i]),
                0,
                places=1,
            )
            self.assertEqual(returns1["returnStatus"], 2)

        returns1 = spam.DIC.register(
            imDef,
            im,
            margin=[3, 3, 3],
            interpolationOrder=1,
            maxIterations=20,
            interpolator="C",
            verbose=True,
        )
        returns1["transformation"] = spam.deformation.decomposePhi(returns1["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns1["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns1["transformation"]["r"][i]),
                0,
                places=1,
            )
            self.assertEqual(returns1["returnStatus"], 2)

        # CASE 1b: im1.shape == im2.shape with PhiInit, margin None
        PhiInit = Phi.copy()
        PhiInit[0:3, -1] = spam.deformation.decomposePhi(Phi.copy(), PhiPoint=imCentre)["t"]
        returns1b = spam.DIC.register(imDef, im, PhiInit=PhiInit, interpolationOrder=1, maxIterations=20)
        returns1b["transformation"] = spam.deformation.decomposePhi(returns1b["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns1b["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns1b["transformation"]["r"][i]),
                0,
                places=2,
            )
            self.assertEqual(returns1b["returnStatus"], 2)

        # CASE 1c: im1.shape == im2.shape hit maxIterations
        returns1c = spam.DIC.register(imDef, im, margin=0, interpolationOrder=1, maxIterations=2, verbose=True)
        self.assertEqual(returns1c["returnStatus"], 1)

        # 2018-07-10 No longer diverges
        # CASE 1d: im1.shape == im2.shape diverging on error condition
        # returns1d = spam.DIC.register(im[10:16,10:16,10:16], imDef[10:16,10:16,10:16], margin=1, maxIterations=6)
        # self.assertEqual(returns1d["returnStatus"], -1)

        # CASE 1e: im1.shape == im2.shape fail to invert M
        returns1e = spam.DIC.register(imDef[10:15, 10:15, 10:15], im[10:15, 10:15, 10:15], margin=2, verbose=True)
        self.assertEqual(returns1e["returnStatus"], -2)

        # 2018-07-10 No longer diverges
        # CASE 1f: im1.shape == im2.shape diverging on displacement condition
        # returns1f = spam.DIC.register(im[10:16,10:16,10:16], imDef[10:16,10:16,10:16], margin=1)
        # self.assertEqual(returns1f["returnStatus"], -3)

        # CASE 2: im1.shape < im2.shape
        with self.assertRaises(ValueError) as context:
            # returns2 = spam.DIC.register(im[5:45, 5:45, 5:45], imDef, margin=3, interpolationOrder=1)
            spam.DIC.register(imDef, im[5:45, 5:45, 5:45], margin=3, interpolationOrder=1)
        self.assertTrue("correlate.register():DimProblem" in str(context.exception))

        # CASE 3: im1.shape > im2.shape
        returns3 = spam.DIC.register(imDef[5:45, 5:45, 5:45], im, margin=3, interpolationOrder=1)
        returns3["transformation"] = spam.deformation.decomposePhi(returns3["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns3["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns3["transformation"]["r"][i]),
                0,
                places=2,
            )

        # CASE 4: 2D images
        returns4 = spam.DIC.register(
            imDef[int(imCentre[0])],
            im[int(imCentre[0])],
            margin=3,
            interpolationOrder=1,
            verbose=True,
        )
        returns4["transformation"] = spam.deformation.decomposePhi(returns4["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns4["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns4["transformation"]["r"][i]),
                0,
                places=2,
            )

        # tests for masks: commented for the moment
        # CASE 5: 3D images with masks
        maskDef = numpy.ones_like(imDef).astype(numpy.uint8)
        maskDef[0, :, :] = maskDef[-1, :, :] = 0
        returns5 = spam.DIC.register(imDef, im, margin=3, im1mask=maskDef, interpolationOrder=1)
        returns5["transformation"] = spam.deformation.decomposePhi(returns5["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns5["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns5["transformation"]["r"][i]),
                0,
                places=2,
            )

        # CASE 5b: 3D images with masks but Phi@image centre rather than mask COM
        maskDef5b = maskDef.copy()
        maskDef5b[0:10, 0:10, 0:10] = False
        returns5b = spam.DIC.register(imDef, im, margin=3, im1mask=maskDef5b, returnPhiMaskCentre=False, interpolationOrder=1)
        returns5b["transformation"] = spam.deformation.decomposePhi(returns5b["Phi"])
        for c in ["r", "t"]:
            for i in range(3):
                self.assertNotEqual(returns5["transformation"][c][i], returns5b["transformation"][c][i])

        # CASE 5c: ensure the mask is actually doing something
        # A carefully-crafted kalisphera example with a reversed mask, one case should converge, the other not
        A5c = spam.kalisphera.makeBlurryNoisySphere([50, 50, 50], [25, 25, 25], 10, noise=0.01, blur=1.0)
        B5c = spam.kalisphera.makeBlurryNoisySphere([50, 50, 50], [26, 26, 26], 10, noise=0.01, blur=1.0)
        returns5cFG = spam.DIC.register(A5c, B5c, im1mask=scipy.ndimage.binary_dilation(A5c > 0.5, iterations=4), maxIterations=50)
        returns5cBG = spam.DIC.register(A5c, B5c, im1mask=scipy.ndimage.binary_erosion(A5c < 0.5, iterations=4), maxIterations=50)
        self.assertTrue(returns5cFG["returnStatus"] == 2)
        self.assertTrue(returns5cBG["returnStatus"] < 2)

        # CASE 6: 2D images with masks
        maskDef = maskDef[int(imCentre[0]), :, :]
        maskDef[0, :] = maskDef[-1, :] = 0
        returns6 = spam.DIC.register(
            imDef[int(imCentre[0]), :, :],
            im[int(imCentre[0]), :, :],
            im1mask=maskDef,
            margin=3,
        )
        returns6["transformation"] = spam.deformation.decomposePhi(returns6["Phi"])
        for i in range(3):
            self.assertAlmostEqual(
                numpy.array(transIn["t"][i]) + numpy.array(returns6["transformation"]["t"][i]),
                0,
                places=2,
            )
            self.assertAlmostEqual(
                numpy.array(transIn["r"][i]) + numpy.array(returns6["transformation"]["r"][i]),
                0,
                places=2,
            )

        # CASE 7: 3D case with large initial rotation (say 120 deg)
        rot = 120
        Phi = spam.deformation.computePhi({"r": [rot, 0, 0]})
        PhiGuess = spam.deformation.computePhi({"r": [rot + 3, 0, 0]})
        imDef = spam.DIC.applyPhi(im, Phi=Phi)
        returns7 = spam.DIC.register(im, imDef, margin=10, PhiInit=PhiGuess)
        returns7["transformation"] = spam.deformation.decomposePhi(returns7["Phi"])
        # Did you converge? Pas donné...
        self.assertEqual(returns7["returnStatus"], 2)
        for i in range(3):
            self.assertAlmostEqual(numpy.array(returns7["transformation"]["t"][i]), 0, places=1)
            self.assertAlmostEqual(
                numpy.array(returns7["transformation"]["r"][i]) - [rot, 0, 0][i],
                0,
                places=1,
            )

        # CASE 7b: lasrge rot and only rigid
        returns7b = spam.DIC.register(im, imDef, margin=10, PhiInit=PhiGuess, PhiRigid=True)
        self.assertEqual(returns7b["returnStatus"], 2)
        returns7b["transformation"] = spam.deformation.decomposePhi(returns7b["Phi"])
        for i in range(3):
            self.assertAlmostEqual(numpy.array(returns7b["transformation"]["t"][i]), 0, places=1)
            self.assertAlmostEqual(
                numpy.array(returns7b["transformation"]["r"][i]) - [rot, 0, 0][i],
                0,
                places=1,
            )
        # Check that it's rigid!!
        self.assertAlmostEqual(
            numpy.linalg.norm(numpy.array(returns7b["transformation"]["U"]) - numpy.eye(3)),
            0,
            places=4,
        )

        # CASE 8: 3D case with large initial rotation (say 120 deg) with gradient update, should need fewer iterations
        rot = 120
        Phi = spam.deformation.computePhi({"r": [rot, 0, 0]})
        PhiGuess = spam.deformation.computePhi({"r": [rot + 3, 0, 0]})
        imDef = spam.DIC.applyPhi(im, Phi=Phi)
        returns8 = spam.DIC.register(im, imDef, margin=10, PhiInit=PhiGuess, verbose=True, updateGradient=True)
        returns8["transformation"] = spam.deformation.decomposePhi(returns8["Phi"])
        # Did you converge? Pas donné...
        self.assertEqual(returns8["returnStatus"], 2)
        for i in range(3):
            self.assertAlmostEqual(numpy.array(returns8["transformation"]["t"][i]), 0, places=1)
            self.assertAlmostEqual(
                numpy.array(returns8["transformation"]["r"][i]) - [rot, 0, 0][i],
                0,
                places=1,
            )

        # Test for a singular PhiInit
        PhiGuess = numpy.array([[1.0, 2, 3, 0], [2, 4, 6, 0], [3, 6, 9, 0]])
        returns8 = spam.DIC.register(im, imDef, margin=10, PhiInit=PhiGuess, verbose=True, updateGradient=True)
        self.assertEqual(returns8["returnStatus"], -4)

        # CASE 9: Same as Case 8 but with negative deltaPhiMin
        rot = 120
        Phi = spam.deformation.computePhi({"r": [rot, 0, 0]})
        PhiGuess = spam.deformation.computePhi({"r": [rot + 3, 0, 0]})
        imDef = spam.DIC.applyPhi(im, Phi=Phi)
        returns9 = spam.DIC.register(
            im,
            imDef,
            margin=10,
            PhiInit=PhiGuess,
            verbose=True,
            updateGradient=True,
            deltaPhiMin=-0.0001,
        )
        returns9["transformation"] = spam.deformation.decomposePhi(returns9["Phi"])
        # Did you converge? Pas donné...
        self.assertEqual(returns9["returnStatus"], 2)
        for i in range(3):
            self.assertAlmostEqual(numpy.array(returns9["transformation"]["t"][i]), 0, places=1)
            self.assertAlmostEqual(
                numpy.array(returns9["transformation"]["r"][i]) - [rot, 0, 0][i],
                0,
                places=1,
            )

    def test_registerMultiScale(self):
        t = {"t": [0, 0, 0], "r": [2.0, 0, 0]}
        Phi = spam.deformation.computePhi(t)
        imDef = spam.DIC.applyPhi(im, Phi=Phi)
        reg1 = spam.DIC.registerMultiscale(im, imDef, 2, deltaPhiMin=0.00001, margin=4)
        t1 = spam.deformation.decomposePhi(reg1["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t1[c][i] - t[c][i], 0, places=1)

        reg2 = spam.DIC.registerMultiscale(im, imDef, 1.1, PhiInit=numpy.eye(4), margin=8)
        t2 = spam.deformation.decomposePhi(reg2["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t2[c][i] - t[c][i], 0, places=1)

        # Adding 2D test
        # reg2d = spam.DIC.registerMultiscale(
        #     im[im.shape[0] // 2],
        #     imDef[im.shape[0] // 2],
        #     2,
        #     PhiInit=numpy.eye(4),
        #     margin=2,
        # )
        t2d = spam.deformation.decomposePhi(reg2["Phi"])
        for c in ["t", "r"]:
            for i in range(1, 3):
                self.assertAlmostEqual(t2d[c][i] - t[c][i], 0, places=1)

        # Check translation example with translation guess
        t = {"t": [4.0, 0, 0], "r": [0.0, 0, 0]}
        Phi = spam.deformation.computePhi(t)
        imDef = spam.DIC.applyPhi(im, Phi=Phi)
        reg3 = spam.DIC.registerMultiscale(im, imDef, 2, deltaPhiMin=0.00001, margin=10, PhiInit=Phi)
        t3 = spam.deformation.decomposePhi(reg3["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t3[c][i] - t[c][i], 0, places=1)

        reg4 = spam.DIC.registerMultiscale(
            im,
            imDef,
            2,
            margin=8,
            PhiInit=spam.deformation.computePhi({"t": [5, 0, 0]}),
            PhiInitBinRatio=2,
        )
        t4 = spam.deformation.decomposePhi(reg4["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t4[c][i] - t[c][i], 0, places=1)

        # check gradient update
        reg5 = spam.DIC.registerMultiscale(
            im,
            imDef,
            2,
            margin=8,
            PhiInit=spam.deformation.computePhi({"t": [5, 0, 0]}),
            PhiInitBinRatio=2,
            updateGradient=True,
        )
        t5 = spam.deformation.decomposePhi(reg5["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t5[c][i] - t[c][i], 0, places=1)

        # Test for margin = None
        reg6 = spam.DIC.registerMultiscale(
            im,
            imDef,
            2,
            margin=None,
            PhiInit=spam.deformation.computePhi({"t": [5, 0, 0]}),
            PhiInitBinRatio=2,
            updateGradient=True,
        )
        t6 = spam.deformation.decomposePhi(reg6["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t6[c][i] - t[c][i], 0, places=1)

        # Test for an im1 mask:
        im1mask = numpy.ones_like(im, dtype=bool)
        im1mask[0:2] = False
        im1mask[-2::] = False
        reg7 = spam.DIC.registerMultiscale(
            im,
            imDef,
            2,
            im1mask=im1mask,
            margin=8,
            PhiInit=spam.deformation.computePhi({"t": [5, 0, 0]}),
            PhiInitBinRatio=2,
        )
        t7 = spam.deformation.decomposePhi(reg7["Phi"])
        for c in ["t", "r"]:
            for i in range(3):
                self.assertAlmostEqual(t7[c][i] - t[c][i], 0, places=1)
        # Test 2D
        reg8 = spam.DIC.registerMultiscale(im[:,10,:], imDef[:,10,:], 2, deltaPhiMin=0.00001, margin=5)
        t8 = spam.deformation.decomposePhi(reg8["Phi"])
        for i in range(3):
            self.assertAlmostEqual(t8['r'][i] - t['r'][i], 0, places=1)
        # Since we are slicing along ZY, we can't compare directly t8 and t
        self.assertAlmostEqual(t8['t'][1] - t['t'][0], 0, places=1)
        self.assertAlmostEqual(t8['t'][2] - t['t'][1], 0, places=1)

    def test_pixelSearch(self):
        tVector = numpy.random.randint(1, 4, 3)
        # print("test_pixelSearch(): initial random displacement:", tVector)
        p = numpy.random.randint(15, 35, 3)
        imDef = spam.DIC.applyPhi(im, Phi=spam.deformation.computePhi({"t": tVector}))

        halfWindowSize = [5, 5, 5]
        # search for the translation of a random point
        subVolSliceRef = (
            slice(int(p[0] - halfWindowSize[0]), int(p[0] + halfWindowSize[0] + 1)),
            slice(int(p[1] - halfWindowSize[1]), int(p[1] + halfWindowSize[1] + 1)),
            slice(int(p[2] - halfWindowSize[2]), int(p[2] + halfWindowSize[2] + 1)),
        )

        topDisp = [-5, -5, -5]
        subVolSliceDef = (
            slice(
                int(p[0] - halfWindowSize[0] + topDisp[0]),
                int(p[0] + halfWindowSize[0] + 1 + 5),
            ),
            slice(
                int(p[1] - halfWindowSize[1] + topDisp[1]),
                int(p[1] + halfWindowSize[1] + 1 + 5),
            ),
            slice(
                int(p[2] - halfWindowSize[2] + topDisp[2]),
                int(p[2] + halfWindowSize[2] + 1 + 5),
            ),
        )

        # CASE 0: one image with itself:
        ps0 = spam.DIC.pixelSearch(im[subVolSliceRef].copy(), im[subVolSliceRef].copy(), returnError=1)
        self.assertTrue(ps0[1] > 0.9)
        self.assertAlmostEqual(ps0[2] , 0, places = 3)
        for i in range(3):
            self.assertAlmostEqual(0, ps0[0][i], places=4)

        # CASE 1: local coordinates
        ps1 = spam.DIC.pixelSearch(im[subVolSliceRef].copy(), imDef[subVolSliceDef].copy())
        # print(ps1)
        self.assertTrue(ps1[1] > 0.9)
        for i in range(3):
            self.assertAlmostEqual(tVector[i], ps1[0][i] + topDisp[0], places=4)

        # CASE 2: local coordinates with mask
        imagette1mask = numpy.ones_like(im[subVolSliceRef])
        imagette1mask[(0, -1), :] = 0
        imagette1mask[:, (0, -1)] = 0
        imagette2mask = numpy.ones_like(im[subVolSliceDef])
        imagette2mask[(0, -1), :] = 0
        imagette2mask[:, (0, -1)] = 0
        ps2 = spam.DIC.pixelSearch(
            im[subVolSliceRef].copy(),
            imDef[subVolSliceDef].copy(),
            imagette1mask=imagette1mask,
            imagette2mask=imagette2mask,
        )
        self.assertTrue(ps2[1] > 0.9)
        for i in range(3):
            self.assertAlmostEqual(tVector[i], ps2[0][i] + topDisp[0], places=4)

        # CASE XX: Catch assertion error when im1 is bigger than im2
        self.assertRaises(AssertionError, spam.DIC.pixelSearch, imDef, im[subVolSliceRef].copy())

    def test_makeGrid(self):
        # 3D image
        nodePositions, nodesDim = spam.DIC.makeGrid((50, 50, 50), 10)
        self.assertEqual((4, 4, 4), nodesDim)

        # 2D image
        nodePositions, nodesDim2D = spam.DIC.makeGrid((1, 50, 50), 10)
        self.assertEqual((1, 4, 4), nodesDim2D)

        im = spam.datasets.loadSnow()
        imageSize = im.shape

        # Make sure it fails with 2D image
        grid1 = spam.DIC.makeGrid(imageSize[1:], 10)
        self.assertTrue(grid1 is None)

        # Make sure it fails with wrong number of node spacing
        grid2 = spam.DIC.makeGrid(imageSize, [10, 10])
        self.assertTrue(grid2 is None)

        # Make sure it fails with wrong number of node spacing
        grid3 = spam.DIC.makeGrid(imageSize, [10, 10, 10, 10])
        self.assertTrue(grid3 is None)

    def test_getImagettes(self):
        # 3D image
        imagetteReturns1 = spam.DIC.getImagettes(im, imCentre, [5, 5, 5], numpy.eye(4), im, numpy.zeros(6), applyF="no")
        self.assertTrue(numpy.allclose(imagetteReturns1["imagette1"].shape, [11, 11, 11]))
        self.assertTrue(numpy.allclose(imagetteReturns1["imagette1"], imagetteReturns1["imagette2"]))

        # check masks + rigid Phi
        imagetteReturns2 = spam.DIC.getImagettes(
            im,
            imCentre,
            [5, 5, 5],
            numpy.eye(4),
            im,
            numpy.zeros(6),
            im1mask=numpy.ones_like(im),
            im2mask=numpy.ones_like(im),
            applyF="rigid",
        )
        self.assertTrue(numpy.allclose(imagetteReturns2["imagette1"].shape, [11, 11, 11]))
        self.assertTrue(numpy.allclose(imagetteReturns2["imagette1"], imagetteReturns2["imagette2"]))
        self.assertTrue(
            numpy.allclose(
                imagetteReturns2["imagette1mask"],
                numpy.ones_like(imagetteReturns2["imagette1"]),
            )
        )
        self.assertTrue(numpy.allclose(imagetteReturns2["imagette1mask"], imagetteReturns2["imagette2mask"]))

        # check apply F
        Phi = numpy.eye(4)
        Phi[0, 0] = 0.8
        imagetteReturns3 = spam.DIC.getImagettes(im, imCentre, [5, 5, 5], Phi, im, numpy.zeros(6), applyF="all")
        self.assertTrue(numpy.allclose(imagetteReturns3["imagette1"].shape, [11, 11, 11]))

        # check apply F with im1mask
        Phi = numpy.eye(4)
        Phi[0, 0] = 0.8
        imagetteReturns4 = spam.DIC.getImagettes(
            im,
            imCentre,
            [5, 5, 5],
            Phi,
            im,
            numpy.zeros(6),
            im1mask=numpy.ones_like(im),
            applyF="all",
        )
        self.assertTrue(numpy.allclose(imagetteReturns4["imagette1"].shape, [11, 11, 11]))
        self.assertTrue(numpy.allclose(imagetteReturns4["imagette1mask"].shape, [11, 11, 11]))

        # check apply F with im1mask in 2D
        Phi = numpy.eye(4)
        Phi[0, 0] = 0.8
        im2D = im[10]
        im2D = im2D[numpy.newaxis, ...]
        imagetteReturns5 = spam.DIC.getImagettes(
            im2D,
            [0, 25, 25],
            [0, 5, 5],
            Phi,
            im2D,
            numpy.zeros(6),
            im1mask=numpy.ones_like(im2D),
            applyF="all",
            twoD=True,
        )
        self.assertTrue(numpy.allclose(imagetteReturns5["imagette1"].shape, [1, 11, 11]))
        self.assertTrue(numpy.allclose(imagetteReturns5["imagette1mask"].shape, [1, 11, 11]))

        # fails glt
        imagetteReturns6 = spam.DIC.getImagettes(
            im,
            imCentre,
            [5, 5, 5],
            numpy.eye(4),
            im,
            numpy.zeros(6),
            applyF="no",
            greyThreshold=[numpy.inf, numpy.inf],
        )
        self.assertEqual(imagetteReturns6["returnStatus"], -5)
        self.assertTrue(imagetteReturns6["imagette1"] is None)

        # fails masks vol
        imagetteReturns7 = spam.DIC.getImagettes(
            im,
            imCentre,
            [5, 5, 5],
            numpy.eye(4),
            im,
            numpy.zeros(6),
            im1mask=numpy.zeros_like(im),
            im2mask=numpy.ones_like(im),
            minMaskCoverage=0.5,
        )
        self.assertEqual(imagetteReturns7["returnStatus"], -5)
        self.assertTrue(imagetteReturns7["imagette1"] is None)

    def test_merge(self):
        #######################################################
        # We're using the DDIC test from scripts here, lightly modified
        #######################################################
        # First we need to create some data using DEM dataset
        pixelSize = 0.0001
        blurSTD = 0.8
        noiseSTD = 0.01
        boxSizeDEM, centres, radii = spam.datasets.loadUniformDEMboxsizeCentreRadius()

        # put 0 in the middle
        centres -= numpy.mean(centres, axis=0)
        rMax = numpy.amax(radii)

        # pad box size
        boxSizeDEM = boxSizeDEM + 5 * rMax

        # add half box size to centres
        centres += numpy.array(boxSizeDEM) / 2.0
        boxSizePx = (boxSizeDEM / pixelSize).astype(int)
        centresPx = centres / pixelSize
        radiiPx = radii / pixelSize
        box = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(box, centresPx, radiiPx)
        box[numpy.where(box > 1.0)] = 1.0
        box[numpy.where(box < 0.0)] = 0.0
        box = box * 0.5
        box = box + 0.25
        box = scipy.ndimage.gaussian_filter(box, sigma=blurSTD)
        box = numpy.random.normal(box, scale=noiseSTD)
        binIm0 = box >= 0.5
        # Run watershed
        labIm0 = spam.label.ITKwatershed.watershed(binIm0)
        # Save images
        tifffile.imwrite("Step0.tif", box.astype("<f4"))
        tifffile.imwrite("Lab0.tif", labIm0.astype(spam.label.labelType))

        # test of rigid translation and rotation
        # Create Phi and Apply (25 px displacement on Y-axis, and 5 degree rotation along Z axis)
        translationStep1 = [5, 2, 0]
        rotationStep1 = [0, 0, 0]
        transformation = {"t": translationStep1, "r": rotationStep1}
        Phi = spam.deformation.computePhi(transformation)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]
        boxDeformed = numpy.zeros(boxSizePx, dtype="<f8")
        spam.kalisphera.makeSphere(boxDeformed, centresPxDeformed, radiiPx)
        boxDeformed[numpy.where(boxDeformed > 1.0)] = 1.0
        boxDeformed[numpy.where(boxDeformed < 0.0)] = 0.0
        boxDeformed = boxDeformed * 0.5
        boxDeformed = boxDeformed + 0.25
        boxDeformed = scipy.ndimage.gaussian_filter(boxDeformed, sigma=blurSTD)
        boxDeformed = numpy.random.normal(boxDeformed, scale=noiseSTD)
        # Save images
        tifffile.imwrite("Step1.tif", boxDeformed.astype("<f4"))

        #######################################################
        # Now use the ddic and ldic
        #######################################################
        exitCode = subprocess.call(["spam-reg", "Step0.tif", "Step1.tif", "-bb", "4", "-be", "2", "-od", "."])
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-glt",
                "0.5",
                "-hws",
                "10",
                "-ns",
                "10",
                "Step0.tif",
                "Step1.tif",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-ld",
                "2",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        # With lists
        spam.DIC.mergeRegularGridAndDiscrete(
            regularGrid=spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv"),
            discrete=[spam.helpers.readCorrelationTSV("Step0-Step1-ddic.tsv")],
            labelledImage=[tifffile.imread("Lab0.tif")],
            alwaysLabel=True,
        )

        # Non-working options, either one list and not the other, or different sizes, catch None returns
        r = spam.DIC.mergeRegularGridAndDiscrete(
            regularGrid=spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv"),
            discrete=[spam.helpers.readCorrelationTSV("Step0-Step1-ddic.tsv")],
            labelledImage=[tifffile.imread("Lab0.tif"), None],
            alwaysLabel=False,
        )
        self.assertEqual(r is None, True)

        r = spam.DIC.mergeRegularGridAndDiscrete(
            regularGrid=spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv"),
            discrete=[spam.helpers.readCorrelationTSV("Step0-Step1-ddic.tsv")],
            labelledImage=tifffile.imread("Lab0.tif"),
            alwaysLabel=False,
        )
        self.assertEqual(r is None, True)

        r = spam.DIC.mergeRegularGridAndDiscrete(
            regularGrid=spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv"),
            discrete=[spam.helpers.readCorrelationTSV("Step0-Step1-ddic.tsv")],
            labelledImage=None,
            alwaysLabel=False,
        )
        self.assertEqual(r is None, True)

        # This works again, no lists
        output = spam.DIC.mergeRegularGridAndDiscrete(
            regularGrid=spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv"),
            discrete=spam.helpers.readCorrelationTSV("Step0-Step1-ddic.tsv"),
            labelledImage=tifffile.imread("Lab0.tif"),
        )

        for i in range(3):
            self.assertAlmostEqual(
                numpy.mean(output["PhiField"][:, i, -1][output["returnStatus"] == 2]) - translationStep1[i],
                0,
                places=1,
            )

        # Check that there are more RS==2 points in the merged than in the ldic
        rsTwoLdic = numpy.sum(spam.helpers.readCorrelationTSV("Step0-Step1-ldic.tsv")["returnStatus"] == 2)
        self.assertEqual(numpy.sum(output["returnStatus"] == 2) > rsTwoLdic, True)

        # Check that there some mergeSource = 1
        # mergeSource = numpy.unique(numpy.genfromtxt('merged.tsv', names=True)['mergeSource'])
        # self.assertEqual(mergeSource[0], 0)
        # self.assertEqual(mergeSource[1], 1)

    def test_interpolatePhiField(self):
        # Fake Phi field -- this will be defined at 0,0,0 and +100 in all directions
        # z =   0 --> transformation below:
        transformation = {"t": [5.0, 0.0, 0.0], "r": [0.0, 0.0, 0.0]}
        Phi = spam.deformation.computePhi(transformation)
        # print(Phi)
        centre = [50, 50, 50]
        # z = 100 --> nothing

        fieldCoords = numpy.array(
            [
                [0, 0, 0],
                [0, 0, 100],
                [0, 100, 0],
                [0, 100, 100],
                [100, 0, 0],
                [100, 0, 100],
                [100, 100, 0],
                [100, 100, 100],
            ]
        )

        fieldValues = numpy.zeros([fieldCoords.shape[0], 4, 4])
        # fieldValues[0:4] = spam.deformation.computePhi(transformation)
        # fieldValues[4:8] = numpy.eye(4)
        for n in range(fieldCoords.shape[0]):
            fieldValues[n] = numpy.eye(4)
            fieldValues[n, 0:3, 0:3] = Phi[0:3, 0:3]
            fieldValues[n, 0:3, -1] = spam.deformation.decomposePhi(Phi, PhiPoint=fieldCoords[n], PhiCentre=centre)["t"]

        interpCoords = numpy.array([[50.0, 50.0, 50.0]])

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=1, verbose=True)
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=1, interpolateF="rigid")
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=1, interpolateF="no")
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], numpy.eye(3), atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=2)
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=2, interpolateF="rigid")
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, nNeighbours=2, interpolateF="no")
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], numpy.eye(3), atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, neighbourRadius=10)
        self.assertTrue(numpy.isnan(interpolatedPhi[0]).sum() == 16)

        interpolatedPhi = spam.DIC.interpolatePhiField(fieldCoords, fieldValues, interpCoords, neighbourRadius=100)
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], numpy.eye(3), atol=0.01))

        # Test all new interplation options
        interpolatedPhi = spam.DIC.interpolatePhiField(
            fieldCoords,
            fieldValues,
            interpCoords,
            nNeighbours=8,
            neighbourDistanceWeight="gaussian",
        )
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(
            fieldCoords,
            fieldValues,
            interpCoords,
            nNeighbours=8,
            neighbourDistanceWeight="mean",
        )
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

        interpolatedPhi = spam.DIC.interpolatePhiField(
            fieldCoords,
            fieldValues,
            interpCoords,
            nNeighbours=8,
            neighbourDistanceWeight="median",
        )
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, -1], transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(interpolatedPhi[0, 0:3, 0:3], Phi[0:3, 0:3], atol=0.01))

    def test_applyRegistrationToPoints(self):
        Phi = spam.deformation.computePhi(
            {
                "t": numpy.random.randint(1, 10, size=3),
                "r": numpy.random.randint(1, 10, size=3),
                "z": numpy.random.uniform(low=0.80, high=1.15, size=3),
            }
        )

        points, nodesDim = spam.DIC.makeGrid((100, 100, 100), 10)
        PhiCentre = [50, 50, 50]

        PhiField = spam.DIC.applyRegistrationToPoints(Phi, PhiCentre, points, verbose=True)
        self.assertTrue(numpy.allclose(numpy.mean(PhiField[:, 0:3, 0:3], axis=0), Phi[0:3, 0:3], atol=0.001))
        self.assertTrue(numpy.allclose(PhiField[points.shape[0] // 2], Phi, atol=0.001))
        PhiField = spam.DIC.applyRegistrationToPoints(Phi, PhiCentre, points, applyF="rigid", verbose=False)
        self.assertTrue(
            numpy.allclose(
                numpy.mean(PhiField[:, 0:3, 0:3], axis=0),
                spam.deformation.computeRigidPhi(Phi)[0:3, 0:3],
                atol=0.001,
            )
        )
        PhiField = spam.DIC.applyRegistrationToPoints(Phi, PhiCentre, points, applyF="no", verbose=False)
        self.assertTrue(numpy.allclose(numpy.mean(PhiField[:, 0:3, 0:3], axis=0), numpy.eye(3), atol=0.001))

    def test_LQC(self):
        points, _ = spam.DIC.grid.makeGrid((10, 10, 10), 2)
        disp = numpy.ones_like(points, dtype="<f4") * 5
        # all points should be coherent
        # LQC0 = spam.DIC.estimateLocalQuadraticCoherency(
        #     points, disp, neighbourRadius=1, verbose=True
        # )
        LQC1 = spam.DIC.estimateLocalQuadraticCoherency(points, disp, neighbourRadius=20, verbose = True)
        self.assertEqual(len(numpy.where(LQC1 > 0.1)[0]), 0)

        # set some bad points
        dispMod = disp.copy()
        dispMod[:4] = numpy.random.randint(-20, high=-2, size=[4, 3])
        dispMod[20:25] = numpy.random.randint(-20, high=-2, size=[5, 3])
        dispMod[-4:] = numpy.random.randint(-20, high=-2, size=[4, 3])

        # try with radius
        LQC2 = spam.DIC.estimateLocalQuadraticCoherency(points, dispMod, neighbourRadius=20)
        self.assertTrue(len(numpy.where(LQC2 > 0.1)[0]), 13)
        # try with number of neighbours
        LQC3 = spam.DIC.estimateLocalQuadraticCoherency(points, dispMod, nNeighbours=35)
        self.assertTrue(len(numpy.where(LQC3 > 0.1)[0]), 13)

        # correct disp with radius
        dispModFit2corrected = spam.DIC.estimateDisplacementFromQuadraticFit(
            points[LQC2 < 0.1],
            dispMod[LQC2 < 0.1],
            points[LQC2 >= 0.1],
            neighbourRadius=20,
            verbose=True,
        )
        dispModFit2 = dispMod.copy()
        dispModFit2[LQC2 >= 0.1] = dispModFit2corrected
        self.assertTrue(numpy.allclose(dispModFit2, disp))

        # correct disp with neighbours
        dispModFit3corrected = spam.DIC.estimateDisplacementFromQuadraticFit(points[LQC3 < 0.1], dispMod[LQC3 < 0.1], points[LQC3 >= 0.1], nNeighbours=35)
        dispModFit3 = dispMod.copy()
        dispModFit3[LQC3 >= 0.1] = dispModFit3corrected
        self.assertTrue(numpy.allclose(dispModFit3, disp))

    # Transform image with unstructured mesh
    def test_deformWithUnstructured(self):
        # size / margin
        s = 50
        m = 5

        # get mesh
        points, connectivity = spam.mesh.createCuboid([s - 2 * m, s - 2 * m, s - 2 * m], s / 5.0, origin=[m, m, m])

        # get image
        im = spam.datasets.loadSnow()
        im = im[:s, :s, :s]

        def checkDef(im, transf):
            print(f"Check apply deformation with: {transf}")
            # create phi
            Phi = spam.deformation.computePhi(transf)

            # centre
            centre = (numpy.array(im.shape) - 1) / 2.0
            # displacement field
            PhiField = spam.DIC.applyRegistrationToPoints(Phi, centre, points, applyF="no")
            disp = PhiField[:, 0:3, -1]

            # apply transformation based on displacement field
            imDef = spam.DIC.applyMeshTransformation(im, points, connectivity, disp)

            # apply transformation based on phi (registration)
            imDefPhi = spam.DIC.deform.applyPhi(im, Phi=Phi, PhiCentre=centre)

            # create mask / deform it with Phi / erode by 1 to avoid border measuring effects
            mask = numpy.zeros_like(im)
            mask[m + 1 : -(m + 1), m + 1 : -(m + 1), m + 1 : -(m + 1)] = 1
            # maskDef = spam.DIC.applyMeshTransformation(mask, points, connectivity, disp)
            maskDefPhi = spam.DIC.deform.applyPhi(mask, Phi=Phi, PhiCentre=centre)
            maskDefPhi = spam.filters.binaryErosion(maskDefPhi)

            diff2 = numpy.square(imDefPhi - imDef)[maskDefPhi == 1].mean()

            # tifffile.imwrite("applyMeshTransformation-test-phi.tiff", imDefPhi)
            # tifffile.imwrite("applyMeshTransformation-test-def.tiff", imDef)
            # tifffile.imwrite("applyMeshTransformation-test-diff.tiff", imDefPhi - imDef)
            # tifffile.imwrite("applyMeshTransformation-test-diff2.tiff", diff2)
            # tifffile.imwrite("applyMeshTransformation-test-maskDefPhi.tiff", maskDefPhi)

            self.assertLess(diff2, 1)

        checkDef(im, {"t": [0, 0, 0]})
        checkDef(im, {"t": [0, 10, 0]})
        checkDef(im, {"r": [30, 0, 0]})
        checkDef(im, {"z": [0.8, 1.5, 0.7]})
        checkDef(im, {"z": [0.8, 1.5, 0.7], "r": [10, 20, -5]})


#     def test_globalDVC(self):
#         import spam.label
#
#         transformations = [
#             # {'t': [0.0, 0.0, 0.0]},
#             {"t": [1.3, 0.0, 0.0]},
#             # {'t': [1.3, 2.1, -0.1]},
#             # {'r': [3, 0, 0]},
#             # {'z': [1.05, 1, 1], 'r': [1, 0, 0]},
#         ]
#         for transformation in transformations:
#             snowRef = spam.datasets.loadSnow()
#             Phi = spam.deformation.computePhi(transformation)
#             snowDef = spam.DIC.applyPhi(snowRef, Phi=Phi)
#
#             margin = [5, 10, 10]
#             meshCharacteristicLength = 15
#
#             # create mesh
#             points, connectivity = spam.mesh.createCuboid(
#                 [
#                     snowRef.shape[0] - 2 * margin[0],
#                     snowRef.shape[1] - 2 * margin[1],
#                     snowRef.shape[2] - 2 * margin[2],
#                 ],  # lengths
#                 meshCharacteristicLength,
#                 origin=margin,
#                 vtkFile="yo",
#             )
#
#             # initial guess?
#             # initialDisplacements = spam.DIC.applyRegistrationToPoints(
#             #     spam.deformation.computePhi({"r": [0.0, 0.0, 0.0]}),
#             #     (numpy.array(snowRef.shape) - 1) / 2.0,
#             #     points,
#             #     applyF="no",
#             # )[:, 0:3, -1]
#
#             spam.DIC.globalCorrelation(
#                 snowRef,
#                 snowDef,
#                 points,
#                 connectivity,
#                 regularisation={
#                     # "ksi_bulk": ksi_b,
#                     "few_times": 3,
#                     "young": 10000,
#                     "poisson": 0,
#                     "bc_dirichlet": [
#                         [0, 5, 100],  # [direction position ksi]
#                         # [0, 95],  # [direction position ksi]
#                         # [0, 95, 0],
#                         # [0, 95, 1],
#                         # [0, 95, 2],
#                     ],
#                 },
#                 convergenceCriterion=0.0001,
#                 debugFiles=True,
#                 maxIterations=20,
#                 # initialDisplacements=initialDisplacements,
#                 # medianFilterEachIteration=True,
#                 verbose=True,
#                 prefix="surfdreg",
#             )
#             # plt.imshow(returns)
#             # plt.show()
#
#             # self.assertAlmostEqual(numpy.mean(returns[:,0])-transformation['t'][0], 0, places=2)
#             # self.assertAlmostEqual(numpy.mean(returns[:,1])-transformation['t'][1], 0, places=2)
#             # self.assertAlmostEqual(numpy.mean(returns[:,2])-transformation['t'][2], 0, places=2)


if __name__ == "__main__":
    unittest.main()

# -*- coding: utf-8 -*-
import unittest
import numpy

import spam.measurements
import spam.datasets


class TestAll(spam.helpers.TestSpam):

    def test_alongAxis(self):
        im = spam.datasets.loadSnow()
        c = spam.measurements.covarianceAlongAxis(im, range(15))
        # c should be [[86833030. 74757580. 53643410. 35916468.]   # axis 0
        #              [86833030. 76282920. 56910720. 39792388.]   # axis 1
        #              [86833030. 76410500. 57040076. 39938920.]]  # axis 2
        self.assertAlmostEqual(numpy.sum(c[:, 3]) / c[0, 0], 1.3318408137585245, places=5)

        # fit
        spam.measurements.fitCovariance(range(15), c[0] / c[0, 0], functionType="gaussian")
        spam.measurements.fitCovariance(range(15), c[0] / c[0, 0], functionType="beta")

        # test all cases
        im = numpy.random.rand(4, 4, 4)
        spam.measurements.covarianceAlongAxis(im, 1)
        spam.measurements.covarianceAlongAxis(im, [0.1, 0.2])
        spam.measurements.covarianceAlongAxis(im, 5)
        spam.measurements.covarianceAlongAxis(im, 1, mask=numpy.random.rand(4, 4, 4) > 0.5)

    def test_subPixel(self):
        im = spam.datasets.loadSnow()
        d, c = spam.measurements.covarianceSubPixel(im, distance=2, step=2, normType="first")
        # d should be [0.        , 1.        , 1.41421354, 1.73205078]
        # c should be [1.        , 0.87239384, 0.7772405 , 0.70228433]
        self.assertAlmostEqual(d[3] - c[3], 1.0297664457003597, places=5)

        # test all cases
        im = numpy.random.rand(4, 4)
        spam.measurements.covarianceSubPixel(im, distance=2, normType="variance")


if __name__ == '__main__':
    unittest.main()

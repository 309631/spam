# -*- coding: utf-8 -*-

import unittest
import spam.datasets
import spam.helpers

class testAll(spam.helpers.TestSpam):

    def test_all(self):
        spam.datasets.loadtiff("idonotexit.tif")
        spam.datasets.loadtiff("snow/snow.tif")

        spam.datasets.load("idonotexit.txt")
        spam.datasets.load("placeholder.txt")

        spam.datasets.loadPickle("idonotexit.p")
        spam.datasets.loadPickle("mesh/structuredMesh.p")

        spam.datasets.loadSnow()
        spam.datasets.loadUnstructuredMesh()
        spam.datasets.loadStructuredMesh()
        spam.datasets.loadConcreteNe()
        spam.datasets.loadConcreteXr()
        spam.datasets.loadDEMspheresMMR()
        spam.datasets.loadDEMboxsizeCentreRadius()
        spam.datasets.loadUniformDEMboxsizeCentreRadius()
        spam.datasets.loadDEMtouchingGrainsAndBranchVector()


if __name__ == '__main__':
        unittest.main()

import unittest

import numpy
import spam.datasets
import spam.DIC


class TestAll(spam.helpers.TestSpam):
    def test_multimodalRegistration(self):
        # load images
        xr = spam.datasets.loadConcreteXr().astype("<f4")
        ne = spam.datasets.loadConcreteNe().astype("<f4")

        # rescale
        bins = 8
        xrMin = xr.min()
        xrMax = xr.max()
        neMax = ne.max()
        neMin = ne.min()
        xr = numpy.array(bins * (xr - xrMin) / (xrMax - xrMin)).astype("<u1")
        ne = numpy.array(bins * (ne - neMin) / (neMax - neMin)).astype("<u1")

        # get gaussian mixture parametersLogToGauss
        spam.DIC.gaussianMixtureParameters(xr, ne, BINS=bins, NPHASES=4, distanceMaxima=1.5)
        spam.DIC.gaussianMixtureParameters(xr, ne, BINS=bins, NPHASES=1, distanceMaxima=None, sliceAxis=2)
        gm, jh = spam.DIC.gaussianMixtureParameters(xr, ne, BINS=bins, NPHASES=1, sliceAxis=1)

        spam.DIC.phaseDiagram(gm, jh, BINS=bins)
        pd, co = spam.DIC.phaseDiagram(gm, jh, BINS=bins, voxelCoverage=0.70)

        spam.DIC.multimodalRegistration(xr, ne, pd, gm, verbose=True)


if __name__ == "__main__":
    unittest.main()

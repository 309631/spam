import unittest

import numpy
import scipy.spatial
import spam.DIC
import spam.mesh


class TestAll(spam.helpers.TestSpam):

    # test structured mesh

    def test_structuringElement(self):
        a = spam.mesh.structuringElement(radius=5, order=numpy.inf, margin=2)
        b = spam.mesh.structuringElement(radius=7, order=2, margin=0)
        c = spam.mesh.structuringElement(radius=8, order=1, margin=8)
        self.assertEqual(int(a.sum() + len(a) + b.sum() + len(b) + c.sum() + len(c)), 3646)

    def test_createLexicoCoordinates(self):
        c = spam.mesh.createLexicoCoordinates((2.5, 1.4, 8.2), (5, 10, 7), origin=(1.8, -2.5, 8.1))
        self.assertEqual(c.shape[0], 5 * 10 * 7)  # check total number of nodes
        self.assertAlmostEqual(c[0, 0], 0.0 + 1.8, places=5)  # x_0
        self.assertAlmostEqual(c[-1, 0], 2.5 + 1.8, places=5)  # x_n
        self.assertAlmostEqual(c[0, 1], 0.0 - 2.5, places=5)  # y_0
        self.assertAlmostEqual(c[-1, 1], 1.4 - 2.5, places=5)  # y_n
        self.assertAlmostEqual(c[0, 2], 0.0 + 8.1, places=5)  # z_0
        self.assertAlmostEqual(c[-1, 2], 8.2 + 8.1, places=5)  # z_n

    def test_createCylindricalMask(self):
        spam.mesh.createCylindricalMask((10, 10, 10), 3)

    # test unstructured mesh
    def test_unstructured(self):
        spam.mesh.createCuboid([1, 1.5, 2], 0.1, gmshFile="tmpCube", vtkFile="tmpCube", skipOutput=True)
        points, connectivity = spam.mesh.createCylinder([1, 1.5], 2, 10, 0.5, gmshFile="tmpCyl", vtkFile="tmpCyl")

        ###
        # Get characteristic length
        ###
        lc = spam.mesh.getMeshCharacteristicLength(points, connectivity)
        self.assertGreater(lc, 0.4)
        self.assertLess(lc, 0.7)

        ###
        # Check tetVolumes
        ###
        # Home made example around unit cube
        points = numpy.array([[0.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0], [1.0, 0.0, 0.0]])
        connectivity = numpy.array([[0, 1, 2, 3]])
        volumes = spam.mesh.tetVolumes(points, connectivity)
        self.assertAlmostEqual(volumes[0], 1 / 6.0, places=5)

        #######################################################
        # Check tet labellers
        #######################################################
        labelledTets = spam.label.labelTetrahedra([50, 50, 50], points * 50, connectivity)
        self.assertEqual(labelledTets[15, 15, 15], 0)
        self.assertEqual(labelledTets[45, 45, 45] != 0, True)

        self.assertEqual(numpy.unique(labelledTets).tolist(), [0, 2])

        Delaunay = scipy.spatial.Delaunay(points * 50)
        labelledTets = spam.label.labelTetrahedraForScipyDelaunay([50, 50, 50], Delaunay)
        self.assertEqual(labelledTets[15, 15, 15], 0)
        self.assertEqual(labelledTets[45, 45, 45] != 0, True)
        del points

        #######################################################
        # Check triangulation and strains
        #######################################################
        # New example with larger distance
        # pointsRef = numpy.array([[  0,   0,   0],
        # [100,   0,   0],
        # [  0, 100,   0],
        # [100, 100,   0],
        # [  0,   0, 100],
        # [100,   0, 100],
        # [  0, 100, 100],
        # [100, 100, 100]], dtype='<f4')

        #######################################################
        # Check connectivityFromVoronoi
        #######################################################
        # Check alpha functionality
        numpy.random.seed(1)
        pointsRef = numpy.random.randint(-20, 20, (20, 3)).astype("<f4")
        connectivity = spam.mesh.triangulate(pointsRef, alpha=-1)

        self.assertTrue(connectivity.shape[0] == 19)
        self.assertEqual(connectivity.shape[1], 4)

        # Check what happens with a nan in the positions
        nanPos = 5
        pointsRef[5] = numpy.array([numpy.nan, numpy.nan, numpy.nan])
        connectivity = spam.mesh.triangulate(pointsRef, alpha=-1)

        # There must be no elements connected to the bad one
        self.assertTrue(numpy.sum(connectivity.ravel() == nanPos) == 0)

        # test BC from DVC
        # create dvc field -- directly x,y,z
        grid = numpy.mgrid[0:2:0.1, 0:1.5:0.1, 0:1:0.1]
        fieldCoords = numpy.zeros((int(grid.shape[1] * grid.shape[2] * grid.shape[3]), 3))
        fieldCoords[:, 0] = grid[0].ravel()  # z
        fieldCoords[:, 1] = grid[1].ravel()  # y
        fieldCoords[:, 2] = grid[2].ravel()  # x

        fieldDisp = numpy.zeros((fieldCoords.shape[0], 3))
        fieldDisp[:, 0] = numpy.random.uniform(1, 100)  # disp z
        fieldDisp[:, 1] = numpy.random.uniform(1, 100)  # disp y
        fieldDisp[:, 2] = numpy.random.uniform(1, 100)  # disp x
        dvcField = numpy.hstack((fieldCoords, fieldDisp))

        # read mesh files
        import meshio

        # case 1 cube
        gmsh = meshio.read("tmpCube.msh")
        meshNodes = numpy.zeros((gmsh.points.shape[0], 4))
        for i, node in enumerate(gmsh.points):
            meshNodes[i] = [i + 1, node[2], node[1], node[0]]  # zyx

        bc1 = spam.mesh.BCFieldFromDVCField(meshNodes, dvcField)
        self.assertAlmostEqual(bc1[:, 4].mean(), fieldDisp[:, 0].mean(), places=3)
        self.assertAlmostEqual(bc1[:, 5].mean(), fieldDisp[:, 1].mean(), places=3)
        self.assertAlmostEqual(bc1[:, 6].mean(), fieldDisp[:, 2].mean(), places=3)

        # case 1b cube only top-bottom surfaces
        bc1b = spam.mesh.BCFieldFromDVCField(meshNodes, dvcField, topBottom=True)
        self.assertAlmostEqual(bc1b[:, 4].mean(), fieldDisp[:, 0].mean(), places=3)
        self.assertAlmostEqual(bc1b[:, 5].mean(), fieldDisp[:, 1].mean(), places=3)
        self.assertAlmostEqual(bc1b[:, 6].mean(), fieldDisp[:, 2].mean(), places=3)

        # case 2 cylinder
        gmsh2 = meshio.read("tmpCyl.msh")
        meshNodes2 = numpy.zeros((gmsh2.points.shape[0], 4))
        for i, node in enumerate(gmsh2.points):
            meshNodes2[i] = [i + 1, node[2], node[1], node[0]]  # zyx
        bc2 = spam.mesh.BCFieldFromDVCField(meshNodes2, dvcField, meshType="cylinder", centre=[1, 1.5], radius=2)
        self.assertAlmostEqual(bc2[:, 4].mean(), fieldDisp[:, 0].mean(), places=3)
        self.assertAlmostEqual(bc2[:, 5].mean(), fieldDisp[:, 1].mean(), places=3)
        self.assertAlmostEqual(bc2[:, 6].mean(), fieldDisp[:, 2].mean(), places=3)

        # case 2b cylinder only top-bottom surfaces
        bc2b = spam.mesh.BCFieldFromDVCField(
            meshNodes2,
            dvcField,
            meshType="cylinder",
            centre=[1, 1.5],
            radius=2,
            topBottom=True,
        )
        self.assertAlmostEqual(bc2b[:, 4].mean(), fieldDisp[:, 0].mean(), places=3)
        self.assertAlmostEqual(bc2b[:, 5].mean(), fieldDisp[:, 1].mean(), places=3)
        self.assertAlmostEqual(bc2b[:, 6].mean(), fieldDisp[:, 2].mean(), places=3)

        # case 3 mask correlation points
        fieldMask = numpy.ones(fieldCoords.shape[0])
        fieldMask[0] = fieldMask[-1] = 0
        bc3 = spam.mesh.BCFieldFromDVCField(
            meshNodes2,
            dvcField,
            meshType="cylinder",
            centre=[1, 1.5],
            radius=2,
            mask=fieldMask,
        )
        self.assertAlmostEqual(bc3[:, 4].mean(), fieldDisp[:, 0].mean(), places=3)
        self.assertAlmostEqual(bc3[:, 5].mean(), fieldDisp[:, 1].mean(), places=3)
        self.assertAlmostEqual(bc3[:, 6].mean(), fieldDisp[:, 2].mean(), places=3)

    def test_rankPoints(self):
        points, _ = spam.DIC.grid.makeGrid([1, 15, 15], 2)
        points = numpy.vstack(([0, 0, 0], points))
        rankedPoints, rowNumbers = spam.mesh.rankPoints(points, verbose=True)
        self.assertTrue(numpy.allclose(rowNumbers, numpy.arange(points.shape[0])))
        self.assertTrue(numpy.allclose(points, rankedPoints))

    def test_periodicity(self):
        lengths = [0.5, 1, 0.8]
        origin = [0.1, -2, 1.7]
        lc = 0.05

        # create mesh with periodic boundary conditions
        points, connectivity = spam.mesh.createCuboid(lengths, lc, origin=origin, periodicity=True, vtkFile="yolo", verbosity=1)

        # check that the boundary conditions are periodic
        # build key for linear node lookup tolerency
        def coord_to_key(coords):
            return f'{" ".join([f"{c:.4f}" for c in coords])}'

        # count coordinates
        # key: serialized coordinates
        # value: list of node number
        count = {}
        for node_number, node_coordinates in enumerate(points):
            key = coord_to_key(node_coordinates)
            if key in count:
                count[key].append(node_number + 1)
            else:
                count[key] = [node_number + 1]

        # insure that there are no double nodes per coordinates
        for node_coordinates, nodes_list in count.items():
            if len(nodes_list) > 1:
                raise BaseException(f"Double node coordinates {node_coordinates}: {nodes_list}")

        for SPATIAL_COORDINATE in [0, 1, 2]:
            TOLERENCY = 1e-6
            TRANSLATION = lengths[SPATIAL_COORDINATE]

            # grab face_x0 and face_x1
            face_x0 = {}
            face_x1 = {}
            for node_number, node_coordinates in enumerate(points):
                if SPATIAL_COORDINATE == 0:
                    key = coord_to_key(node_coordinates[1:])
                elif SPATIAL_COORDINATE == 1:
                    key = coord_to_key(node_coordinates[::2])
                elif SPATIAL_COORDINATE == 2:
                    key = coord_to_key(node_coordinates[:-1])
                else:
                    raise BaseException("SPATIAL_COORDINATE must be either 0, 1, or 2")

                if abs(node_coordinates[SPATIAL_COORDINATE] - origin[SPATIAL_COORDINATE]) < TOLERENCY:
                    face_x0[key] = node_number + 1

                elif abs(node_coordinates[SPATIAL_COORDINATE] - origin[SPATIAL_COORDINATE] - TRANSLATION) < TOLERENCY:
                    face_x1[key] = node_number + 1

            # get face x=0 nodes
            x = {0: "x", 1: "y", 2: "z"}[SPATIAL_COORDINATE]
            yz = {0: "yz", 1: "xz", 2: "xy"}[SPATIAL_COORDINATE]
            for i, (node_coordinates, node_x0) in enumerate(face_x0.items()):
                if node_coordinates in face_x1:
                    # node_x1 = face_x1[node_coordinates]
                    # c_x0 = points[node_x0 - 1]
                    # c_x1 = points[node_x1 - 1]
                    pass
                else:
                    print(f"{i + 1:03d} Coordinates {yz}={node_coordinates}: face {x}=0 node {node_x0:03d} ({coord_to_key(points[node_x0 - 1])}) -> NOT FOUND")

                self.assertTrue(node_coordinates in face_x1)

    def test_tetrahedra(self):

        # test shape functions
        points = [[0.1, 0.2, 0.3], [-0.1, 1.1, -0.2], [0.4, 0.5, 0.2], [0.9, -0.5, 0.1]]
        v, c = spam.mesh.shapeFunctions(points)
        self.assertAlmostEqual(v, 0.038833, 6)
        tc = numpy.array(
            [
                [1.155, -1.974, -1.073, 0.858],
                [0.652, -0.558, -0.086, -1.931],
                [-1.352, 2.275, 1.888, 2.489],
                [0.545, 0.258, -0.73, -1.416],
            ]
        )
        self.assertTrue(numpy.allclose(c, tc, atol=0.001))

        # test elementary stiffness matrix
        young = 10
        poisson = 0.3
        ke = spam.mesh.elementaryStiffnessMatrix(points, young, poisson)
        tke = numpy.array(
            [
                [
                    2.32,
                    0.791,
                    -0.633,
                    0.342,
                    0.127,
                    0.783,
                    -2.331,
                    -1.2,
                    -0.809,
                    -0.33,
                    0.281,
                    0.659,
                ],
                [
                    0.791,
                    1.294,
                    -0.344,
                    0.159,
                    -0.035,
                    0.453,
                    -1.104,
                    -1.411,
                    -0.356,
                    0.153,
                    0.152,
                    0.247,
                ],
                [
                    -0.633,
                    -0.344,
                    1.139,
                    0.462,
                    0.293,
                    -0.688,
                    -0.297,
                    -0.036,
                    0.144,
                    0.467,
                    0.087,
                    -0.595,
                ],
                [
                    0.342,
                    0.159,
                    0.462,
                    0.721,
                    0.018,
                    0.402,
                    -1.406,
                    -0.265,
                    -0.967,
                    0.343,
                    0.088,
                    0.103,
                ],
                [
                    0.127,
                    -0.035,
                    0.293,
                    0.018,
                    0.607,
                    0.062,
                    -0.201,
                    -0.992,
                    -0.593,
                    0.056,
                    0.42,
                    0.238,
                ],
                [
                    0.783,
                    0.453,
                    -0.688,
                    0.402,
                    0.062,
                    1.997,
                    -1.192,
                    -0.849,
                    -2.727,
                    0.007,
                    0.334,
                    1.418,
                ],
                [
                    -2.331,
                    -1.104,
                    -0.297,
                    -1.406,
                    -0.201,
                    -1.192,
                    4.163,
                    1.604,
                    2.114,
                    -0.426,
                    -0.299,
                    -0.626,
                ],
                [
                    -1.2,
                    -1.411,
                    -0.036,
                    -0.265,
                    -0.992,
                    -0.849,
                    1.604,
                    3.563,
                    1.755,
                    -0.139,
                    -1.159,
                    -0.87,
                ],
                [
                    -0.809,
                    -0.356,
                    0.144,
                    -0.967,
                    -0.593,
                    -2.727,
                    2.114,
                    1.755,
                    4.545,
                    -0.338,
                    -0.806,
                    -1.961,
                ],
                [
                    -0.33,
                    0.153,
                    0.467,
                    0.343,
                    0.056,
                    0.007,
                    -0.426,
                    -0.139,
                    -0.338,
                    0.414,
                    -0.07,
                    -0.136,
                ],
                [
                    0.281,
                    0.152,
                    0.087,
                    0.088,
                    0.42,
                    0.334,
                    -0.299,
                    -1.159,
                    -0.806,
                    -0.07,
                    0.588,
                    0.386,
                ],
                [
                    0.659,
                    0.247,
                    -0.595,
                    0.103,
                    0.238,
                    1.418,
                    -0.626,
                    -0.87,
                    -1.961,
                    -0.136,
                    0.386,
                    1.138,
                ],
            ]
        )
        self.assertTrue(numpy.allclose(ke, tke, atol=0.001))

        # test global stiffness matrix (and symmetry)
        points = numpy.array(
            [
                [0.1, 0.2, 0.3],
                [-0.1, 0.6, -0.2],
                [0.4, 0.5, 0.2],
                [0.9, -0.5, 0.1],
                [0.6, 0.6, 0],
            ]
        )
        connectivity = numpy.array([[0, 1, 2, 3], [1, 2, 3, 4]])

        K = spam.mesh.globalStiffnessMatrix(points, connectivity, young, poisson)
        tK = numpy.transpose(
            [
                [
                    2.2511079455523904,
                    0.6759786852379445,
                    -1.2167616334283002,
                    -0.028885723330168003,
                    0.11818085892159957,
                    0.5757359924026594,
                    -1.704257676479899,
                    -0.9974147937110902,
                    0.2184235517568849,
                    -0.5179645457423236,
                    0.20325524955154595,
                    0.42260208926875603,
                    0.0,
                    0.0,
                    0.0,
                ],
                [
                    0.6759786852379445,
                    1.554685026907249,
                    -0.7419278252611586,
                    0.15023214097288165,
                    -0.347420069642292,
                    0.42141500474833815,
                    -0.901260947557244,
                    -1.2670148781259893,
                    0.34425451092117754,
                    0.07505012134641761,
                    0.05974992086103194,
                    -0.02374169040835704,
                    0.0,
                    0.0,
                    0.0,
                ],
                [
                    -1.2167616334283002,
                    -0.7419278252611586,
                    2.4779729872322465,
                    0.2552231718898387,
                    0.26115859449192785,
                    -1.7158647251239845,
                    0.5709876543209879,
                    0.6647673314339985,
                    -0.007386303682599861,
                    0.3905508072174739,
                    -0.18399810066476746,
                    -0.754721958425662,
                    0.0,
                    0.0,
                    0.0,
                ],
                [
                    -0.028885723330168003,
                    0.15023214097288165,
                    0.2552231718898387,
                    1.4177203799313212,
                    0.15381214149779665,
                    0.8128817350475555,
                    -0.8558316983302876,
                    -0.20200455926156238,
                    -1.2133148702522645,
                    0.14658183933423638,
                    0.26093697860007314,
                    0.017492925699355448,
                    -0.6795847976051025,
                    -0.36297670180918906,
                    0.12771703761551495,
                ],
                [
                    0.11818085892159957,
                    -0.347420069642292,
                    0.26115859449192785,
                    0.15381214149779665,
                    1.0022207117525783,
                    0.22205108922536998,
                    -0.10585071310771613,
                    -0.9275488773514713,
                    -0.8453608495909679,
                    0.16478313244622694,
                    0.4146198999554893,
                    0.3898096326184311,
                    -0.33092541975790707,
                    -0.14187166471430446,
                    -0.02765846674476098,
                ],
                [
                    0.5757359924026594,
                    0.42141500474833815,
                    -1.7158647251239845,
                    0.8128817350475555,
                    0.22205108922536998,
                    2.7914175329102946,
                    -1.0851097420471363,
                    -1.0056172598473785,
                    -2.9631511021339927,
                    -0.11071220250577284,
                    0.5500660428748415,
                    0.9516032403323247,
                    -0.19279578289730584,
                    -0.18791487700117138,
                    0.9359950540153584,
                ],
                [
                    -1.704257676479899,
                    -0.901260947557244,
                    0.5709876543209879,
                    -0.8558316983302876,
                    -0.10585071310771613,
                    -1.0851097420471363,
                    4.784141434592646,
                    1.229861074286718,
                    -0.8675960875622467,
                    -0.07742964666258503,
                    0.0323592678828625,
                    -0.1287595027019732,
                    -2.146622413119875,
                    -0.2551086815046206,
                    1.5104776779903684,
                ],
                [
                    -0.9974147937110902,
                    -1.2670148781259893,
                    0.6647673314339985,
                    -0.20200455926156238,
                    -0.9275488773514713,
                    -1.0056172598473785,
                    1.229861074286718,
                    4.44860426177403,
                    0.5849554813175795,
                    0.12851311403670881,
                    -0.861518054129447,
                    -0.8485243997088328,
                    -0.15895483535077437,
                    -1.3925224521671218,
                    0.6044188468046333,
                ],
                [
                    0.2184235517568849,
                    0.34425451092117754,
                    -0.007386303682599861,
                    -1.2133148702522645,
                    -0.8453608495909679,
                    -2.9631511021339927,
                    -0.8675960875622467,
                    0.5849554813175795,
                    8.20783914998053,
                    -0.0005543744968448949,
                    -1.008780809965243,
                    -1.0057190290786828,
                    1.8630417805544712,
                    0.924931667317454,
                    -4.231582715085253,
                ],
                [
                    -0.5179645457423236,
                    0.07505012134641761,
                    0.3905508072174739,
                    0.14658183933423638,
                    0.16478313244622694,
                    -0.11071220250577284,
                    -0.07742964666258503,
                    0.12851311403670881,
                    -0.0005543744968448949,
                    0.3993525066565111,
                    -0.15261032722021628,
                    -0.14196807767027733,
                    0.0494598464141611,
                    -0.21573604060913704,
                    -0.13731615254457888,
                ],
                [
                    0.20325524955154595,
                    0.05974992086103194,
                    -0.18399810066476746,
                    0.26093697860007314,
                    0.4146198999554893,
                    0.5500660428748415,
                    0.0323592678828625,
                    -0.861518054129447,
                    -1.008780809965243,
                    -0.15261032722021628,
                    0.7704620430226747,
                    0.3043033922898558,
                    -0.34394116881426534,
                    -0.3833138097097489,
                    0.33840947546531314,
                ],
                [
                    0.42260208926875603,
                    -0.02374169040835704,
                    -0.754721958425662,
                    0.017492925699355448,
                    0.3898096326184311,
                    0.9516032403323247,
                    -0.1287595027019732,
                    -0.8485243997088328,
                    -1.0057190290786828,
                    -0.14196807767027733,
                    0.3043033922898558,
                    0.5698360551246431,
                    -0.16936743459586096,
                    0.17815306520890273,
                    0.23900169204737723,
                ],
                [
                    0.0,
                    0.0,
                    0.0,
                    -0.6795847976051025,
                    -0.33092541975790707,
                    -0.19279578289730584,
                    -2.146622413119875,
                    -0.15895483535077437,
                    1.8630417805544712,
                    0.0494598464141611,
                    -0.34394116881426534,
                    -0.16936743459586096,
                    2.7767473643108165,
                    0.8338214239229468,
                    -1.5008785630613044,
                ],
                [
                    0.0,
                    0.0,
                    0.0,
                    -0.36297670180918906,
                    -0.14187166471430446,
                    -0.18791487700117138,
                    -0.2551086815046206,
                    -1.3925224521671218,
                    0.924931667317454,
                    -0.21573604060913704,
                    -0.3833138097097489,
                    0.17815306520890273,
                    0.8338214239229468,
                    1.9177079265911752,
                    -0.9151698555251855,
                ],
                [
                    0.0,
                    0.0,
                    0.0,
                    0.12771703761551495,
                    -0.02765846674476098,
                    0.9359950540153584,
                    1.5104776779903684,
                    0.6044188468046333,
                    -4.231582715085253,
                    -0.13731615254457888,
                    0.33840947546531314,
                    0.23900169204737723,
                    -1.5008785630613044,
                    -0.9151698555251855,
                    3.056585969022517,
                ],
            ]
        )
        self.assertTrue(numpy.allclose(K, tK, atol=0.000001))


if __name__ == "__main__":
    unittest.main()

# -*- coding: utf-8 -*-
"""
Expectation of global descriptors in 3D
=======================================

This example shows how to compute theoretical expectations of
total volume (L3), surface area (L2), mean curvature measure (L1) and Euler characteristic (L0)
of excursion sets as a function of the excursion's threshold.
Excrusions are define here as the subdomain of the domain where
the Random Field is defined where the Random Field values are *above*
a certain threshold.

Here the two measures are seen as function of the threshold value.

Monte Carlo results are confronted to the theory.
"""

# sphinx_gallery_thumbnail_number = 12
import spam.excursions
import spam.measurements
import matplotlib.pyplot as plt
import numpy

#####################################
# Correlated Random Field parameters
#####################################
# First we set all the correlated Random Fields parameters.
# It is assumed that the distribution is Gaussian with zero mean
# and the covariance function is Gaussian too.

# set the variance
variance = 2.0
# standard deviation
std = numpy.sqrt(variance)
# correlation length
correlationLength = 0.5
# length of the domain
length = 10.0

# set the thresholds between -5 and 5
thresholds = numpy.linspace(-5, 5, 200)
thresholdsMC = numpy.linspace(-4, 4, 20)  # less for monte carlo

##################################################
# Compute the four theoretical expected measures
##################################################
# The four measures of the excursion are computed and ploted for every thresholds

# spatial dimension
spatialDimension = 3
# the measure number 3
totalVolume = spam.excursions.expectedMesures(thresholds, 3, spatialDimension, std=std, lc=correlationLength, a=length)
# the measure number 2
totalSurface = 2.0 * spam.excursions.expectedMesures(thresholds, 2, spatialDimension, std=std, lc=correlationLength, a=length)
# the measure number 1
caliperDiameter = 0.5 * spam.excursions.expectedMesures(thresholds, 1, spatialDimension, std=std, lc=correlationLength, a=length)
# the measure number 0
eulerCharac = spam.excursions.expectedMesures(thresholds, 0, spatialDimension, std=std, lc=correlationLength, a=length)

plt.figure()
plt.xlabel("Threshold")
plt.title("Total volume")
plt.plot(thresholds, totalVolume, 'r')

plt.figure()
plt.xlabel("Threshold")
plt.title("Total surface area")
plt.plot(thresholds, totalSurface, 'r')

plt.figure()
plt.xlabel("Threshold")
plt.title("Mean caliper diametre")
plt.plot(thresholds, caliperDiameter, 'r')

plt.figure()
plt.xlabel("Threshold")
plt.title("Euler characteristic")
plt.plot(thresholds, eulerCharac, 'r')


#############################################################
# Generate 5 realisations of the correlated Random Field
#############################################################
# In order to compare the theoretical values to Monte Carlo results,
# first 5 realisations of a correlated Random Field are generated.

# number of realisations
nRea = 5
nNodes = 100

# define the covariance
covarianceParameters = {'var': variance, 'len_scale': correlationLength}

# generate realisations
realisations = spam.excursions.simulateRandomField(
    lengths=length,
    nNodes=nNodes,
    covarianceParameters=covarianceParameters,
    dim=spatialDimension,
    nRea=nRea)

#############################################################
# We now show, on the left, a slice a the first realisation and on the right
# the corresponding excrusion for 3 thresholds values: -2, 0 and 2 which corresponds
# to three different topologies.

plt.figure()
plt.title("Middle slice of the first realisation")
plt.imshow(realisations[0][int(nNodes / 2), :, :])

plt.figure()
plt.title("Middle slice of the excursion with a threshold of -2")
plt.imshow(realisations[0][int(nNodes / 2), :, :] > -2)

plt.figure()
plt.title("Middle slice of the excursion with a threshold of 0")
plt.imshow(realisations[0][int(nNodes / 2), :, :] > 0)

plt.figure()
plt.title("Middle slice of the excursion with a threshold of 2")
plt.imshow(realisations[0][int(nNodes / 2), :, :] > 2)

##########################################################
# Compute the four averaged measures (actually three)
##########################################################
# For every thresholds, three average measures (L0, L2 and L3) over all the realisations
# are compute and compared to the theoretical values.

# save average for every thresholds
totalVolumeMC = numpy.zeros_like(thresholdsMC)
totalSurfaceMC = numpy.zeros_like(thresholdsMC)
caliperDiameterMC = numpy.zeros_like(thresholdsMC)
eulerCharacMC = numpy.zeros_like(thresholdsMC)

# compute the aspect ratio
ar = length / float(nNodes)

# loop over the thresholds
for i, t in enumerate(thresholdsMC):
    print(f"Threshold: {i + 1}/{len(thresholdsMC)}: {t:.2f}")
    # loop over the realisations
    for r in realisations:
        totalVolumeMC[i] += spam.measurements.volume((r > t), aspectRatio=(ar, ar, ar)) / float(nRea)
        totalSurfaceMC[i] += spam.measurements.surfaceArea(r, level=t, aspectRatio=(ar, ar, ar)) / float(nRea)
        # commented because requires too much ressources
        # caliperDiameterMC[i] += spam.measurements.totalCurvature(r, level=t, aspectRatio=(ar, ar, ar))/(float(nRea)*2.0*numpy.pi)
        eulerCharacMC[i] += spam.measurements.eulerCharacteristic((r > t)) / float(nRea)
    print(f"\t volume  = {totalVolumeMC[i]:.2f} \t (err = {abs(totalVolume[i] - totalVolumeMC[i]) / abs(totalVolume[i]):.2f})")
    print(f"\t surface = {totalSurfaceMC[i]:.2f} \t (err = {abs(totalSurface[i] - totalSurfaceMC[i]) / abs(totalSurface[i]):.2f})")
    print(f"\t caliper = {caliperDiameterMC[i]:.2f} \t (err = {abs(caliperDiameter[i] - caliperDiameterMC[i]) / abs(caliperDiameter[i]):.2f})")
    print(f"\t euler   = {eulerCharacMC[i]:.2f} \t (err = {abs(eulerCharac[i] - eulerCharacMC[i]) / abs(eulerCharac[i]):.2f})")


##########################################################
# We can now plot the theory and Monte Carlo measures over the thresholds

# plot volume
plt.figure()
plt.xlabel("Threshold")
plt.title("Total volume")
plt.plot(thresholds, totalVolume, 'r', label='Theory')
plt.plot(thresholdsMC, totalVolumeMC, '*b', label='Monte Carlo')
plt.legend()

# plot surface
plt.figure()
plt.xlabel("Threshold")
plt.title("Total surface area")
plt.plot(thresholds, totalSurface, 'r', label='Theory')
plt.plot(thresholdsMC, totalSurfaceMC, '*b', label='Monte Carlo')
plt.legend()

# plot caliper diametre
plt.figure()
plt.plot(thresholds, caliperDiameter, 'r')
plt.plot(thresholdsMC, caliperDiameterMC, '*b', label='Monte Carlo')
plt.xlabel("Threshold")
plt.title("Mean caliper diametre")
plt.legend()

# plot Euler characteristic
plt.figure()
plt.xlabel("Threshold")
plt.title("Euler characteristic")
plt.plot(thresholds, eulerCharac, 'r', label='Theory')
plt.plot(thresholdsMC, eulerCharacMC, '*b', label='Monte Carlo')
plt.legend()
plt.show()

# -*- coding: utf-8 -*-
"""
Plotting the particles orientation
==================================

A simple example for ploting particles orientation.
"""

import spam.label.toolkit as ltk
import spam.plotting.orientationPlotter as orientationPlotter
import tifffile

lab = tifffile.imread("../data/M2EA05/M2EA05-quart-01-bin-watershed.tif")

[eigenValues, eigenVectors] = ltk.getMomentOfInertia(lab)

orientationPlotter.plotProjection(eigenVectors[:, 0:3],
                                  title="M2EA05 Particle Orientataion (Maj Eigenvector)",
                                  subtitle={"points": "Each major eigenvector",
                                            "bins": "Binned orientations"},
                                  pointMarkerSize=3)

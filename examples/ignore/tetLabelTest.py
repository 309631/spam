import numpy
import tifffile
import pickle
import spam.label.toolkit

p = pickle.load(open("data/mesh.p"))

# print p['points'].max()
print(p.keys())

spam.label.toolkit.getLabelledTetrahedra([300, 300, 300], p['cells'], p['points'])

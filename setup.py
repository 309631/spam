import glob
import os
import platform

import setuptools

try:
    from pybind11.setup_helpers import ParallelCompile, Pybind11Extension

    # Optional multithreaded build
    ParallelCompile(default=0).install()
except ImportError:
    from setuptools import Extension as Pybind11Extension


modulesToCompile = ["mesh", "label", "filters", "measurements", "kalisphera", "DIC"]

###############################################################
# hey! If you're adding something to the list below, please
# also add it to the list in docs/source/scripts.rst
###############################################################
scripts = [
    "scripts/spam-ereg",
    "scripts/spam-ereg-discrete",
    "scripts/spam-ddic",
    "scripts/spam-deformImage",
    "scripts/spam-discreteStrain",
    "scripts/spam-filterPhiField",
    "scripts/spam-gdic",
    "scripts/spam-grf",
    "scripts/spam-hdf-reader",
    "scripts/spam-imposeBCFromDVC",
    "scripts/spam-ITKwatershed",
    "scripts/spam-ldic",
    "scripts/spam-mesh",
    "scripts/spam-mesh-subdomains",
    "scripts/spam-mmr",
    "scripts/spam-mmr-graphical",
    "scripts/spam-moveLabels",
    "scripts/spam-passPhiField",
    "scripts/spam-pixelSearch",
    "scripts/spam-pixelSearchPropagate",
    "scripts/spam-reg",
    "scripts/spam-regularStrain",
]

packages = [
    "spam",
    "spam.deformation",
    "spam.DIC",
    "spam.excursions",
    "spam.filters",
    "spam.helpers",
    "spam.kalisphera",
    "spam.label",
    "spam.measurements",
    "spam.mesh",
    "spam.orientations",
    "spam.plotting",
    "spam.visual",
]

# 2022-02: EA: a catch for windows compile flags in conda-forge
if platform.system() == "Windows":
    EXTRA_CFLAGS = ["/std:c++17"]
    EXTRA_LFLAGS = None
    LIBS = None

elif platform.system() == "Darwin":
    EXTRA_CFLAGS = ["-std=c++14", "-O3", "-lgmp"]
    EXTRA_LFLAGS = ["-lgmp"]
    LIBS = ["gmp"]
else:
    EXTRA_CFLAGS = ["-std=c++14", "-O3", "-lgmp", "-fopenmp"]
    EXTRA_LFLAGS = ["-lgmp", "-fopenmp"]
    LIBS = ["gmp"]


class get_pybind_include:
    """
    Helper class to determine the pybind11 include path
    The purpose of this class is to postpone importing pybind11
    until it is actually installed, so that the ``get_include()``
    method can be invoked.
    """

    def __init__(self, user=False):
        self.user = user

    def __str__(self):
        import pybind11

        return pybind11.get_include(self.user)


# global extensions
extensions = []
for moduleName in modulesToCompile:
    extensions.append(
        Pybind11Extension(
            "spam." + moduleName + "." + moduleName + "Toolkit",
            glob.glob("src/spam/" + moduleName + "/src/*.cpp"),
            extra_compile_args=EXTRA_CFLAGS,
            extra_link_args=EXTRA_LFLAGS,
            include_dirs=[
                "src/spam/" + moduleName + "/include/",
                os.getenv("PREFIX"),
                os.environ.get("EIGEN_INCLUDE_DIR", "/usr/include/eigen3/"),
                "/usr/local/include/eigen3",
                "/usr/include/eigen3",
                "/usr/local/include",
                "/usr/include",
                "/opt/homebrew/Cellar/cgal/5.5.2/include",
                "/opt/homebrew/Cellar/boost/1.81.0_1/include",
                "/opt/homebrew/Cellar/eigen/3.4.0_1/include/eigen3",
                get_pybind_include(),
                get_pybind_include(user=True),
            ],
            libraries=LIBS,
        )
    )


setuptools.setup(
    # name='spam',
    # version='0.6.2.1',
    # copyright = "SPAM authors 2020",
    package_dir={"spam": "src/spam"},
    packages=packages,
    # include_package_data=True,
    ext_modules=extensions,
    scripts=scripts,
    # cmdclass=cmdclass,
    data_files=[("share/img", ["docs/source/images/icon.png", "docs/source/images/logo/spam-horizontal.png", "docs/source/images/logo/spam-logo.png"])],
)

#!/bin/bash
set -e -x

#   "3.8.16"
PYBINS=(
   "3.9.16"
   "3.10.11"
   "3.11.3"
)

# Compile wheels
for PYBIN in ${PYBINS[@]}; do
    pyenv local $PYBIN
    pyenv versions
    python3 --version
    python3 -m pip install -U pip
    python3 -m pip install -U wheel
    python3 -m pip install -U setuptools
    python3 -m pip install numpy
    python3 -m pip install pybind11
    python3 -m pip install cython        # needed for scikit image
    python3 setup.py build -e "/usr/bin/env python"
    python3 -m pip install .
    python3 -m pip wheel . -w wheelhouse/
    rm -rf build dist
done

python3 -m pip install twine
#python -m twine upload ./wheelhouse/spam-*.whl --verbose
